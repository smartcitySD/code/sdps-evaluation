package surf.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Random;

import surf.data.entity.Service;

public class Util {
	
	public Util(){
		
	}
	
	public String readFile( String file ) throws IOException {
	    BufferedReader reader = new BufferedReader( new FileReader (file));
	    String         line = null;
	    StringBuilder  stringBuilder = new StringBuilder();
	    String         ls = System.getProperty("line.separator");

	    try {
	        while( ( line = reader.readLine() ) != null ) {
	            stringBuilder.append( line );
	            stringBuilder.append( ls );
	        }

	        return stringBuilder.toString().replaceAll("[\n\r]","");
	    } finally {
	        reader.close();
	    }
	}
	
	public String readFileList( String file ) throws IOException {
	    BufferedReader reader = new BufferedReader( new FileReader (file));
	    String         line = null;
	    StringBuilder  stringBuilder = new StringBuilder();
	    //String         ls = System.getProperty("line.separator");

	    try {
	        while( ( line = reader.readLine() ) != null ) {
	            stringBuilder.append( line );
	            stringBuilder.append( "-" );
	        }

	        return stringBuilder.toString().replaceAll("[\n\r]","");
	    } finally {
	        reader.close();
	    }
	}
	
	public void writeFile( Service service, int type ) {
		File f;
		//f = new File("/home/ubuntu/data/"+service.getId()+".txt");
		//f = new File("D:\\experiments\\data\\"+type+"\\"+service.getId()+".txt");
		f = new File("\\home\\experiments\\data\\"+type+"\\"+service.getId()+".txt");
		
		try{
			FileWriter w = new FileWriter(f);
			BufferedWriter bw = new BufferedWriter(w);
			PrintWriter wr = new PrintWriter(bw);  
			wr.write(service.getId()+";"+service.getUri()+";"+service.getName()+";"+service.getRt()+";"+service.getRb()+";"+service.getLocationX()+";"+service.getLocationY()+";"+service.getLocationZ()+";"+service.getDescription()+";"+service.getUnit()+";"+service.getTitle()+";"+service.getInput()+";"+service.getOutput());
			wr.close();
			bw.close();
			}
		catch(IOException e){
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}

	public void writeFile( String path, ArrayList<String> content ) {
		File f;
		f = new File(path);
		
		try{
			FileWriter w = new FileWriter(f);
			BufferedWriter bw = new BufferedWriter(w);
			PrintWriter wr = new PrintWriter(bw);
			for(int i=0;i<content.size();i++)wr.write(content.get(i)+"\n");
			wr.close();
			bw.close();
			}
		catch(IOException e){
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}
	
	public ArrayList<ArrayList<Integer>> selectData(int n,double x) {
		// TODO Auto-generated method stub
		ArrayList<ArrayList<Integer>> gatewaysDataPoints = new ArrayList<ArrayList<Integer>>();
		ArrayList<Integer> gateway1 = new ArrayList<Integer>();
		ArrayList<Integer> gateway2 = new ArrayList<Integer>();
		//double both = n*x;
		Random rnd = new Random();
		
		for(double i=0; i<n;i++){
			int value = (int) (rnd.nextDouble() * 59108);
			if(!gateway1.contains(value)){
				gateway1.add(value);
			}
			else i--;
		}
		
		for(double i=0; i<n;i++){
			int value = (int) (rnd.nextDouble() * 59108);
			if(!gateway2.contains(value)){
				gateway2.add(value);
			}
			else i--;
		}
		
		/*for(double i=0; i<n-both;i++){
			int value = (int) (rnd.nextDouble() * 59108);
			if(!gateway1.contains(value)){
				gateway1.add(value);
			}
			else i--;
		}
		
		for(double i=0; i<n-both;i++){
			int value = (int) (rnd.nextDouble() * 59108);
			if(!gateway2.contains(value) && !gateway1.contains(value)){
				gateway2.add(value);
			}
			else i--;
		}*/
		
		gatewaysDataPoints.add(gateway1);
		gatewaysDataPoints.add(gateway2);
		return gatewaysDataPoints;
	}

	public ArrayList<Service> getServices(ArrayList<Integer> gateway, int type) {
		// TODO Auto-generated method stub
				ArrayList<Service> services = new ArrayList<>();
				//File folder = new File("/home/ubuntu/data/");
				//File folder = new File("D:/experiments/data/"+type+"/");
				File folder = new File("/home/pi/experiments/data/"+type+"/");
				File[] listOfFiles = folder.listFiles();
				
				for(int i=0; i<gateway.size(); i++){
					try{
						String f = readFile(listOfFiles[gateway.get(i)].getPath());
						String [] parts = f.split(";");
						Service service = new Service(parts[0],parts[1],parts[2],parts[10],parts[8],parts[9],300,0.45,parts[5],parts[6],parts[7],parts[11],parts[12].replaceAll("[\n\r]",""));
						services.add(service);
					}
					catch(Exception ex){
						ex.printStackTrace();
					}
				}
				return services;
	}

	public void writeFile(String path, String content) {
		// TODO Auto-generated method stub
		File f;
		f = new File(path);
		
		try{
			FileWriter w = new FileWriter(f);
			BufferedWriter bw = new BufferedWriter(w);
			PrintWriter wr = new PrintWriter(bw);
			wr.write(content);
			wr.close();
			bw.close();
			w.close();
			}
		catch(IOException e){
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}

	public String getAverage(String path) {
		// TODO Auto-generated method stub
		String res="";
		File folder = new File(path);
		File[] listOfFiles = folder.listFiles();
		long avg = 0;
		for (int i = 0; i < listOfFiles.length; i++) {
			try {
				String val = readFile(listOfFiles[i].getPath()).replaceAll("[\n\r]","");
				avg = avg + Long.parseLong(val);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		avg = avg/listOfFiles.length;
		res = "" + avg;
		return res;
	}

	public void delete(String path) {
		// TODO Auto-generated method stub
		File folder = new File(path);
		File[] listOfFiles = folder.listFiles();
		for(int i = 0; i<listOfFiles.length; i++){
			listOfFiles[i].delete();
		}
	}

}
