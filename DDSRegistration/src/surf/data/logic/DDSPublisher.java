package surf.data.logic;

import com.rti.dds.domain.DomainParticipant;
import com.rti.dds.domain.DomainParticipantFactory;
import com.rti.dds.infrastructure.StatusKind;
import com.rti.dds.publication.Publisher;
import com.rti.dds.topic.Topic;
import com.rti.dds.type.builtin.StringDataWriter;
import com.rti.dds.type.builtin.StringTypeSupport;

public class DDSPublisher {
	
	private DomainParticipant participant;
	
	public DDSPublisher(){
		participant = DomainParticipantFactory.get_instance().create_participant( 0, DomainParticipantFactory.PARTICIPANT_QOS_DEFAULT, null, StatusKind.STATUS_MASK_NONE);
	}
	
	public StringDataWriter createDataWriter(String t){
		Topic topic = participant.create_topic(t, StringTypeSupport.get_type_name(), DomainParticipant.TOPIC_QOS_DEFAULT, null, StatusKind.STATUS_MASK_NONE);
		StringDataWriter dataWriter = (StringDataWriter) participant.create_datawriter(topic, Publisher.DATAWRITER_QOS_DEFAULT, null, StatusKind.STATUS_MASK_NONE);
		return dataWriter;
	}
	
	public boolean deleteDataWriter(StringDataWriter dataWriter){
		try{
			participant.delete_datawriter(dataWriter);
			return true;
		}
		catch(Exception ex){
			ex.printStackTrace();
			return false;
		}
	}
	

}
