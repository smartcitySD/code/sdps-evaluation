package surf.tests.DDSRegistration;

import java.io.IOException;
import java.util.ArrayList;

import com.rti.dds.type.builtin.StringDataWriter;

import surf.data.entity.Service;
import surf.data.logic.DDSPublisher;
import surf.util.Util;

public class DDSRegistration {
	public static void main(String[] args) {		
		//String base = "D:/experiments/results/mdns/";
		String base = "/home/pi/experiments/results/dds/";
		int round = 9;
		//Extract extract = new Extract();
		Util u = new Util();
		DDSPublisher publish = new DDSPublisher();
				
		for(int n=2000; n<=2000;n+=200){
			round ++;
			u.writeFile(base+"temp/round.txt", ""+round);
			System.out.println("-----------------------ROUND "+ round +" N=" + n + "-------------------------");
						
			ArrayList<ArrayList<Integer>> gatewaysDataPoints = u.selectData(n,0);
			ArrayList<Integer> gateway1DataPoints = gatewaysDataPoints.get(0);
			System.out.println("-----------------------DATAPOINTS GENERATED-------------------------");
			System.out.println("-----------------------REGISTERING SERVICES GW1-------------------------");
			ArrayList<Service> servicesGW1 = u.getServices(gateway1DataPoints,1);
			ArrayList<StringDataWriter> dataWriters = new ArrayList<>();
			
			try {
				for(int i=0; i<servicesGW1.size();i++){
					System.out.println("-----------------------Registering Service "+(i+1)+"--- ROUND:"+round+"-------------------------");
					System.out.println("ID: "+servicesGW1.get(i).getId());
					dataWriters.add(publish.createDataWriter(servicesGW1.get(i).getId()));
					Thread.sleep(3000);
				}
				
				System.out.println("-----------------------REGISTERED " + n + " SERVICES GW1-------------------------");
				System.out.println("-----------------------WAITING ONE MINUTE AFTER REGISTRATION-------------------------");
				Thread.sleep(60000);
				
				ArrayList<String> timeBeforeRegistration = new ArrayList<String>();
				ArrayList<String> timeRegistration = new ArrayList<String>();
				ArrayList<String> timeUnregistration = new ArrayList<String>();
				ArrayList<String> timeBeforeUnregistration = new ArrayList<String>();
				
				for(int i=1; i<=100;i++){
					System.out.println("-----------------------Registering Example "+i+"--- ROUND:"+round+"-------------------------");
					long timeBefore = System.currentTimeMillis();
					timeBeforeRegistration.add(""+timeBefore);
					StringDataWriter exampleDataWriter = publish.createDataWriter("example"+i);
					long timeAfter = System.currentTimeMillis() - timeBefore;
					timeRegistration.add(""+timeAfter);
					Thread.sleep(5000);
					System.out.println("-----------------------Unregistering Example "+i+"--- ROUND:"+round+"-------------------------");
					timeBefore = System.currentTimeMillis();
					timeBeforeUnregistration.add(""+timeBefore);
					publish.deleteDataWriter(exampleDataWriter);
					timeAfter=System.currentTimeMillis()-timeBefore;
					timeUnregistration.add(""+timeAfter);
					Thread.sleep(3000);
				}
				u.writeFile(base+"/"+round+"/1/timesRegistration.txt", timeRegistration);
				u.writeFile(base+"/"+round+"/1/timesUnregistration.txt", timeUnregistration);
				u.writeFile(base+"/"+round+"/timesBeforeRegistration.txt", timeBeforeRegistration);
				u.writeFile(base+"/"+round+"/timesBeforeUnregistration.txt", timeBeforeUnregistration);
				
				for(int i=0; i<dataWriters.size();i++){
					System.out.println("-----------------------Unregistering Service "+(i+1)+"--- ROUND:"+round+"-------------------------");
					System.out.println("ID: "+servicesGW1.get(i).getId());
					publish.deleteDataWriter(dataWriters.get(i));
					Thread.sleep(1000);
				}
				
				System.out.println("-----------------------WAITING 10 SECONDS AFTER UNREGISTRATION-------------------------");
				Thread.sleep(10000);
				System.out.println("-----------------------UNREGISTERED " + n + " SERVICES GW1-------------------------");
			}
			catch(Exception e3){
				e3.printStackTrace();
			}
		}
		System.out.println("-----------------------WAITING BEFORE PROCESSING ONE MINUTE-------------------------");		
		try {
			Thread.sleep(60000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		System.out.println("PROCESSING DATA");
		ArrayList<String>timesRegistration = new ArrayList<>();
		ArrayList<String>timesUnregistration = new ArrayList<>();
		
		try {
			for(int r = 10;r<=10;r++){
				ArrayList<Long>timesBeforeRegistration = new ArrayList<>();
				ArrayList<Long>timesBeforeUnregistration = new ArrayList<>();
				System.out.println("Reading Data Round");
				String [] beforeRegistration = u.readFileList(base+"/"+r+"/timesBeforeRegistration.txt").split("-");
				for(int i =0; i<beforeRegistration.length; i++){
					timesBeforeRegistration.add(Long.parseLong(beforeRegistration[i]));
				}
				System.out.println("Before Registration Values:" + beforeRegistration.length);
				String [] beforeUnregistration = u.readFileList(base+"/"+r+"/timesBeforeUnregistration.txt").split("-");
				for(int i =0; i<beforeUnregistration.length; i++){
					timesBeforeUnregistration.add(Long.parseLong(beforeUnregistration[i]));
				}
				System.out.println("Before Unregistration Values:" + beforeUnregistration.length);
				
				for(int n=5;n<=20;n=n+5){
					
					ArrayList<Long>timesAfterRegistration = new ArrayList<>();
					ArrayList<Long>timesAfterUnregistration = new ArrayList<>();
					
					System.out.println("Reading Data Nodes");
					String [] afterRegistration = u.readFileList(base+"/"+r+"/"+n+"/timesRegister.txt").split("-");
					for(int i =0; i<afterRegistration.length; i++){
						timesAfterRegistration.add(Long.parseLong(afterRegistration[i]));
					}
					System.out.println("Times Registration Values:" + afterRegistration.length);
					String [] afterUnregistration = u.readFileList(base+"/"+r+"/"+n+"/timesUnregister.txt").split("-");
					for(int i =0; i<afterUnregistration.length; i++){
						timesAfterUnregistration.add(Long.parseLong(afterUnregistration[i]));
					}
					System.out.println("Times Unregistration Values:" + afterUnregistration.length);
					
					System.out.println("Substracting Times");
					for(int i=0; i<100;i++){
						long totalReg = timesAfterRegistration.get(i)-timesBeforeRegistration.get(i);
						timesRegistration.add(""+totalReg);
						long totalUnreg = timesAfterUnregistration.get(i)-timesBeforeUnregistration.get(i);
						timesUnregistration.add(""+totalUnreg);
					}
					
					System.out.println("WRITING FILES Round:"+r+"---Node:"+n);
					u.writeFile(base+"/"+r+"/"+n+"/timesRegistration.txt", timesRegistration);
					u.writeFile(base+"/"+r+"/"+n+"/timesUnregistration.txt", timesUnregistration);
					timesRegistration = new ArrayList<>();
					timesUnregistration = new ArrayList<>();
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}