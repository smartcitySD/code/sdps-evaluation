package surf.data.entity;

public class Service {

	private String id;
	private String uri;
	private String name;
	private String title;
	private String description;
	private String unit;
	private int rt;
	private double rb;
	private String locationX;
	private String locationY;
	private String locationZ;
	private String input;
	private String output;
	
	
	public Service(String id, String uri, String name, String title, String description, String unit, int rt, double rb, String locationX, String locationY, String locationZ, String input, String output) {
		// TODO Auto-generated constructor stub
		this.id = id;
		this.uri = uri;
		this.name = name;
		this.title = title;
		this.description = description;
		this.setUnit(unit);
		this.rt = rt;
		this.rb = rb;
		this.locationX = locationX;
		this.locationY = locationY;
		this.setLocationZ(locationZ);
		this.input=input;
		this.output=output;
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getUri() {
		return uri;
	}


	public void setUri(String uri) {
		this.uri = uri;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getUnit() {
		return unit;
	}


	public void setUnit(String unit) {
		this.unit = unit;
	}

	public int getRt() {
		return rt;
	}


	public void setRt(int rt) {
		this.rt = rt;
	}


	public double getRb() {
		return rb;
	}


	public void setRb(double rb) {
		this.rb = rb;
	}


	public String getLocationX() {
		return locationX;
	}


	public void setLocationX(String locationX) {
		this.locationX = locationX;
	}


	public String getLocationY() {
		return locationY;
	}


	public void setLocationY(String locationY) {
		this.locationY = locationY;
	}


	public String getLocationZ() {
		return locationZ;
	}


	public void setLocationZ(String locationZ) {
		this.locationZ = locationZ;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getInput() {
		return input;
	}


	public void setInput(String input) {
		this.input = input;
	}
	
	public String getOutput() {
		return output;
	}


	public void setOutput(String output) {
		this.output = output;
	}

}
