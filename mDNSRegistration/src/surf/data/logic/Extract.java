package surf.data.logic;

import java.io.File;
import java.util.ArrayList;
import java.util.Random;

import surf.data.entity.Service;
import surf.util.Util;

public class Extract {
	
	public Extract(){
		
	}

	public void extractData() {
		// TODO Auto-generated method stub
		ArrayList<Service> services = new ArrayList<Service>();
		services = getServices();
		extraction(1,services);
		extraction(2,services);
		extraction(3,services);
	}
	
	public void extraction(int type,ArrayList<Service> services){
		//File folder = new File("/home/ubuntu/Downloads/rdf/");
		switch(type){
			case 1:
				writeServices(services,type);
				break;
			case 2:
				services = transformServiceNames(services);
				writeServices(services, type);
				break;
			case 3:
				services = transformServiceSemantics(services);
				writeServices(services,type);
				break;
		}
	}
	
	public ArrayList<Service> getServices(){
		System.out.println("**********************GET SERVICES**********************");
		Service service = null;
		Util u = new Util();
		Random randomGenerator = new Random();
		String[] parameters = null;
		//File folder = new File("D:/experiments/rdf/");
		File folder = new File("/home/experiments/rdf/");
		File[] listOfFiles = folder.listFiles();
		ArrayList<Service> services = new ArrayList<Service>();
		
		for (int i = 0; i < listOfFiles.length; i++) {
			String station = "";
			String uri="";
			String id = "";
			String name = "";
			String title = "";
			String description = "";
			String unit = "";
			int rt = 0;
			double rb = 0;
			String locationX="";
			String locationY="";
			String locationZ="";
			String input = "";
			String output = "";
			
			System.out.println("----------------New Service FILE " + listOfFiles[i].getName() +  "----------------");
			
			try {
				String f = u.readFile(listOfFiles[i].getPath()).replaceAll("\n", "");
				if(f.contains("om-owl:parameter")){
					String[] parts = f.split("sens_obs:");
					for(int j = 0; j < parts.length ; j++){
						if(parts[j].contains("System")){
							String [] p1=parts[j].split("hasSourceURI ");
							String [] p2=p1[1].split("om-owl:parameter");
							station = p2[0].split("=")[1].replace(" ","").replace(">;", "").replaceAll("[\n\r]","");
							String [] p3=p2[1].split("om-owl:processLocation");
							parameters = p3[0].split("weather:_");
						}
						if(parts[j].contains("point")){
							String[] p4 = parts[j].split("alt");
							String[] p5 = p4[1].split("lat");
							locationZ = p5[0].replace(" ", "").replace("\"", "").replace("^", "").replace("xsd:float;wgs84:", "").replaceAll("[\n\r]","").replace(";wgs84:", "").replace("xsd:", "").replace("float", "");
							String[] p6 = p5[1].split("long");
							locationY = p6[0].replace(" ", "").replace("\"", "").replace("^", "").replace("xsd:float;wgs84:", "").replaceAll("[\n\r]","").replace(";wgs84:", "").replace("xsd:", "").replace("float", "");
							String[] p7 = p6[1].split("float");
							locationX = p7[0].replace(" ", "").replace("\"", "").replace("^", "").replace("xsd:float;wgs84:", "").replaceAll("[\n\r]","").replace(";wgs84:", "").replace("xsd:", "").replace("float", "");
						}
					}
					
					rt = (int) Math.round(randomGenerator.nextGaussian() * 100 + 500);
					rb = Math.round(randomGenerator.nextGaussian() * 75 + 800);
					rb = rb/1000;
				
					for(int j = 1; j<parameters.length;j++){
						parameters[j]=parameters[j].replace(" ", "").replace(",", "").replace(";", "").replace("_", "");
						name = parameters[j].replaceAll("[\n\r]","");;
						title = parameters[j].replaceAll("[\n\r]","");;
						switch (name){
							case "WindDirection":
								unit = "degrees";
								input = "string";
								output = "double";
								break;
							case "RelativeHumidity":
								unit = "%";
								input = "string";
								output = "double";
								break;
							case "DewPoint":
								unit = "*C";
								input = "string";
								output = "int";
								break;
							case "AirTemperature":
								unit = "*C";
								input = "string";
								output = "int,locationX,locationY,locationZ";
								break;
							case "PeakWindSpeed":
								unit = "km/h";
								input = "string";
								output = "int";
								break;
							case "WindSpeed":
								unit = "km/h";
								input = "string";
								output = "int";
								break;
							case "WindGust":
								unit = "km/h";
								input = "string";
								output = "int";
								break;
							case "PrecipitationAccumulated":
								unit = "mm3";
								input = "string";
								output = "int";
								break;
							case "PeakWindDirection":
								unit = "degrees";
								input = "string";
								output = "double";
								break;
							case "Visibility":
								unit = "kms";
								input = "string";
								output = "int";
								break;
							case "SnowSmoothed":
								unit = "kg/m2";
								input = "string";
								output = "double";
								break;
							case "SnowDepth":
								unit = "kg/m2";
								input = "string";
								output = "double";
								break;
							case "PrecipitationSmoothed":
								unit = "kg/m2";
								input = "string";
								output = "double";
								break;
							case "SoilTemperature":
								unit = "*C";
								input = "string";
								output = "int";
								break;
							case "SoilMoisture":
								unit = "mm3";
								input = "string";
								output = "int";
								break;
							case "SnowInterval":
								unit = "sec";
								input = "string";
								output = "int";
								break;
					}
					description = "This service monitors " + name;
					uri = "http://surf/"+station + "_" + name+"/"; 
					id = station + "_"+ name;
					
					System.out.println(id);
					service = new Service(id,uri,name,title,description,unit,rt,rb,locationX,locationY,locationZ,input,output);
					services.add(service);
					id = "";
					uri = "";
					name = "";
					title = "";
					description = "";
					unit = "";
					}
				}
			}
			catch (Exception ex){
				ex.printStackTrace();
			}
		}
		return services;
	}
	
	private ArrayList<Service> transformServiceNames(ArrayList<Service> services) {
		// TODO Auto-generated method stub
		Random randomGenerator = new Random();
		for(int i=0; i<services.size(); i++){
			
			float r = randomGenerator.nextFloat();
			if(r>=0.9)services.get(i).setName(services.get(i).getName().replace("i", ""));
			
			r = randomGenerator.nextFloat();
			if(r>=0.5)services.get(i).setName(services.get(i).getName().substring(0, 6));
			
			r = randomGenerator.nextFloat();
			if(r>=0.6){
				if(!services.get(i).getName().equals("AirTemperature")&&!services.get(i).getName().equals("AirTemp")&&!services.get(i).getName().equals("ArTemperature")&&!services.get(i).getName().equals("ArTemp"))services.get(i).setName("AirTemperature");
			}
		}
		return services;
		
	}

	private ArrayList<Service> transformServiceSemantics(ArrayList<Service> services) {
		// TODO Auto-generated method stub
		Random randomGenerator = new Random();
		for(int i=0; i<services.size(); i++){
			
			float r = randomGenerator.nextFloat();
			if(r>=0.9)services.get(i).setName(services.get(i).getName().replace("i", ""));
			
			r = randomGenerator.nextFloat();
			if(r>=0.5)services.get(i).setName(services.get(i).getName().substring(0, 6));
			
			r = randomGenerator.nextFloat();
			if(r>=0.6){
				if(!services.get(i).getName().equals("AirTemperature")&&!services.get(i).getName().equals("AirTemp")&&!services.get(i).getName().equals("ArTemperature")&&!services.get(i).getName().equals("ArTemp"))services.get(i).setName("AirTemperature");
			}
			
			r = randomGenerator.nextFloat();
			if(r>=0.7){
				if(services.get(i).getName().equals("AirTemperature")){
					services.get(i).setName("TemperaturaDelAire");
					services.get(i).setUnit("*F");
				}
			}
		}
		return services;
	}
	
	private void writeServices(ArrayList<Service> services,int type) {
		Util u = new Util();
		for(int i=0; i<services.size();i++)u.writeFile(services.get(i),type);
	}
		
}
