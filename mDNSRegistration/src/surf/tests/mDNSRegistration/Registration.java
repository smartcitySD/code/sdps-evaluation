package surf.tests.mDNSRegistration;

import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
/*import java.util.HashMap;
import java.util.Map;*/

import javax.jmdns.JmDNS;
import javax.jmdns.ServiceInfo;

//import com.apple.dnssd.DNSSDRegistration;
//import com.apple.dnssd.TXTRecord;

import surf.data.entity.Service;
//import surf.tests.mDNSRegistration.DNSRegister;
import surf.util.Util;

public class Registration {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		//String base = "D:/experiments/results/mdns/";
		String base = "/home/pi/experiments/results/mdns/";
		int round = 6;
		//Extract extract = new Extract();
		Util u = new Util();
		u.delete(base+"temp/dis");
		u.delete(base+"temp/reg");
		u.delete(base+"temp/unreg");
		
		for(int n=1400; n<=1400;n+=200){
		
			round ++;
			u.writeFile(base+"temp/round.txt", ""+round);
			u.writeFile(base+"temp/i.txt", "1");
			System.out.println("-----------------------ROUND "+ round +" N=" + n + "-------------------------");
						
			ArrayList<ArrayList<Integer>> gatewaysDataPoints = u.selectData(n,0);
			ArrayList<Integer> gateway1DataPoints = gatewaysDataPoints.get(0);
			System.out.println("-----------------------DATAPOINTS GENERATED-------------------------");
			System.out.println("-----------------------REGISTERING SERVICES GW1-------------------------");
			ArrayList<Service> servicesGW1 = u.getServices(gateway1DataPoints,1);
			
			try {
				byte[] ipAddr = new byte[]{(byte)192, (byte)168, (byte)2, (byte)1};
				InetAddress addr = InetAddress.getByAddress(ipAddr);
				JmDNS jmdnsGW1 = JmDNS.create(addr);
				jmdnsGW1.unregisterAllServices();
				for(int i=0; i<servicesGW1.size();i++){
					System.out.println("-----------------------Registering Service "+(i+1)+"--- ROUND:"+round+"-------------------------");
					int port = 2000 + i;
					/*Map<String, Object> map = new HashMap<String, Object>();
					map.put("uri", servicesGW1.get(i).getUri());
					map.put("title", servicesGW1.get(i).getTitle());
					map.put("description", servicesGW1.get(i).getDescription());
					map.put("unit", servicesGW1.get(i).getUnit());
					map.put("rt", "" + servicesGW1.get(i).getRt());
					map.put("rel", "" +servicesGW1.get(i).getRb());
					map.put("locationX", servicesGW1.get(i).getLocationX());
					map.put("locationY", servicesGW1.get(i).getLocationY());
					map.put("locationZ", servicesGW1.get(i).getLocationZ());
					map.put("input", servicesGW1.get(i).getInput());
					map.put("output", servicesGW1.get(i).getOutput());*/
					System.out.println("-----------------------"+servicesGW1.get(i).getId()+"--- ROUND:"+round+"-------------------------");
					/*ServiceInfo info = ServiceInfo.create("_"+servicesGW1.get(i).getName()+"._tcp.local.",servicesGW1.get(i).getId(),port,100,100,map);*/
					ServiceInfo info = ServiceInfo.create("_"+servicesGW1.get(i).getName()+"._tcp.local.",servicesGW1.get(i).getId(), port, "");
					try {
						jmdnsGW1.registerService(info);
						Thread.sleep(1000);
					} catch (Exception e1) {
						// 	TODO Auto-generated catch block
						e1.printStackTrace();
						System.out.println("Problems registering service in round: " + round);
					}
				}
				
				/*System.out.println("-----------------------WAITING TEN SECONDS-------------------------");
				Thread.sleep(10000);
				System.out.println("-----------------------REGISTERED " + n + " SERVICES GW1-------------------------");
										*/
				ArrayList<String> timeBeforeRegistration = new ArrayList<String>();
				ArrayList<String> timeBeforeUnregistration = new ArrayList<String>();
				byte[] ipAddrExample = new byte[]{(byte)192, (byte)168, (byte)2, (byte)1};
				InetAddress addrExample = InetAddress.getByAddress(ipAddrExample);
				JmDNS jmdnsExample = JmDNS.create(addrExample);
				for(int i=1; i<=400;i++){
					u.writeFile(base+"temp/i.txt", ""+i);
					
					int port = 3000 + i;
					System.out.println("-----------------------Registering Example "+i+"--- ROUND:"+round+"-------------------------");
					/*Map<String, Object> map = new HashMap<String, Object>();
					map.put("uri", "http://surf.mdns/exammple_"+i);
					map.put("title", "Example_"+i);
					map.put("description", "This is the example " +i);
					map.put("unit", "*U");
					map.put("rt", "" + "900");
					map.put("rel", "" + "0.5");
					map.put("locationX", "52.236");
					map.put("locationY", "-15.25");
					map.put("locationZ", "3.5");
					map.put("input", "string");
					map.put("output", "int");
					ServiceInfo info = ServiceInfo.create("_example._tcp.local.","example"+i,port,100,100,map);*/
					//ServiceInfo info = ServiceInfo.create("_example._tcp","example"+i, port," ");
					ServiceInfo info = ServiceInfo.create("_example._tcp.local.","example"+i, port, "");
					try {
						timeBeforeRegistration.add(""+System.currentTimeMillis()+","+i);
						jmdnsExample.registerService(info);
						/*Process proc = null;
					    try {
					    	String command="sh /home/pi/mdns/register.sh example" + i + " _example._tcp " + port;
					    	System.out.println(command);
					    	timeBeforeRegistration.add(""+System.currentTimeMillis());
					        proc = Runtime.getRuntime().exec(command);
					        //proc.waitFor();
					    } catch (IOException e) {
					        // TODO Auto-generated catch block
					        e.printStackTrace();
					    }*/
						Thread.sleep(5000);
						timeBeforeUnregistration.add(""+System.currentTimeMillis()+","+i);
						jmdnsExample.unregisterService(info);
						Thread.sleep(5000);
						//proc.destroy();
					} catch (Exception e1) {
						//	TODO Auto-generated catch block
						e1.printStackTrace();
						System.out.println("Problems registering service in round: " + round);
					}
				}
				jmdnsExample.close();
				u.writeFile(base+"/"+round+"/timesBeforeRegistration.txt", timeBeforeRegistration);
				u.writeFile(base+"/"+round+"/timesBeforeUnregistration.txt", timeBeforeUnregistration);
				
				for(int i=0; i<servicesGW1.size();i++){
					System.out.println("-----------------------Unregistering Service "+(i+1)+"--- ROUND:"+round+"-------------------------");
					int port = 2000 + i;
					/*Map<String, Object> map = new HashMap<String, Object>();
					map.put("uri", servicesGW1.get(i).getUri());
					map.put("title", servicesGW1.get(i).getTitle());
					map.put("description", servicesGW1.get(i).getDescription());
					map.put("unit", servicesGW1.get(i).getUnit());
					map.put("rt", "" + servicesGW1.get(i).getRt());
					map.put("rel", "" +servicesGW1.get(i).getRb());
					map.put("locationX", servicesGW1.get(i).getLocationX());
					map.put("locationY", servicesGW1.get(i).getLocationY());
					map.put("locationZ", servicesGW1.get(i).getLocationZ());
					map.put("input", servicesGW1.get(i).getInput());
					map.put("output", servicesGW1.get(i).getOutput());
					ServiceInfo info = ServiceInfo.create("_"+servicesGW1.get(i).getName()+"._tcp.local.",servicesGW1.get(i).getId(),port,100,100,map);*/
					ServiceInfo info = ServiceInfo.create("_"+servicesGW1.get(i).getName()+"._tcp.local.",servicesGW1.get(i).getId(), port, "");
					try {
						System.out.println("-----------------------"+servicesGW1.get(i).getId()+"--- ROUND:"+round+"-------------------------");
						jmdnsGW1.unregisterService(info);
					} catch (Exception e1) {
						// 	TODO Auto-generated catch block
						e1.printStackTrace();
						System.out.println("Problems unregistering service in round: " + round);
					}
				}
				Thread.sleep(10000);
				u.delete(base+"temp/dis");
				u.delete(base+"temp/reg");
				u.delete(base+"temp/unreg");
				u.delete(base+"temp/bro");
				jmdnsGW1.close();
			}
			catch(Exception e3){
				e3.printStackTrace();
			}
		}
				
		System.out.println("PROCESSING DATA");
		ArrayList<String>timesRegistration = new ArrayList<>();
		ArrayList<String>timesUnregistration = new ArrayList<>();
		
		try {
			for(int r = 7;r<=7;r++){
				ArrayList<Integer>examplesBeforeRegistration = new ArrayList<>();
				ArrayList<Integer>examplesBeforeUnregistration = new ArrayList<>();
				ArrayList<Long>timesBeforeRegistration = new ArrayList<>();
				ArrayList<Long>timesBeforeUnregistration = new ArrayList<>();
				System.out.println("Reading Data Round");
				String [] beforeRegistration = u.readFileList(base+"/"+r+"/timesBeforeRegistration.txt").split("-");
				System.out.println("Before Registration Values:" + beforeRegistration.length);
				String [] beforeUnregistration = u.readFileList(base+"/"+r+"/timesBeforeUnregistration.txt").split("-");
				System.out.println("Before Unregistration Values:" + beforeUnregistration.length);
				
				System.out.println("Splitting Before Registration");
				for(int i=0;i<beforeRegistration.length;i++){
					timesBeforeRegistration.add(Long.parseLong(beforeRegistration[i].split(",")[0]));
					examplesBeforeRegistration.add(Integer.parseInt(beforeRegistration[i].split(",")[1]));
				}
				
				System.out.println("Splitting Before Unregistration");
				for(int i=0;i<beforeUnregistration.length;i++){
					timesBeforeUnregistration.add(Long.parseLong(beforeUnregistration[i].split(",")[0]));
					examplesBeforeUnregistration.add(Integer.parseInt(beforeUnregistration[i].split(",")[1]));
				}
				for(int n=0;n<=20;n=n+5){
					if(n==0)n=1;
					ArrayList<Long>timesAfterRegistration = new ArrayList<>();
					ArrayList<Long>timesAfterUnregistration = new ArrayList<>();
					ArrayList<Integer>examplesAfterRegistration = new ArrayList<>();
					ArrayList<Integer>examplesAfterUnregistration = new ArrayList<>();
					
					System.out.println("Reading Data Nodes");
					String [] afterRegistration = u.readFileList(base+"/"+r+"/"+n+"/timesRegister.txt").split("-");
					System.out.println("Times Registration Values:" + afterRegistration.length);
					String [] afterUnregistration = u.readFileList(base+"/"+r+"/"+n+"/timesUnregister.txt").split("-");
					System.out.println("Times Unregistration Values:" + afterUnregistration.length);
					
					System.out.println("Splitting After Registration");
					for(int i=0;i<afterRegistration.length;i++){
						timesAfterRegistration.add(Long.parseLong(afterRegistration[i].split(",")[0]));
						examplesAfterRegistration.add(Integer.parseInt(afterRegistration[i].split(",")[1]));
					}
					
					System.out.println("Splitting After Unregistration");
					for(int i=0;i<afterUnregistration.length;i++){
						timesAfterUnregistration.add(Long.parseLong(afterUnregistration[i].split(",")[0]));
						examplesAfterUnregistration.add(Integer.parseInt(afterUnregistration[i].split(",")[1]));
					}
					
					System.out.println("Substracting Times");
					int j1=0;
					int j2=0;
					for(int i=0; i<400;i++){
						if(examplesBeforeRegistration.get(i)-examplesAfterRegistration.get(j1)==0){
							long totalReg = timesAfterRegistration.get(j1)-timesBeforeRegistration.get(i);
							timesRegistration.add(""+totalReg);
							j1++;
						}
						
						if(examplesBeforeUnregistration.get(i)-examplesAfterUnregistration.get(j2)==0){
							long totalUnreg = timesAfterUnregistration.get(j2)-timesBeforeUnregistration.get(i);
							timesUnregistration.add(""+totalUnreg);
							j2++;
						}
					}
					System.out.println("WRITING FILES Round:"+r+"---Node:"+n);
					u.writeFile(base+"/"+r+"/"+n+"/timesRegistration.txt", timesRegistration);
					u.writeFile(base+"/"+r+"/"+n+"/timesUnregistration.txt", timesUnregistration);
					timesRegistration = new ArrayList<>();
					timesUnregistration = new ArrayList<>();
					if(n==1)n=0;
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
