package surf.tests.ddssd;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;

import com.jcraft.jsch.ChannelShell;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

import surf.util.Util;

public class DDSDiscovery {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String user = "root";
	    String password = "root";
	    String host = "192.168.1.123";
	    int port=22;
	    int round = 1;
	    Util u = new Util();
	    ArrayList<String> timesRegister = new ArrayList<>();
	    ArrayList<String> timesUnregister = new ArrayList<>();
	    String base = "D:/experiments/results/dds/";
	    
		try{
			
			JSch jsch = new JSch();
			Session session = jsch.getSession(user, host, port);
			session.setPassword(password);
			session.setConfig("StrictHostKeyChecking", "no");
			System.out.println("Establishing Connection...");
			session.connect();
			System.out.println("Connection established.");
			ChannelShell channel=(ChannelShell) session.openChannel("shell");
			BufferedReader in=new BufferedReader(new InputStreamReader(channel.getInputStream()));
			OutputStream out = channel.getOutputStream();
			PrintStream commander = new PrintStream(out, true);
			channel.connect();
			//DataWriter (New) name: "null" topic: "example1" type: "DDS::String"

			commander.println("java -jar /root/env/rti/SimpleDDS/SimpleDDS.jar");
			//round ++;
			commander.close();
        	String line;
        	boolean b = true;
        	boolean r = false;
        	int ser = 1;
        	while ((line = in.readLine()) != null && b){
        	    System.out.print(line+"\n");
        	    if(line.equals("DataWriter (New) name: \"null\" topic: \"example_"+ser+"\" type: \"DDS::String\"")){
        	    	long after = System.currentTimeMillis();
        			String res1 = u.readFile(base+"temp/reg/example"+ser+".txt").replaceAll("[\n\r]","");
        			long before = Long.parseLong(res1);
        			long total = after - before;
        			timesRegister.add(""+total);
        			u.writeFile(base+"/"+round+"/10/timesRegister.txt", timesRegister);
        	    	r = true;
        	    }
        	    if(r){
        	    	if(line.equals("processDiscoveredTypes")){
            	    	long after = System.currentTimeMillis();
            			String res1 = u.readFile(base+"temp/unreg/example"+ser+".txt").replaceAll("[\n\r]","");
            			long before = Long.parseLong(res1);
            			long total = after - before;
            			timesUnregister.add(""+total);
            			u.writeFile(base+"/"+round+"/10/timesUnregister.txt", timesUnregister);
            	    	ser ++;
            	    	r = false;
            	    }
        	    }
        	    System.out.println("Service:" + ser);
        	    if(ser==51 && !r){
        	    	round ++;
        	    	timesRegister = new ArrayList<>();
        	    	timesUnregister = new ArrayList<>();
        	    	ser = 1;
        	    }

        	}
        	channel.disconnect();
        	session.disconnect();
        	
        }catch(Exception e){
        	e.printStackTrace();
        }

	}

}
