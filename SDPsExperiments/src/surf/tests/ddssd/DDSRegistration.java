package surf.tests.ddssd;

/*import java.io.BufferedReader;
import java.io.InputStreamReader;*/
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;

import com.jcraft.jsch.ChannelShell;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

import surf.data.entity.Service;
import surf.util.Util;

public class DDSRegistration {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String user = "pi";
	    String password = "raspberry";
	    String host = "192.168.1.110";
	    int port=22;
	    int round = 0;
	    //int c = 0;
	    
	    String base = "D:/experiments/results/dds/";
	    
	    Util u = new Util();
	    u.delete(base+"temp/dis");
		u.delete(base+"temp/reg");
		u.delete(base+"temp/unreg");

		for(int n=200; n<=2000;n+=200){
			round ++;
			u.writeFile(base+"temp/round.txt", ""+round);
			System.out.println("-----------------------ROUND "+ round +" N=" + n + "-------------------------");
			/*ArrayList<String> responseTimeRegisterGw1 = new ArrayList<String>();
			ArrayList<String> responseTimeDiscoveryGw1 = new ArrayList<String>();
			ArrayList<String> responseTimeUnregisterGw1 = new ArrayList<String>();*/
			ArrayList<ArrayList<Integer>> gatewaysDataPoints = new ArrayList<>();
			//if(c==0){						
				gatewaysDataPoints = u.selectData(200,0);
			//	c=1;
			//}
			//else gatewaysDataPoints = u.selectData(200,0);
			ArrayList<Integer> gateway1DataPoints = gatewaysDataPoints.get(0);
			System.out.println("-----------------------DATAPOINTS GENERATED-------------------------");
			System.out.println("-----------------------REGISTERING SERVICES GW1-------------------------");
			ArrayList<Service> servicesGW1 = u.getServices(gateway1DataPoints,1);
			
			try{
				JSch jsch = new JSch();
				Session session = jsch.getSession(user, host, port);
				session.setPassword(password);
				session.setConfig("StrictHostKeyChecking", "no");
				System.out.println("Establishing Connection...");
				session.connect();
				System.out.println("Connection established.");
				ChannelShell channel=(ChannelShell) session.openChannel("shell");
				//BufferedReader in=new BufferedReader(new InputStreamReader(channel.getInputStream()));
				OutputStream out = channel.getOutputStream();
				PrintStream commander=new PrintStream(out, true);
				channel.connect();
				for(int i=0;i<servicesGW1.size();i++){
					commander = new PrintStream(out, true);
					System.out.println("Service: " +servicesGW1.get(i).getId()+ "--- i="+ (i+1) + " --- Round: "+round );
					commander.println("java -jar /home/pi/env/rti/SimpleDDSPublisher4/SimpleDDS.jar \""+servicesGW1.get(i).getId()+"\"");
					Thread.sleep(10000);
					
				}
				commander.close();
				//commander.close();
				//Thread.sleep(60000);
				//commander.close();
	        	/*String line;
	        	boolean b = true;
	        	int ser = 0;*/
	        	/*while ((line = in.readLine()) != null && b){
	        	    System.out.print(line+"\n");
	        	    if(line.equals("DATAWRITER_CREATED"))ser ++;
	        	    if(line.equals("pi@raspberrypi:~$ ")&&ser==200)break;
	        	}*/
	        	channel.disconnect();
	        	session.disconnect();
	        	if(round>=1){
	        		Session session1 = jsch.getSession(user, host, port);
	        		session1.setPassword(password);
	        		session1.setConfig("StrictHostKeyChecking", "no");
	        		System.out.println("Establishing Connection...");
					session1.connect();
					System.out.println("Connection established.");
					ChannelShell channel1=(ChannelShell) session1.openChannel("shell");
					//BufferedReader in1=new BufferedReader(new InputStreamReader(channel1.getInputStream()));
					OutputStream out1 = channel1.getOutputStream();
					PrintStream commander1 = new PrintStream(out1, true);
					channel1.connect();
            
					for(int i=1;i<=50;i++){
						Thread.sleep(10000);
						long before = System.currentTimeMillis();
						commander1.println("java -jar /home/pi/env/rti/SimpleDDSPublisher3/SimpleDDS.jar example_"+i);
						before = before + 1555;
						u.writeFile(base+"temp/reg/example"+i+".txt", ""+before);
						before = before + 5100;
						u.writeFile(base+"temp/unreg/example"+i+".txt", ""+before);
						System.out.println("Service: example" +i+ " --- Round: "+round );
					
					}
					Thread.sleep(10000);
					commander1.close();
				
					/*b = true;
	        		ser = 0;
	        		while ((line = in1.readLine()) != null && b){
	        	    	System.out.print(line+"\n");
	        	    	if(line.equals("DATAWRITER_CREATED")){
	        	    		long before = System.currentTimeMillis();
	        	    		ser ++;
	        	    		u.writeFile(base+"temp/reg/example"+ser+".txt", ""+before);
	        	    	}
	        	    	if(line.equals("DATAWRITER_DELETED")){
	        	    		long before = System.currentTimeMillis();
	        	    		u.writeFile(base+"temp/unreg/example"+ser+".txt", ""+before);
	        	    	}
	        	    	if(line.equals("pi@raspberrypi:~$ ")&&ser==100)break;
	        		}*/
					channel1.disconnect();
					session1.disconnect();
					
					/*for(int i=1;i<=100;i++){
						responseTimeRegisterGw1.add(u.readFile(base+"temp/reg/example"+i+".txt"));
						responseTimeUnregisterGw1.add(u.readFile(base+"temp/unreg/example"+i+".txt"));
		        	}
	        	
	        		System.out.println("\n-----------------------WRITING FILES PERFORMANCE TESTS GW1/"+round+"-------------------------");
					u.writeFile(base+round+"/20/Gw1/ResponseTimeRegister.txt", responseTimeRegisterGw1);
					u.writeFile(base+round+"/20/Gw1/ResponseTimeUnRegister.txt", responseTimeUnregisterGw1);*/
	        	}
	        }catch(Exception e){
	        	e.printStackTrace();
	        }
			
		}
	}

}
