package surf.tests.dnssd;

//import java.io.IOException;

import com.apple.dnssd.DNSSDService;
import com.apple.dnssd.ResolveListener;
import com.apple.dnssd.TXTRecord;

import surf.util.Util;

public class DNSResolve implements ResolveListener {

	private String base = "D:/experiments/results/dnssd/";
	
	public DNSResolve(){
		
	}
	
	@Override
	public void operationFailed(DNSSDService service, int errorCode) {
		// TODO Auto-generated method stub
		System.out.println("Resolve failed " + errorCode);
	}

	@Override
	public void serviceResolved(DNSSDService resolver, int flags, int ifIndex, String fullName, String hostName, int port, TXTRecord txtRecord) {
		// TODO Auto-generated method stub
		Util u = new Util();
		long total = 0;
		System.out.println(fullName);
		String [] name = fullName.split("\\.");
		System.out.println("*************"+name[0]);
		try {
			if(fullName.contains("example") && u.readFile(base+"temp/dis/ban.txt").equals("0")){
				long after = System.currentTimeMillis();
				u.writeFile(base+"temp/dis/ban.txt", "1");
				try {
					String res = u.readFile(base+"temp/dis/"+name[0]+".txt").replaceAll("[\n\r]","");;
					long before = Long.parseLong(res);
					total = after - before;
					u.writeFile(base+"temp/dis/"+name[0]+".txt", ""+total);
					u.writeFile(base+"temp/dis/"+name[0]+"r.txt", ""+after);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("Resolve listener");
				System.out.println("Service Resolved  : " + fullName + ":"+port);
				System.out.println("Host Name  : " + hostName);
				System.out.println("TXTRecord  : " + txtRecord);
				System.out.println("TOTAL  : " + total);
				System.out.println("End Resolve listener");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	
	public void dispose() {
		// TODO Auto-generated method stub
		try {
			this.finalize();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			System.out.println("Problems in destructor Resolve");
			e.printStackTrace();
		}
		
	}

}
