package surf.tests.dnssd;

import com.apple.dnssd.DNSSDService;
import com.apple.dnssd.QueryListener;

public class DNSQuery implements QueryListener {

	@Override
	public void operationFailed(DNSSDService service, int errorCode) {
		// TODO Auto-generated method stub
		System.out.println("Query failed " + errorCode);		
	}

	@Override
	public void queryAnswered(DNSSDService query, int flags, int ifIndex, String fullName, int rrtype, int rrclass, byte[] rdata, int ttl) {
		// / TODO Auto-generated method stub
		System.out.println("Query listener");
		System.out.println("Service Name  : " + fullName);
		System.out.println("End Query listener");
	}

}
