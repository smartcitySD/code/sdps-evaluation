package surf.tests.dnssd;

import com.apple.dnssd.DNSSDRegistration;
import com.apple.dnssd.DNSSDService;
import com.apple.dnssd.RegisterListener;

import surf.util.Util;

public class DNSRegister implements RegisterListener {
	
	private String base = "D:/experiments/results/mdns/";
	
	public DNSRegister(){
		
	}
	
	@Override
	public void operationFailed(DNSSDService service, int errorCode) {
		// TODO Auto-generated method stub
		System.out.println("Registration failed " + errorCode);		
	}

	@Override
	public void serviceRegistered(DNSSDRegistration registration, int flags, String serviceName, String regType, String domain){
		// TODO Auto-generated method stub
		Util u = new Util();
		
		try {
			if(serviceName.contains("example") && u.readFile(base+"temp/reg/ban.txt").equals("0")){
				long after = System.currentTimeMillis();
				u.writeFile(base+"temp/reg/ban.txt", "1");
				try {
					String res = u.readFile(base+"temp/reg/"+serviceName+".txt").replaceAll("[\n\r]","");
					long before = Long.parseLong(res);
					long total = after - before;
					u.writeFile(base+"temp/reg/"+serviceName+".txt", ""+total);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Registered Name  : " + serviceName);
		System.out.println("           Type  : " + regType);
		System.out.println("           Domain: " + domain);
	}

	public void dispose() {
		// TODO Auto-generated method stub
		try {
			this.finalize();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			System.out.println("Problems in destructor Register");
			e.printStackTrace();
		}
		
	}
}
