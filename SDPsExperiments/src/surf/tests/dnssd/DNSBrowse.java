package surf.tests.dnssd;

import java.io.IOException;

import com.apple.dnssd.BrowseListener;
import com.apple.dnssd.DNSSDService;

import surf.util.Util;

public class DNSBrowse implements BrowseListener {

	private String base = "D:/experiments/results/mdns/";
	
	public DNSBrowse(){
		
	}
	
	@Override
	public void operationFailed(DNSSDService service, int errorCode) {
		// TODO Auto-generated method stub
		System.out.println("Browsing failed " + errorCode);
	}

	@Override
	public void serviceFound(DNSSDService browser, int flags, int ifIndex, String serviceName, String regType, String domain) {
		// TODO Auto-generated method stub
		Util u = new Util();
		try {
			//if(u.readFile(base+"temp/exp.txt").equals("1")){
				if(serviceName.contains("example") && u.readFile(base+"temp/bro/ban.txt").equals("0")){
					long after = System.currentTimeMillis();
					u.writeFile(base+"temp/bro/ban.txt", "1");
					try {
						String res = u.readFile(base+"temp/bro/"+serviceName+".txt").replaceAll("[\n\r]","");;
						long before = Long.parseLong(res);
						long total = after - before;
						u.writeFile(base+"temp/bro/"+serviceName+".txt", ""+total);
					} catch (IOException e) {
						//TODO Auto-generated catch block
						e.printStackTrace();
					} 
				}
			/*}
			else{
				if(serviceName.contains("AirTemperature"))u.writeFile(base+"temp/prec_rec/tp/"+serviceName+".txt", "1");
				else u.writeFile(base+"temp/prec_rec/fn/"+serviceName+".txt", "1");
			}*/
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		System.out.println("Browse listener");
		System.out.println("Service Found  : " + serviceName);
		System.out.println("End Browse listener");
		
	}

	@Override
	public void serviceLost(DNSSDService browser, int flags, int ifIndex, String serviceName, String regType, String domain){
		// TODO Auto-generated method stub
		Util u = new Util();
		
		try {
			if(serviceName.contains("example") && u.readFile(base+"temp/unreg/ban.txt").equals("0")){
				long after = System.currentTimeMillis();
				u.writeFile(base+"temp/unreg/ban.txt", "1");
				try {
					String res = u.readFile(base+"temp/unreg/"+serviceName+".txt").replaceAll("[\n\r]","");;
					long before = Long.parseLong(res);
					long total = after - before;
					u.writeFile(base+"temp/unreg/"+serviceName+".txt", ""+total);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
			}
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Browse listener");
		System.out.println("Service Lost  : " + serviceName);
		System.out.println("End Browse listener");
	}
	
	public void dispose() {
		// TODO Auto-generated method stub
		try {
			this.finalize();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			System.out.println("Problems in destructor Browse");
			e.printStackTrace();
		}
		
	}

}
