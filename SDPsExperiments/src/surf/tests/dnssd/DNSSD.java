package surf.tests.dnssd;

/*import java.io.IOException;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.jmdns.JmDNS;
import javax.jmdns.ServiceInfo;

import com.apple.dnssd.DNSSDException;
import com.apple.dnssd.DNSSDRegistration;
import com.apple.dnssd.DNSSDService;
import com.apple.dnssd.TXTRecord;
import surf.data.entity.Service;*/
//import surf.data.logic.Extract;
import surf.util.Util;

public class DNSSD {

	public DNSSD() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		String base = "D:/experiments/results/dnssd/";
		int round = 0;
		//Extract extract = new Extract();
		Util u = new Util();
		u.delete(base+"temp/dis");
		u.delete(base+"temp/reg");
		u.delete(base+"temp/unreg");
		
		//extract.extractData();
			
		/*System.out.println("\n-----------------------PERFORMANCE TESTS-------------------------");
		u.writeFile(base+"temp/exp.txt", "1");
		for(int n=10; n<=10;n+=200){
			ArrayList<DNSSDRegistration> registers = new ArrayList<>();
			round ++;
			System.out.println("-----------------------ROUND "+ round +" N=" + n + "-------------------------");
			ArrayList<String> responseTimeRegisterGw1 = new ArrayList<String>();
			ArrayList<String> responseTimeDiscoveryGw1 = new ArrayList<String>();
			ArrayList<String> responseTimeUnregisterGw1 = new ArrayList<String>();
			ArrayList<String> responseTimeBrowserGw1 = new ArrayList<String>();
			ArrayList<ArrayList<Integer>> gatewaysDataPoints = u.selectData(n,0);
			
			ArrayList<Integer> gateway1DataPoints = gatewaysDataPoints.get(0);
			System.out.println("-----------------------DATAPOINTS GENERATED-------------------------");
			System.out.println("-----------------------REGISTERING SERVICES GW1-------------------------");
			ArrayList<Service> servicesGW1 = u.getServices(gateway1DataPoints,1);
			
			u.writeFile(base+"temp/reg/ban.txt", "-1");
			u.writeFile(base+"temp/unreg/ban.txt", "-1");
			u.writeFile(base+"temp/dis/ban.txt", "-1");
			u.writeFile(base+"temp/bro/ban.txt", "-1");
			try {
				for(int i=0;i<servicesGW1.size();i++){
					DNSRegister register = new DNSRegister();
					int port = 2000 + i;
					System.out.println("Name: " +servicesGW1.get(i).getId());
					System.out.println("Type: _"+servicesGW1.get(i).getName()+"._tcp");
					TXTRecord txt = new TXTRecord();
					txt.set("uri", servicesGW1.get(i).getUri());
					txt.set("title", servicesGW1.get(i).getTitle());
					txt.set("description", servicesGW1.get(i).getDescription());
					txt.set("unit", servicesGW1.get(i).getUnit());
					txt.set("rt", "" + servicesGW1.get(i).getRt());
					txt.set("rel", "" + servicesGW1.get(i).getRb());
					txt.set("locationX", servicesGW1.get(i).getLocationX());
					txt.set("locationY", servicesGW1.get(i).getLocationY());
					txt.set("locationZ", servicesGW1.get(i).getLocationZ());
					txt.set("input", servicesGW1.get(i).getInput());
					txt.set("output", servicesGW1.get(i).getOutput());
					r = com.apple.dnssd.DNSSD.register( 0, 10, servicesGW1.get(i).getId(), "_example._tcp",  "local", "192.168.1.100", port, txt, register);
					registers.add(r);
					//register.dispose();
				}
				Thread.sleep(3000);
				
				for(int j = 0; j<5; j++){
					DNSRegister register = new DNSRegister();
					DNSResolve resolve = new DNSResolve();
					DNSBrowse browse = new DNSBrowse();
					int port = 3000 + j;
					TXTRecord txt = new TXTRecord();
					txt.set("uri", "http://surf.dnssd/exammple_"+j);
					txt.set("title", "Example_"+j);
					txt.set("description", "This is the example " +j);
					txt.set("unit", "*U");
					txt.set("rt", "" + "900");
					txt.set("rel", "" + "0.5");
					txt.set("locationX", "52.236");
					txt.set("locationY", "-15.25");
					txt.set("locationZ", "3.5");
					txt.set("input", "string");
					txt.set("output", "int");
					u.writeFile(base+"temp/reg/ban.txt", "0");
					u.writeFile(base+"temp/reg/example_"+j+".txt", ""+System.currentTimeMillis());
					r = com.apple.dnssd.DNSSD.register(0, 10, "example_"+j, "_examples._tcp", null, null, port, txt, new DNSRegister());
					//registers.add(r);
					Thread.sleep(3000);
					u.writeFile(base+"temp/dis/ban.txt", "0");
					u.writeFile(base+"temp/dis/example_"+j+"s.txt", ""+System.currentTimeMillis());
					u.writeFile(base+"temp/dis/example_"+j+".txt", ""+System.currentTimeMillis());
					res = com.apple.dnssd.DNSSD.resolve(0, 10, "example_"+j, "_examples._tcp", "local", new DNSResolve());
					u.writeFile(base+"temp/bro/ban.txt", "0");
					u.writeFile(base+"temp/bro/example_"+j+".txt", ""+System.currentTimeMillis());
					b = com.apple.dnssd.DNSSD.browse("_examples._tcp", new DNSBrowse());
					Thread.sleep(3000);
					u.writeFile(base+"temp/unreg/ban.txt", "0");
					u.writeFile(base+"temp/unreg/example_"+j+".txt", ""+System.currentTimeMillis());
					r.stop();
					Thread.sleep(3000);
					res.stop();
					b.stop();*/
					//registers.remove(registers.size()-1);
					/*register.dispose();
					resolve.dispose();
					browse.dispose();*/
				/*}
				for(int i=0;i<registers.size();i++){
					registers.get(i).stop();
					//registers.remove(i);
				}
				Thread.sleep(10000);
													
				for(int j=0; j<5;j++){
					responseTimeRegisterGw1.add(u.readFile(base+"temp/reg/example_"+j+".txt").replaceAll("[\n\r]",""));
					responseTimeDiscoveryGw1.add(u.readFile(base+"temp/dis/example_"+j+".txt").replaceAll("[\n\r]",""));
					responseTimeUnregisterGw1.add(u.readFile(base+"temp/unreg/example_"+j+".txt").replaceAll("[\n\r]",""));
					responseTimeBrowserGw1.add(u.readFile(base+"temp/bro/example_"+j+".txt").replaceAll("[\n\r]",""));
				}
				
				u.delete(base+"temp/dis");
				u.delete(base+"temp/reg");
				u.delete(base+"temp/unreg");
				u.delete(base+"temp/bro");
			
				System.out.println("\n-----------------------WRITING FILES PERFORMANCE TESTS GW1/"+round+"-------------------------");
				u.writeFile(base+round+"/Gw1/ResponseTimeRegister.txt", responseTimeRegisterGw1);
				u.writeFile(base+round+"/Gw1/ResponseTimeUnRegister.txt", responseTimeUnregisterGw1);
				u.writeFile(base+round+"/Gw1/ResponseTime.txt", responseTimeDiscoveryGw1);
				u.writeFile(base+round+"/Gw1/ResponseTimeBrowsing.txt", responseTimeBrowserGw1);
			}
			catch(Exception e3){
				e3.printStackTrace();
			}
		}*/
		
		/*System.out.println("\n-----------------------PRECISION AND RECALL TESTS-------------------------");
		u.writeFile(base+"temp/exp.txt", "2");
		ArrayList<String> precisionGw1_t1 = new ArrayList<String>();
		ArrayList<String> precisionGw1_t2 = new ArrayList<String>();
		ArrayList<String> precisionGw1_t3 = new ArrayList<String>();
		ArrayList<String> recallGw1_t1 = new ArrayList<String>();
		ArrayList<String> recallGw1_t2 = new ArrayList<String>();
		ArrayList<String> recallGw1_t3 = new ArrayList<String>();
		ArrayList<Service> servicesGW1 = new ArrayList<Service>();
		ArrayList<DNSSDRegistration> registers = new ArrayList<>();
		
		double relevants = 0;
		double precisionGw1 = 0;
		double recallGw1 = 0;
		double true_positivesGw1 = 0;
		double false_negativesGw1 = 0;
		int n = 100;
		
		for(int i=0; i<100; i++){
			ArrayList<ArrayList<Integer>> gatewaysDataPoints =u.selectData(n,0);
			ArrayList<Integer> gateway1DataPoints = gatewaysDataPoints.get(0);
			System.out.println("-----------------------DATAPOINTS GENERATED-------------------------");
			System.out.println("-----------------------REGISTERING SERVICES GW1 TEST 1-------------------------");
			servicesGW1 = u.getServices(gateway1DataPoints,1);
			for(int j=0; j<servicesGW1.size();j++){
				if(servicesGW1.get(j).getDescription().contains("AirTemperature"))relevants++;
			}
			
			DNSBrowse browse = new DNSBrowse();
			
			try {
				for(int j=0;j<servicesGW1.size();j++){
					int port = 2000 + j;
					DNSRegister register = new DNSRegister();
					System.out.println("Name: " +servicesGW1.get(j).getId());
					System.out.println("Type: _"+servicesGW1.get(j).getName()+"._tcp");
					TXTRecord txt = new TXTRecord();
					txt.set("uri", servicesGW1.get(j).getUri());
					txt.set("title", servicesGW1.get(j).getId());
					txt.set("description", servicesGW1.get(j).getDescription());
					txt.set("unit", servicesGW1.get(j).getUnit());
					txt.set("rt", "" + servicesGW1.get(j).getRt());
					txt.set("rel", "" + servicesGW1.get(j).getRb());
					txt.set("locationX", servicesGW1.get(j).getLocationX());
					txt.set("locationY", servicesGW1.get(j).getLocationY());
					txt.set("locationZ", servicesGW1.get(j).getLocationZ());
					txt.set("input", servicesGW1.get(j).getInput());
					txt.set("output", servicesGW1.get(j).getOutput());
					DNSSDRegistration r1 = com.apple.dnssd.DNSSD.register(0, 10, servicesGW1.get(j).getId(), "_"+servicesGW1.get(j).getName()+"._tcp", null, null, port, txt, register);
					Thread.sleep(3000);
					registers.add(r1);
					register.dispose();
				}
				DNSSDService b1 = com.apple.dnssd.DNSSD.browse("_AirTemperature._tcp", browse);
				Thread.sleep(20000);
				b1.stop();
				
				for(int j=0;j<registers.size();j++){
					registers.get(j).stop();
					//registers.remove(i);
				}
				Thread.sleep(10000);
				browse.dispose();
				
				try {
					File folderTP = new File(base+"temp/prec_rec/tp/");;
					File folderFN = new File(base+"temp/prec_rec/fn/");;
					true_positivesGw1 = folderTP.listFiles().length;
					false_negativesGw1 = folderFN.listFiles().length;
				} catch (Exception e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
				precisionGw1 = true_positivesGw1/(true_positivesGw1+false_negativesGw1);
				recallGw1=true_positivesGw1/relevants;
				precisionGw1_t1.add(""+precisionGw1);
				recallGw1_t1.add(""+recallGw1);
						
				u.delete(base+"temp/reg/");
				u.delete(base+"temp/unreg/");
				u.delete(base+"temp/dis/");
				u.delete(base+"temp/prec_rec/tp/");
				u.delete(base+"temp/prec_rec/fn/");
			
				relevants = 0;
				true_positivesGw1 = 0;
				false_negativesGw1 = 0;
				precisionGw1 = 0;
				recallGw1 = 0;
				
				gatewaysDataPoints =u.selectData(n,0);
				gateway1DataPoints = gatewaysDataPoints.get(0);
				System.out.println("-----------------------DATAPOINTS GENERATED-------------------------");
				System.out.println("-----------------------REGISTERING SERVICES GW1 TEST 2-------------------------");
				servicesGW1.addAll(u.getServices(gateway1DataPoints,2));
				for(int j=0; j<servicesGW1.size();j++){
					if(servicesGW1.get(j).getDescription().contains("AirTemperature"))relevants++;
				}
				
				for(int j=0;j<servicesGW1.size();j++){
					int port = 2000 + j;
					DNSRegister register = new DNSRegister();
					System.out.println("Name: " +servicesGW1.get(j).getId());
					System.out.println("Type: _"+servicesGW1.get(j).getName()+"._tcp");
					TXTRecord txt = new TXTRecord();
					txt.set("uri", servicesGW1.get(j).getUri());
					txt.set("title", servicesGW1.get(j).getId());
					txt.set("description", servicesGW1.get(j).getDescription());
					txt.set("unit", servicesGW1.get(j).getUnit());
					txt.set("rt", "" + servicesGW1.get(j).getRt());
					txt.set("rel", "" + servicesGW1.get(j).getRb());
					txt.set("locationX", servicesGW1.get(j).getLocationX());
					txt.set("locationY", servicesGW1.get(j).getLocationY());
					txt.set("locationZ", servicesGW1.get(j).getLocationZ());
					txt.set("input", servicesGW1.get(j).getInput());
					txt.set("output", servicesGW1.get(j).getOutput());
					DNSSDRegistration r1 = com.apple.dnssd.DNSSD.register(0, 10, servicesGW1.get(j).getId(), "_"+servicesGW1.get(j).getName()+"._tcp", null, null, port, txt, register);
					Thread.sleep(3000);
					registers.add(r1);
					register.dispose();
				}
				b1 = com.apple.dnssd.DNSSD.browse("_AirTemperature._tcp", browse);
				Thread.sleep(20000);
				b1.stop();
				
				for(int j=0;j<registers.size();j++){
					registers.get(j).stop();
					//registers.remove(i);
				}
				Thread.sleep(10000);
				browse.dispose();
				
				try {
					File folderTP = new File(base+"temp/prec_rec/tp/");;
					File folderFN = new File(base+"temp/prec_rec/fn/");;
					true_positivesGw1 = folderTP.listFiles().length;
					false_negativesGw1 = folderFN.listFiles().length;
				} catch (Exception e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
				precisionGw1 = true_positivesGw1/(true_positivesGw1+false_negativesGw1);
				recallGw1=true_positivesGw1/relevants;
				precisionGw1_t2.add(""+precisionGw1);
				recallGw1_t2.add(""+recallGw1);
						
				u.delete(base+"temp/reg/");
				u.delete(base+"temp/unreg/");
				u.delete(base+"temp/dis/");
				u.delete(base+"temp/prec_rec/tp/");
				u.delete(base+"temp/prec_rec/fn/");
			
				relevants = 0;
				true_positivesGw1 = 0;
				false_negativesGw1 = 0;
				precisionGw1 = 0;
				recallGw1 = 0;
			
				gatewaysDataPoints =u.selectData(n,0);
				gateway1DataPoints = gatewaysDataPoints.get(0);
				System.out.println("-----------------------DATAPOINTS GENERATED-------------------------");
				System.out.println("-----------------------REGISTERING SERVICES GW1 TEST 3-------------------------");
				servicesGW1.addAll(u.getServices(gateway1DataPoints,3));
				for(int j=0; j<servicesGW1.size();j++){
					if(servicesGW1.get(j).getDescription().contains("AirTemperature"))relevants++;
				}
				
				for(int j=0;j<servicesGW1.size();j++){
					int port = 2000 + j;
					DNSRegister register = new DNSRegister();
					System.out.println("Name: " +servicesGW1.get(j).getId());
					System.out.println("Type: _"+servicesGW1.get(j).getName()+"._tcp");
					TXTRecord txt = new TXTRecord();
					txt.set("uri", servicesGW1.get(j).getUri());
					txt.set("title", servicesGW1.get(j).getId());
					txt.set("description", servicesGW1.get(j).getDescription());
					txt.set("unit", servicesGW1.get(j).getUnit());
					txt.set("rt", "" + servicesGW1.get(j).getRt());
					txt.set("rel", "" + servicesGW1.get(j).getRb());
					txt.set("locationX", servicesGW1.get(j).getLocationX());
					txt.set("locationY", servicesGW1.get(j).getLocationY());
					txt.set("locationZ", servicesGW1.get(j).getLocationZ());
					txt.set("input", servicesGW1.get(j).getInput());
					txt.set("output", servicesGW1.get(j).getOutput());
					DNSSDRegistration r1 = com.apple.dnssd.DNSSD.register(0, 10, servicesGW1.get(j).getId(), "_"+servicesGW1.get(j).getName()+"._tcp", null, null, port, txt, register);
					Thread.sleep(3000);
					registers.add(r1);
					register.dispose();
				}
				b1 = com.apple.dnssd.DNSSD.browse("_AirTemperature._tcp", browse);
				Thread.sleep(20000);
				b1.stop();
				
				for(int j=0;j<registers.size();j++){
					registers.get(j).stop();
					//registers.remove(i);
				}
				Thread.sleep(10000);
				browse.dispose();
				
				try {
					File folderTP = new File(base+"temp/prec_rec/tp/");;
					File folderFN = new File(base+"temp/prec_rec/fn/");;
					true_positivesGw1 = folderTP.listFiles().length;
					false_negativesGw1 = folderFN.listFiles().length;
				} catch (Exception e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
				precisionGw1 = true_positivesGw1/(true_positivesGw1+false_negativesGw1);
				recallGw1=true_positivesGw1/relevants;
				precisionGw1_t3.add(""+precisionGw1);
				recallGw1_t3.add(""+recallGw1);
						
				u.delete(base+"temp/reg/");
				u.delete(base+"temp/unreg/");
				u.delete(base+"temp/dis/");
				u.delete(base+"temp/prec_rec/tp/");
				u.delete(base+"temp/prec_rec/fn/");
			
				relevants = 0;
				true_positivesGw1 = 0;
				false_negativesGw1 = 0;
				precisionGw1 = 0;
				recallGw1 = 0;
			}
			catch(Exception ex){
				ex.printStackTrace();
			}
		}
	
		System.out.println("\n-----------------------WRITING FILES PRECISION AND RECALL GW1-------------------------");
		u.writeFile(base+"prec_recall/Gw1/1/Precision.txt", precisionGw1_t1);
		u.writeFile(base+"prec_recall/Gw1/2/Precision.txt", precisionGw1_t2);
		u.writeFile(base+"prec_recall/Gw1/3/Precision.txt", precisionGw1_t3);
		u.writeFile(base+"prec_recall/Gw1/1/Recall.txt", recallGw1_t1);
		u.writeFile(base+"prec_recall/Gw1/2/Recall.txt", recallGw1_t2);
		u.writeFile(base+"prec_recall/Gw1/3/Recall.txt", recallGw1_t3);*/
		
	}

}
