package surf.tests.coap;

import java.util.ArrayList;
import surf.data.entity.Service;
import surf.util.Util;

public class CoAPTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Client c = new Client("coap://134.226.36.181:55683/");
		//String base = "/home/ubuntu/experiments/";
		String base = "D:/experiments/results/coap/";
		int round = 6;
		int subround = 0;
		Util u = new Util();
		ArrayList<Service> servicesGW1 = new ArrayList<Service>();
		ArrayList<Service> servicesGW2 = new ArrayList<Service>();
		
		System.out.println("-----------------------CLIENTS STARTING-------------------------\n");
		CoAPClient c1 = new CoAPClient("coap://134.226.36.181:45683/WeatherTest/");
		//CoAPClient c1 = new CoAPClient("coap://localhost:5683/WeatherTest/");
		CoAPClient c2 = new CoAPClient("coap://134.226.36.181:55683/WeatherTest/");
		//CoAPClient c2 = new CoAPClient("coap://localhost:5683/WeatherTest/");
		
		System.out.println("-----------------------CLIENTS STARTED-------------------------\n");
		
		System.out.println("\n-----------------------PERFORMANCE TESTS-------------------------");
		for(int n=1600; n<=2000;n+=200){
			round ++;
			subround = 0;  
			System.out.println("-----------------------ROUND "+ round +" N=" + n + "-------------------------");
			ArrayList<String> responseTimeRegisterGw1 = new ArrayList<String>();
			ArrayList<String> responseTimeRegisterGw2 = new ArrayList<String>();
			/*ArrayList<String> responseTimeGw1 = new ArrayList<String>();
			ArrayList<String> responseTimeGw2 = new ArrayList<String>();*/
			ArrayList<String> responseTimeUnregisterGw1 = new ArrayList<String>();
			ArrayList<String> responseTimeUnregisterGw2 = new ArrayList<String>();
			for(double i=0; i<0.1; i+=0.1){
				subround ++;
				System.out.println("-----------------------DELETING CURRENT SERVICES-------------------------");
				c1.Delete();
				c2.Delete();
				
				System.out.println("-----------------------SUB-ROUND "+ subround +"-------------------------");
				System.out.println("-----------------------GENERATING DATAPOINTS-------------------------");
				ArrayList<ArrayList<Integer>> gatewaysDataPoints =u.selectData(n, i);
				ArrayList<Integer> gateway1DataPoints = gatewaysDataPoints.get(0);
				ArrayList<Integer> gateway2DataPoints = gatewaysDataPoints.get(1);
				System.out.println("-----------------------DATAPOINTS GENERATED-------------------------");
				System.out.println("-----------------------REGISTERING SERVICES GW1-------------------------");
				servicesGW1 = u.getServices(gateway1DataPoints,1);
				c1.Register(servicesGW1);
				System.out.println("-----------------------SERVICES REGISTERED GW1-------------------------");
				System.out.println("-----------------------REGISTERING SERVICES GW2-------------------------");
				servicesGW2 = u.getServices(gateway2DataPoints,1);
				c2.Register(servicesGW2);
				System.out.println("-----------------------SERVICES REGISTERED GW2-------------------------");
				
				for(int j = 0; j< 100; j++){
					
					System.out.println("-----------------------SUB-ROUND "+ (j+1) +"-------------------------");
					Service s = new Service("FakeService001", "FakeService001", "FakeService", "FakeService", "It is a fake service", "*C", 800, 0.35, "-53", "0", "2600", "int", "int,string");
					System.out.println("-----------------------REGISTERING FAKE SERVICE GW1-------------------------");
					String r1 = c1.Register(s);
					long total = Long.parseLong(r1);
					responseTimeRegisterGw1.add(""+total);
					System.out.println("-----------------------FAKE SERVICE REGISTERED GW1-------------------------");
					System.out.println("-----------------------REGISTERING FAKE SERVICE GW2-------------------------");
					String r2 = c2.Register(s);
					total = Long.parseLong(r2);
					responseTimeRegisterGw2.add(""+total);
					System.out.println("-----------------------FAKE SERVICE REGISTERED GW2-------------------------");
					
					System.out.println("-----------------------UNREGISTERING FAKE SERVICE GW1-------------------------");
					r1 = c1.Unregister(s);
					total = Long.parseLong(r1);
					responseTimeUnregisterGw1.add(""+total);
					System.out.println("-----------------------FAKE SERVICE REGISTERED GW1-------------------------");
					System.out.println("-----------------------REGISTERING FAKE SERVICE GW2-------------------------");
					r2 = c2.Unregister(s);
					total = Long.parseLong(r2);
					responseTimeUnregisterGw2.add(""+total);
					System.out.println("-----------------------FAKE SERVICE REGISTERED GW2-------------------------");
					
					/*System.out.println("\n-----------------------STARTING DISCOVERY GW1 " + j +"-------------------------");
					try{
					String[] rs1 = c1.Discover("AirTemperature").split(",");
					responseTimeGw1.add(rs1[2]);
					}
					catch(Exception ex){
						responseTimeGw1.add("-");
					}
					System.out.println("\n-----------------------FINISHING DISCOVERY GW1 " + j +"-------------------------");
					System.out.println("\n-----------------------STARTING DISCOVERY GW2 " + j +"-------------------------");
					try{
						String[] rs2 = c2.Discover("AirTemperature").split(",");
						responseTimeGw2.add(rs2[2]);
					}
					catch(Exception ex){
						responseTimeGw2.add("-");
					}
					System.out.println("\n-----------------------FINISHING DISCOVERY GW2 " + j +"-------------------------");		*/
				}
				
			}
			System.out.println("\n-----------------------WRITING FILES GW1-------------------------");
			u.writeFile(base+round+"/Gw1/ResponseTimeRegister.txt", responseTimeRegisterGw1);
			u.writeFile(base+round+"/Gw1/ResponseTimeUnRegister.txt", responseTimeUnregisterGw1);
			//u.writeFile(base+round+"/Gw1/ResponseTime.txt", responseTimeGw1);
							
			System.out.println("\n-----------------------WRITING FILES GW2-------------------------");
			u.writeFile(base+round+"/Gw2/ResponseTimeRegister.txt", responseTimeRegisterGw2);
			u.writeFile(base+round+"/Gw2/ResponseTimeUnRegister.txt", responseTimeUnregisterGw2);
			//u.writeFile(base+round+"/Gw2/ResponseTime.txt", responseTimeGw2);
		}
		c1.Delete();
		c2.Delete();
		System.out.println("\n-----------------------END PERFORMANCE TESTS-------------------------");
		
		System.out.println("\n-----------------------PRECISION AND RECALL TESTS-------------------------");
		ArrayList<String> precisionGw1_t1 = new ArrayList<String>();
		ArrayList<String> precisionGw2_t1 = new ArrayList<String>();
		ArrayList<String> precisionGw1_t2 = new ArrayList<String>();
		ArrayList<String> precisionGw2_t2 = new ArrayList<String>();
		ArrayList<String> precisionGw1_t3 = new ArrayList<String>();
		ArrayList<String> precisionGw2_t3 = new ArrayList<String>();
		ArrayList<String> recallGw1_t1 = new ArrayList<String>();
		ArrayList<String> recallGw2_t1 = new ArrayList<String>();
		ArrayList<String> recallGw1_t2 = new ArrayList<String>();
		ArrayList<String> recallGw2_t2 = new ArrayList<String>();
		ArrayList<String> recallGw1_t3 = new ArrayList<String>();
		ArrayList<String> recallGw2_t3 = new ArrayList<String>();
		double relevantsGw1 = 0;
		double relevantsGw2 = 0;
		double precisionGw1 = 0;
		double precisionGw2 = 0;
		double recallGw1 = 0;
		double recallGw2 = 0;
		double true_positivesGw1 = 0;
		double true_positivesGw2 = 0;
		double false_negativesGw1 = 0;
		double false_negativesGw2 = 0;
		int n = 100;
		
		for(int i=0; i<100; i++){
			System.out.println("-----------------------STARTING TEST 1-------------------------");
			ArrayList<ArrayList<Integer>> gatewaysDataPoints =u.selectData(n,0);
			ArrayList<Integer> gateway1DataPoints = gatewaysDataPoints.get(0);
			ArrayList<Integer> gateway2DataPoints = gatewaysDataPoints.get(1);
			System.out.println("-----------------------DATAPOINTS GENERATED-------------------------");
			System.out.println("-----------------------REGISTERING SERVICES GW1 TEST 1- "+(i+1)+" -------------------------");
			servicesGW1=u.getServices(gateway1DataPoints,1);
			System.out.println("-----------------------REGISTERING SERVICES GW2 TEST 1- "+(i+1)+" -------------------------");
			servicesGW2=u.getServices(gateway2DataPoints,1);
			
			relevantsGw1 = Double.parseDouble(c1.Register(servicesGW1));
			relevantsGw2 = Double.parseDouble(c2.Register(servicesGW2));
			
			System.out.println("-----------------------DISCOVERING SERVICES GW1 TEST 1- "+(i+1)+" -------------------------");
			String[] res1 = c1.Discover("AirTemperature").split(",");
			true_positivesGw1 = Double.parseDouble(res1[0]);
			false_negativesGw1 = Double.parseDouble(res1[1]);
			System.out.println("-----------------------DISCOVERING SERVICES GW2 TEST 1- "+(i+1)+" -------------------------");
			String[] res2 = c2.Discover("AirTemperature").split(",");
			true_positivesGw2 = Double.parseDouble(res2[0]);
			false_negativesGw2 = Double.parseDouble(res2[1]);
						
			precisionGw1 = true_positivesGw1/(true_positivesGw1+false_negativesGw1);
			precisionGw2 = true_positivesGw2/(true_positivesGw2+false_negativesGw2);
			recallGw1 = true_positivesGw1/relevantsGw1;
			recallGw2 = true_positivesGw2/relevantsGw2;
			precisionGw1_t1.add(""+precisionGw1);
			precisionGw2_t1.add(""+precisionGw2);
			recallGw1_t1.add(""+recallGw1);
			recallGw2_t1.add(""+recallGw2);
			
			System.out.println("-----------------------DELETING SERVICES GW1 TEST 1- "+(i+1)+" -------------------------");
			c1.Delete();
			System.out.println("-----------------------DELETING SERVICES GW2 TEST 1- "+(i+1)+" -------------------------");
			c2.Delete();
			
			System.out.println("-----------------------STARTING TEST 2-------------------------");
			relevantsGw1 = 0;
			relevantsGw2 = 0;
			true_positivesGw1 = 0;
			true_positivesGw2 = 0;
			false_negativesGw1 = 0;
			false_negativesGw2 = 0;
			precisionGw1 = 0;
			precisionGw2 = 0;
			recallGw1 = 0;
			recallGw2 = 0;
		
			gatewaysDataPoints =u.selectData(n,0);
			gateway1DataPoints = gatewaysDataPoints.get(0);
			gateway2DataPoints = gatewaysDataPoints.get(1);
			System.out.println("-----------------------DATAPOINTS GENERATED-------------------------");
			System.out.println("-----------------------REGISTERING SERVICES GW1 TEST 2- "+(i+1)+" -------------------------");
			servicesGW1.addAll(u.getServices(gateway1DataPoints,2));
			System.out.println("-----------------------REGISTERING SERVICES GW2 TEST 2- "+(i+1)+" -------------------------");
			servicesGW2.addAll(u.getServices(gateway2DataPoints,2));
						
			relevantsGw1 = Double.parseDouble(c1.Register(servicesGW1));
			relevantsGw2 = Double.parseDouble(c2.Register(servicesGW2));
			
			System.out.println("-----------------------DISCOVERING SERVICES GW1 TEST 2- "+(i+1)+" -------------------------");
			res1 = c1.Discover("AirTemperature").split(",");
			true_positivesGw1 = Double.parseDouble(res1[0]);
			false_negativesGw1 = Double.parseDouble(res1[1]);
			System.out.println("-----------------------DISCOVERING SERVICES GW2 TEST 2- "+(i+1)+" -------------------------");
			res2 = c2.Discover("AirTemperature").split(",");
			true_positivesGw2 = Double.parseDouble(res2[0]);
			false_negativesGw2 = Double.parseDouble(res2[1]);
						
			precisionGw1 = true_positivesGw1/(true_positivesGw1+false_negativesGw1);
			precisionGw2 = true_positivesGw2/(true_positivesGw2+false_negativesGw2);
			recallGw1 = true_positivesGw1/relevantsGw1;
			recallGw2 = true_positivesGw2/relevantsGw2;
			precisionGw1_t2.add(""+precisionGw1);
			precisionGw2_t2.add(""+precisionGw2);
			recallGw1_t2.add(""+recallGw1);
			recallGw2_t2.add(""+recallGw2);
			
			System.out.println("-----------------------DELETING SERVICES GW1 TEST 2- "+(i+1)+" -------------------------");
			c1.Delete();
			System.out.println("-----------------------DELETING SERVICES GW2 TEST 2- "+(i+1)+" -------------------------");
			c2.Delete();

			System.out.println("-----------------------STARTING TEST 3-------------------------");
			relevantsGw1 = 0;
			relevantsGw2 = 0;
			true_positivesGw1 = 0;
			true_positivesGw2 = 0;
			false_negativesGw1 = 0;
			false_negativesGw2 = 0;
			precisionGw1 = 0;
			precisionGw2 = 0;
			recallGw1 = 0;
			recallGw2 = 0;
			
			gatewaysDataPoints =u.selectData(n,0);
			gateway1DataPoints = gatewaysDataPoints.get(0);
			gateway2DataPoints = gatewaysDataPoints.get(1);
			System.out.println("-----------------------DATAPOINTS GENERATED-------------------------");
			System.out.println("-----------------------REGISTERING SERVICES GW1 TEST 3- "+(i+1)+" -------------------------");
			servicesGW1.addAll(u.getServices(gateway1DataPoints,3));
			System.out.println("-----------------------REGISTERING SERVICES GW2 TEST 3- "+(i+1)+" -------------------------");
			servicesGW2.addAll(u.getServices(gateway2DataPoints,3));
						
			relevantsGw1 = Double.parseDouble(c1.Register(servicesGW1));
			relevantsGw2 = Double.parseDouble(c2.Register(servicesGW2));
			
			System.out.println("-----------------------DISCOVERING SERVICES GW1 TEST 2- "+(i+1)+" -------------------------");
			res1 = c1.Discover("AirTemperature").split(",");
			true_positivesGw1 = Double.parseDouble(res1[0]);
			false_negativesGw1 = Double.parseDouble(res1[1]);
			System.out.println("-----------------------DISCOVERING SERVICES GW2 TEST 2- "+(i+1)+" -------------------------");
			res2 = c2.Discover("AirTemperature").split(",");
			true_positivesGw2 = Double.parseDouble(res2[0]);
			false_negativesGw2 = Double.parseDouble(res2[1]);
						
			precisionGw1 = true_positivesGw1/(true_positivesGw1+false_negativesGw1);
			precisionGw2 = true_positivesGw2/(true_positivesGw2+false_negativesGw2);
			recallGw1 = true_positivesGw1/relevantsGw1;
			recallGw2 = true_positivesGw2/relevantsGw2;
			precisionGw1_t3.add(""+precisionGw1);
			precisionGw2_t3.add(""+precisionGw2);
			recallGw1_t3.add(""+recallGw1);
			recallGw2_t3.add(""+recallGw2);
			
			System.out.println("-----------------------DELETING SERVICES GW1 TEST 3- "+(i+1)+" -------------------------");
			c1.Delete();
			System.out.println("-----------------------DELETING SERVICES GW2 TEST 3- "+(i+1)+" -------------------------");
			c2.Delete();
						
			relevantsGw1 = 0;
			relevantsGw2 = 0;
			true_positivesGw1 = 0;
			true_positivesGw2 = 0;
			false_negativesGw1 = 0;
			false_negativesGw2 = 0;
			precisionGw1 = 0;
			precisionGw2 = 0;
			recallGw1 = 0;
			recallGw2 = 0;
		}
		
		System.out.println("-----------------------DELETING SERVICES GW1 AT THE END-------------------------");
		c1.Delete();
		System.out.println("-----------------------DELETING SERVICES GW2 AT THE END-------------------------");
		c2.Delete();
	
		System.out.println("\n-----------------------WRITING FILES PRECISION AND RECALL GW1-------------------------");
		u.writeFile(base+"prec_recall/Gw1/1/Precision.txt", precisionGw1_t1);
		u.writeFile(base+"prec_recall/Gw1/2/Precision.txt", precisionGw1_t2);
		u.writeFile(base+"prec_recall/Gw1/3/Precision.txt", precisionGw1_t3);
		u.writeFile(base+"prec_recall/Gw1/1/Recall.txt", recallGw1_t1);
		u.writeFile(base+"prec_recall/Gw1/2/Recall.txt", recallGw1_t2);
		u.writeFile(base+"prec_recall/Gw1/3/Recall.txt", recallGw1_t3);
		
		System.out.println("\n-----------------------WRITING FILES PRECISION AND RECALL GW2-------------------------");
		u.writeFile(base+"prec_recall/Gw2/1/Precision.txt", precisionGw2_t1);
		u.writeFile(base+"prec_recall/Gw2/2/Precision.txt", precisionGw2_t2);
		u.writeFile(base+"prec_recall/Gw2/3/Precision.txt", precisionGw2_t3);
		u.writeFile(base+"prec_recall/Gw2/1/Recall.txt", recallGw2_t1);
		u.writeFile(base+"prec_recall/Gw2/2/Recall.txt", recallGw2_t2);
		u.writeFile(base+"prec_recall/Gw2/3/Recall.txt", recallGw2_t3);
		
	}

}
