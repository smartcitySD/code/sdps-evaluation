package surf.tests.coap;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Set;

import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapResponse;
import org.eclipse.californium.core.WebLink;
import org.json.JSONException;
import org.json.JSONObject;

import surf.data.entity.Service;

public class CoAPClient {

	protected CoapClient client;
	private URI uri = null;
	
	public CoAPClient(String server) {
		// TODO Auto-generated constructor stub
				
				try {
					uri = new URI(server);
				} catch (URISyntaxException e) {
					System.err.println("Invalid URI: " + e.getMessage());
					System.exit(-1);
				}
				client = new CoapClient(uri);
	}
	
	public String Register(ArrayList<Service> services){
		int relevants=0;
		
		for(int i=0; i<services.size(); i++){
			Register(services.get(i));
			if(services.get(i).getDescription().contains("AirTemperature"))relevants++;
		}
		return ""+relevants;
	}
	
	public String Discover(String query){
		int fn = 0;
		int tp = 0;
		long after = 0;
		long total = 0;
		long before = System.currentTimeMillis();
		
		try{
			Set<WebLink> links = client.discover("title="+query);
			after = System.currentTimeMillis();
			total = after - before;
			String result = links.toString().replace("[", "").replace("]", "");
			System.out.println(result);
			String [] results = result.split(",");
			for(int i=0; i<results.length; i++){
				String r[] = results[i].split(">");
				if(r[0].contains("AirTemperature")) tp++;
				else fn ++;
			}
			return tp + "," + fn + "," + total;
		}
		catch(Exception ex){
			return "fail";
		}
	}
	
	public String Delete(){
		long it = 0;
		long et = 0;
		long rtt = 0;
		CoapResponse response;
		try{
			it = System.currentTimeMillis();
			//response = client.delete();
			/*Request request = new Request(Code.DELETE);
			request.setURI(uri);
			request.setPayload(resource);
			request.send();*/
			response=client.delete("",0);
			et = System.currentTimeMillis();
			rtt = et - it;
			System.out.println(response.advanced().getType() + "-" + response.getCode() + "-" + response.getResponseText());
			System.out.println(response.getResponseText());
			return "" + rtt;
		}
		catch(Exception ex){
			return Delete();
		}
	}
	
	public String Register(Service s) {
		CoapResponse response;
		long total = 0;
		
		JSONObject service = new JSONObject();
		JSONObject attributes = new JSONObject();
		
		try {
			service.append("href", s.getId());
			service.append("rt", s.getUri());
			attributes.append("responseTime", s.getRt());
			attributes.append("reliability", s.getRb());
			attributes.append("locationX", s.getLocationX());
			attributes.append("locationY", s.getLocationY());
			attributes.append("locationZ", s.getLocationZ());
			attributes.append("description", s.getDescription());
			attributes.append("unit", s.getUnit());
			attributes.append("title", s.getName());
			service.append("attributes", attributes);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String message = service.toString().replace("[", "").replace("]", "");
		System.out.println(message);
		
		long before = System.currentTimeMillis();
		response = client.post(message,50);
		long after = System.currentTimeMillis();
		total = after - before;
		
		System.out.println(response.advanced().getType() + "-" + response.getCode()+ "-" + response.getResponseText());
		System.out.println(response.getResponseText());
		return "" + total;
	}

	public String Unregister(Service s) {
		CoapResponse response;
		long total = 0;
		
		JSONObject service = new JSONObject();
		JSONObject attributes = new JSONObject();
		try {
			service.append("href", s.getUri());
			service.append("rt", s.getName());
			attributes.append("responseTime", s.getRt());
			attributes.append("reliability", s.getRb());
			attributes.append("locationX", s.getLocationX());
			attributes.append("locationY", s.getLocationY());
			attributes.append("locationZ", s.getLocationZ());
			attributes.append("description", s.getDescription());
			attributes.append("unit", s.getUnit());
			attributes.append("title", s.getTitle());
			service.append("attributes", attributes);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String message = service.toString().replace("[", "").replace("]", "");
		System.out.println(message);
		long before = System.currentTimeMillis();
		response = client.delete(message, 1);
		long after = System.currentTimeMillis();
		total = after - before;
		System.out.println(response.advanced().getType() + "-" + response.getCode() + "-" + response.getResponseText());
		System.out.println(response.getResponseText());
		return "" + total;
}


}
