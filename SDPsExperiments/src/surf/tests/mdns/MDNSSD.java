package surf.tests.mdns;

import java.util.ArrayList;

import com.apple.dnssd.DNSSDRegistration;
//import com.apple.dnssd.DNSSDService;
import com.apple.dnssd.TXTRecord;

import surf.data.entity.Service;
//import surf.tests.dnssd.DNSBrowse;
import surf.tests.dnssd.DNSRegister;
//import surf.data.logic.Extract;
import surf.util.Util;

public class MDNSSD {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String base = "D:/experiments/results/mdns/";
		int round = 0;
		long before = 0;
		/*long after = 0;
		long total = 0;*/
		//Extract extract = new Extract();
		Util u = new Util();
		u.delete(base+"temp/dis");
		u.delete(base+"temp/reg");
		u.delete(base+"temp/unreg");
		
		DNSSDRegistration r = null;
		//DNSSDService res = null;
		//DNSSDService b = null;
		//int nodes = 5;
				
		for(int n=200; n<=2000;n+=200){
			ArrayList<DNSSDRegistration> registers = new ArrayList<>();
			/*try {
				Thread.sleep(10000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}*/
			round ++;
			u.writeFile(base+"temp/round.txt", ""+round);
			System.out.println("-----------------------ROUND "+ round +" N=" + n + "-------------------------");
			ArrayList<String> responseTimeRegisterGw1 = new ArrayList<String>();
			//ArrayList<String> responseTimeDiscoveryGw1 = new ArrayList<String>();
			ArrayList<String> responseTimeUnregisterGw1 = new ArrayList<String>();
			//ArrayList<String> responseTimeBrowserGw1 = new ArrayList<String>();
						
			ArrayList<ArrayList<Integer>> gatewaysDataPoints = u.selectData(n,0);
			ArrayList<Integer> gateway1DataPoints = gatewaysDataPoints.get(0);
			System.out.println("-----------------------DATAPOINTS GENERATED-------------------------");
			System.out.println("-----------------------REGISTERING SERVICES GW1-------------------------");
			ArrayList<Service> servicesGW1 = u.getServices(gateway1DataPoints,1);
			
			u.writeFile(base+"temp/reg/ban.txt", "-1");
			u.writeFile(base+"temp/unreg/ban.txt", "-1");
			u.writeFile(base+"temp/dis/ban.txt", "-1");
			u.writeFile(base+"temp/bro/ban.txt", "-1");
			
			try {
				for(int i=0;i<servicesGW1.size();i++){
					DNSRegister register = new DNSRegister();
					int port = 2000 + i;
					System.out.println("Name: " +servicesGW1.get(i).getId());
					System.out.println("Type: _"+servicesGW1.get(i).getName()+"._tcp");
					TXTRecord txt = new TXTRecord();
					txt.set("uri", servicesGW1.get(i).getUri());
					txt.set("title", servicesGW1.get(i).getTitle());
					txt.set("description", servicesGW1.get(i).getDescription());
					txt.set("unit", servicesGW1.get(i).getUnit());
					txt.set("rt", "" + servicesGW1.get(i).getRt());
					txt.set("rel", "" + servicesGW1.get(i).getRb());
					txt.set("locationX", servicesGW1.get(i).getLocationX());
					txt.set("locationY", servicesGW1.get(i).getLocationY());
					txt.set("locationZ", servicesGW1.get(i).getLocationZ());
					txt.set("input", servicesGW1.get(i).getInput());
					txt.set("output", servicesGW1.get(i).getOutput());
					r = com.apple.dnssd.DNSSD.register( 0, 3, servicesGW1.get(i).getId(), "_example._tcp",  "local", "192.168.1.100", port, txt, register);
					registers.add(r);
					//register.dispose();
				}
				
				System.out.println("-----------------------WAITING-------------------------");
				System.out.println(""+System.currentTimeMillis());
				Thread.sleep((60000*round));
				/*System.out.println(""+System.currentTimeMillis());
				Thread.sleep(60000);*/
				System.out.println("-----------------------REGISTERED " + n + " SERVICES GW1-------------------------");
				
				for(int i=1; i<=100;i++){
					DNSRegister register = new DNSRegister();
					//DNSResolve resolve = new DNSResolve();
					//DNSBrowse browse = new DNSBrowse();
					TXTRecord txt = new TXTRecord();
					txt.set("uri", "http://surf.dnssd/exammple_"+i);
					txt.set("title", "Example_"+i);
					txt.set("description", "This is the example " +i);
					txt.set("unit", "*U");
					txt.set("rt", "" + "900");
					txt.set("rel", "" + "0.5");
					txt.set("locationX", "52.236");
					txt.set("locationY", "-15.25");
					txt.set("locationZ", "3.5");
					txt.set("input", "string");
					txt.set("output", "int");
					try {
						before = System.currentTimeMillis();
						u.writeFile(base+"temp/reg/example"+i+".txt", ""+before);
						u.writeFile(base+"temp/reg/ban.txt", "0");
						u.writeFile(base+"temp/reg/example_ssh"+i+".txt", ""+before);
						r = com.apple.dnssd.DNSSD.register( 0, 10,"example"+i, "_example._tcp",  "local", "192.168.1.100", 2020, txt, register);
						//Thread.sleep(2000);
						//b = com.apple.dnssd.DNSSD.browse("_example._tcp", browse);
						Thread.sleep(3000);
						before = System.currentTimeMillis();
						u.writeFile(base+"temp/unreg/example"+i+".txt", ""+before);
						u.writeFile(base+"temp/unreg/ban.txt", "0");
						u.writeFile(base+"temp/unreg/example_ssh"+i+".txt", ""+before);
						r.stop();
						Thread.sleep(3000);
						//b.stop();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
						
				}
				
				//Thread.sleep(240000);
													
				for(int i=1; i<=100;i++){
					responseTimeRegisterGw1.add(u.readFile(base+"temp/reg/example"+i+".txt").replaceAll("[\n\r]",""));
					//responseTimeDiscoveryGw1.add(u.readFile(base+"temp/dis/example"+i+".txt").replaceAll("[\n\r]",""));
					responseTimeUnregisterGw1.add(u.readFile(base+"temp/unreg/example"+i+".txt").replaceAll("[\n\r]",""));
					//responseTimeBrowserGw1.add(u.readFile(base+"temp/bro/example"+i+".txt").replaceAll("[\n\r]",""));
				}
				
				u.delete(base+"temp/dis");
				u.delete(base+"temp/reg");
				u.delete(base+"temp/unreg");
				u.delete(base+"temp/bro");
			
				System.out.println("\n-----------------------WRITING FILES PERFORMANCE TESTS GW1/"+round+"-------------------------");
				u.writeFile(base+round+"/5/Gw1/ResponseTimeRegister.txt", responseTimeRegisterGw1);
				u.writeFile(base+round+"/5/Gw1/ResponseTimeUnRegister.txt", responseTimeUnregisterGw1);
				//u.writeFile(base+round+"/15/Gw1/ResponseTime.txt", responseTimeDiscoveryGw1);
				//u.writeFile(base+round+"/15/Gw1/ResponseTimeBrowsing.txt", responseTimeBrowserGw1);
				
				for(int i=0;i<registers.size();i++){
					registers.get(i).stop();
					//registers.remove(i);
				}
			}
			catch(Exception e3){
				e3.printStackTrace();
			}
		}
		
		
		/*for(int i=1; i<=100;i++){
		long time = 0;
		TXTRecord txt = new TXTRecord();
		txt.set("uri", "http://");
		txt.set("title", "title");
		txt.set("description", "desc");
		txt.set("unit", "unit");
		txt.set("rt", "" + "rt");
		txt.set("rel", "" + "rel");
		txt.set("locationX", "x");
		txt.set("locationY", "y");
		txt.set("locationZ", "z");
		txt.set("input", "in");
		txt.set("output", "out");
		try {
			r = com.apple.dnssd.DNSSD.register( 0, 10,"example"+i, "_example._tcp",  "local", "192.168.1.100", 2020, txt, register);
			time = System.currentTimeMillis();
			startTimesRegister.add(""+time);
			Thread.sleep(10000);
			time = System.currentTimeMillis();
			startTimesUnregister.add(""+time);
			r.stop();
			//Thread.sleep(3000);
			} catch (DNSSDException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}*/
	
		/*u.writeFile(base+"temp/startTimesRegister.txt", startTimesRegister);
		u.writeFile(base+"temp/startTimesUnregister.txt", startTimesUnregister);*/
		
		//extract.extractData();
		
		/*System.out.println("\n-----------------------PERFORMANCE TESTS-------------------------");
		for(int n=3; n<=3;n+=500){
			round ++;
			System.out.println("-----------------------ROUND "+ round +" N=" + n + "-------------------------");
			ArrayList<String> responseTimeRegisterGw1 = new ArrayList<String>();
			//ArrayList<String> responseTimeRegisterGw2 = new ArrayList<String>();
			ArrayList<String> responseTimeDiscoveryGw1 = new ArrayList<String>();
			//ArrayList<String> responseTimeDiscoveryGw2 = new ArrayList<String>();
			ArrayList<String> responseTimeUnregisterGw1 = new ArrayList<String>();
			//ArrayList<String> responseTimeUnregisterGw2 = new ArrayList<String>();
			
			ArrayList<ArrayList<Integer>> gatewaysDataPoints =u.selectData(n,0);
			ArrayList<Integer> gateway1DataPoints = gatewaysDataPoints.get(0);
			System.out.println("-----------------------DATAPOINTS GENERATED-------------------------");
			System.out.println("-----------------------REGISTERING SERVICES GW1-------------------------");
			ArrayList<Service> servicesGW1 = u.getServices(gateway1DataPoints,1);
			
			try {
				JmDNS jmdnsGW1 = JmDNS.create("192.168.1.100");
				jmdnsGW1.unregisterAllServices();
				for(int i=0; i<servicesGW1.size();i++){
					System.out.println("-----------------------Service "+(i+1)+"-------------------------");
					int port = 2000 + i;
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("uri", servicesGW1.get(i).getUri());
					map.put("title", servicesGW1.get(i).getTitle());
					map.put("description", servicesGW1.get(i).getDescription());
					map.put("unit", servicesGW1.get(i).getUnit());
					map.put("rt", "" + servicesGW1.get(i).getRt());
					map.put("rel", "" +servicesGW1.get(i).getRb());
					map.put("locationX", servicesGW1.get(i).getLocationX());
					map.put("locationY", servicesGW1.get(i).getLocationY());
					map.put("locationZ", servicesGW1.get(i).getLocationZ());
					map.put("input", servicesGW1.get(i).getInput());
					map.put("output", servicesGW1.get(i).getOutput());
					ServiceInfo info = ServiceInfo.create("_"+servicesGW1.get(i).getName()+"._tcp.local.",servicesGW1.get(i).getId(),port,100,100,map);
					
					try {
						//u.writeFile(base+"temp/reg/reg_"+servicesGW1.get(i).getId().replace("_","")+".txt", ""+0);
						jmdnsGW1.registerService(info);
					} catch (IOException e1) {
						// 	TODO Auto-generated catch block
						e1.printStackTrace();
						System.out.println("Problems registering service in round: " + round);
					}
				}
			
				for(int j = 0; j< 10; j++){
					System.out.println("-----------------------SUB-ROUND "+ (j+1) +"-------------------------");
					System.out.println("-----------------------Registering Fake Service-------------------------");
					int port = 2000 + j;
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("uri", "http://surf/FAKE_AirTemperature/");
					map.put("title", "AirTemperature");
					map.put("description", "This service monitors AirTemperature");
					map.put("unit", "*C");
					map.put("rt", "" + "591");
					map.put("rel", "" + "0.737");
					map.put("locationX", "-85.2733");
					map.put("locationY", "35.6819");
					map.put("locationZ", "1780");
					map.put("input", "string");
					map.put("output", "int,locationX,locationY,locationZ");
					ServiceInfo info = ServiceInfo.create("_AirTemperature._tcp.local.","FAKE_AirTemperature",port,100,100,map);
					try {
						before = System.currentTimeMillis();
						u.writeFile(base+"temp/reg/reg_FAKEAirTemperature.txt", ""+before);
						jmdnsGW1.registerService(info);
						after = System.currentTimeMillis();
						total = after - before;
					} catch (IOException e1) {
						// 	TODO Auto-generated catch block
						e1.printStackTrace();
						System.out.println("Problems registering service in round: " + round);
					}
					responseTimeRegisterGw1.add("" + total);
					//Thread t = new Thread();
					/*try {
						t.sleep(25000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}*/
					/*responseTimeRegisterGw2.add(u.readFile(base+"temp/reg/reg_FAKEAirTemperature.txt").replaceAll("[\n\r]",""));
					responseTimeDiscoveryGw2.add(u.readFile(base+"temp/dis/dis_FAKEAirTemperature.txt").replaceAll("[\n\r]",""));*/
					/*u.delete(base+"temp/reg/");
					u.delete(base+"temp/dis/");
					jmdnsGW1.close();
					
					JmDNS jmdnsGWSD=JmDNS.create("192.168.1.100");
					System.out.println("-----------------------SUB-ROUND "+ (j+1) +"-------------------------");
					System.out.println("-----------------------Service Discovery-------------------------");
					before = System.currentTimeMillis();
					//ServiceInfo [] serviceInfos = jmdnsGWSD.list("_AirTemperature._tcp.local.");
					ServiceInfo service = jmdnsGWSD.getServiceInfo("_AirTemperature._tcp.local.","FAKE_AirTemperature");
					after = System.currentTimeMillis();
					total=after-before;
					responseTimeDiscoveryGw1.add(""+total);
					//for (ServiceInfo service : serviceInfos) {
						System.out.println("## resolve service " + service.getName()  + "##");
						System.out.println("-- Description: " + service.getPropertyString("description"));
						System.out.println("-- URI: " + service.getPropertyString("uri"));
						System.out.println("-- Title: " + service.getPropertyString("title"));
						System.out.println("-- Unit: " + service.getPropertyString("unit"));
						System.out.println("-- Response Time: " + service.getPropertyString("rt"));
						System.out.println("-- Reliability: " + service.getPropertyString("rel"));
						System.out.println("-- Latitude: " + service.getPropertyString("locationX"));
						System.out.println("-- Longitude: " + service.getPropertyString("locationY"));
						System.out.println("-- Altitude: " + service.getPropertyString("locationZ"));
						System.out.println("-- Input: " + service.getPropertyString("input"));
						System.out.println("-- Output: " + service.getPropertyString("output"));
					//}
					jmdnsGWSD.close();
					jmdnsGW1 = JmDNS.create("192.168.1.100"); 
					System.out.println("-----------------------SUB-ROUND "+ (j+1) +"-------------------------");
					System.out.println("-----------------------Unregistering Fake Service-------------------------");
					before = System.currentTimeMillis();
					//u.writeFile(base+"temp/unreg/unreg_FAKEAirTemperature.txt", ""+before);
					jmdnsGW1.unregisterService(info);
					after = System.currentTimeMillis();
					total = after - before;
					responseTimeUnregisterGw1.add("" + total);
					/*t = new Thread();
					try {
						t.sleep(25000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}*/
					//responseTimeUnregisterGw2.add(u.readFile(base+"temp/unreg/unreg_FAKEAirTemperature.txt").replaceAll("[\n\r]",""));
				/*}
				
				System.out.println("-----------------------UNREGISTERING SERVICES GW1-------------------------");
				for(int i=0; i<servicesGW1.size();i++){
					System.out.println("-----------------------Service "+(i+1)+"-------------------------");
					int port = 2000 + i;
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("uri", servicesGW1.get(i).getUri());
					map.put("title", servicesGW1.get(i).getTitle());
					map.put("description", servicesGW1.get(i).getDescription());
					map.put("unit", servicesGW1.get(i).getUnit());
					map.put("rt", "" + servicesGW1.get(i).getRt());
					map.put("rel", "" +servicesGW1.get(i).getRb());
					map.put("locationX", servicesGW1.get(i).getLocationX());
					map.put("locationY", servicesGW1.get(i).getLocationY());
					map.put("locationZ", servicesGW1.get(i).getLocationZ());
					map.put("input", servicesGW1.get(i).getInput());
					map.put("output", servicesGW1.get(i).getOutput());
					ServiceInfo info = ServiceInfo.create("_"+servicesGW1.get(i).getName()+"._tcp.local.",servicesGW1.get(i).getId(),port,100,100,map);
					//u.writeFile(base+"temp/unreg/unreg_"+servicesGW1.get(i).getId().replace("_","")+".txt", ""+0);
					jmdnsGW1.unregisterService(info);
				}
				Thread.sleep(25000);
				System.out.println("-----------------------CLOSING JMDNS-------------------------");
				u.delete(base+"temp/unreg/");
				jmdnsGW1.close();
				
				System.out.println("\n-----------------------WRITING FILES PERFORMANCE TESTS GW1-------------------------");
				u.writeFile(base+round+"/Gw1/ResponseTimeRegister.txt", responseTimeRegisterGw1);
				u.writeFile(base+round+"/Gw1/ResponseTimeUnRegister.txt", responseTimeUnregisterGw1);
				u.writeFile(base+round+"/Gw1/ResponseTime.txt", responseTimeDiscoveryGw1);
				
				/*System.out.println("\n-----------------------WRITING FILES PERFORMANCE TESTS GW2-------------------------");
				u.writeFile(base+round+"/Gw2/ResponseTimeRegister.txt", responseTimeRegisterGw2);
				u.writeFile(base+round+"/Gw2/ResponseTimeUnRegister.txt", responseTimeUnregisterGw2);
				u.writeFile(base+round+"/Gw2/ResponseTime.txt", responseTimeDiscoveryGw2);*/
			/*}
			catch(Exception ex){
				ex.printStackTrace();
				System.out.println("Problems creating JmDNS at round: " + round);
			}
		}
		u.delete(base+"temp/prec_rec/tp/");
		u.delete(base+"temp/prec_rec/fn/");
		System.out.println("\n-----------------------END PERFORMANCE TESTS-------------------------");
		
		/*System.out.println("\n-----------------------PRECISION AND RECALL TESTS-------------------------");
		ArrayList<String> precisionGw1_t1 = new ArrayList<String>();
		ArrayList<String> precisionGw2_t1 = new ArrayList<String>();
		ArrayList<String> precisionGw1_t2 = new ArrayList<String>();
		ArrayList<String> precisionGw2_t2 = new ArrayList<String>();
		ArrayList<String> precisionGw1_t3 = new ArrayList<String>();
		ArrayList<String> precisionGw2_t3 = new ArrayList<String>();
		ArrayList<String> recallGw1_t1 = new ArrayList<String>();
		ArrayList<String> recallGw2_t1 = new ArrayList<String>();
		ArrayList<String> recallGw1_t2 = new ArrayList<String>();
		ArrayList<String> recallGw2_t2 = new ArrayList<String>();
		ArrayList<String> recallGw1_t3 = new ArrayList<String>();
		ArrayList<String> recallGw2_t3 = new ArrayList<String>();
		ArrayList<Service> servicesGW1 = new ArrayList<Service>();
		double relevants = 0;
		double precisionGw1 = 0;
		double precisionGw2 = 0;
		double recallGw1 = 0;
		double recallGw2 = 0;
		double true_positivesGw1 = 0;
		double true_positivesGw2 = 0;
		double false_negativesGw1 = 0;
		double false_negativesGw2 = 0;
		int n = 100;
		
		for(int i=0; i<100; i++){
			ArrayList<ArrayList<Integer>> gatewaysDataPoints =u.selectData(n,0);
			ArrayList<Integer> gateway1DataPoints = gatewaysDataPoints.get(0);
			System.out.println("-----------------------DATAPOINTS GENERATED-------------------------");
			System.out.println("-----------------------REGISTERING SERVICES GW1 TEST 1-------------------------");
			servicesGW1 = u.getServices(gateway1DataPoints,1);
			for(int j=0; j<servicesGW1.size();j++){
				if(servicesGW1.get(j).getDescription().contains("AirTemperature"))relevants++;
			}
						
			try {
				JmDNS jmdnsGW1 = JmDNS.create("192.168.1.100");
				for(int x=0; x<servicesGW1.size();x++){
					System.out.println("-----------------------Service "+(x+1)+"-------------------------");
					int port = 2000 + x;
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("uri", servicesGW1.get(x).getUri());
					map.put("title", servicesGW1.get(x).getTitle());
					map.put("description", servicesGW1.get(x).getDescription());
					map.put("unit", servicesGW1.get(x).getUnit());
					map.put("rt", "" + servicesGW1.get(x).getRt());
					map.put("rel", "" +servicesGW1.get(x).getRb());
					map.put("locationX", servicesGW1.get(x).getLocationX());
					map.put("locationY", servicesGW1.get(x).getLocationY());
					map.put("locationZ", servicesGW1.get(x).getLocationZ());
					map.put("input", servicesGW1.get(x).getInput());
					map.put("output", servicesGW1.get(x).getOutput());
					ServiceInfo info = ServiceInfo.create("_"+servicesGW1.get(x).getName()+"._tcp.local.",servicesGW1.get(x).getId(),port,100,100,map);
					
					try {
						u.writeFile(base+"temp/reg/reg_"+servicesGW1.get(x).getId().replace("_","")+".txt", "0");
						jmdnsGW1.registerService(info);
					} 
					catch (IOException e1) {
						// 	TODO Auto-generated catch block
						e1.printStackTrace();
						System.out.println("Problems registering service in round: " + i);
					}
				}
			
				ServiceInfo[] serviceInfos = jmdnsGW1.list("_AirTemperature._tcp.local.");
				for (ServiceInfo service : serviceInfos) {
					System.out.println("## resolve service " + service.getName()  + " : " + service.getURL());
					System.out.println("-- Description: " + service.getPropertyString("description"));
					System.out.println("-- URI: " + service.getPropertyString("uri"));
					System.out.println("-- Title: " + service.getPropertyString("title"));
					System.out.println("-- Unit: " + service.getPropertyString("unit"));
					System.out.println("-- Response Time: " + service.getPropertyString("rt"));
					System.out.println("-- Reliability: " + service.getPropertyString("rel"));
					System.out.println("-- Latitude: " + service.getPropertyString("locationX"));
					System.out.println("-- Longitude: " + service.getPropertyString("locationY"));
					System.out.println("-- Altitude: " + service.getPropertyString("locationZ"));
					System.out.println("-- Input: " + service.getPropertyString("input"));
					System.out.println("-- Output: " + service.getPropertyString("output"));
					
					if(service.getPropertyString("description").contains("AirTemperature"))true_positivesGw1 ++;
					else false_negativesGw1++;
				}
				
				for(int j=0; j<servicesGW1.size();j++){
					System.out.println("-----------------------Service "+(j+1)+"-------------------------");
					int port = 2000 + j;
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("uri", servicesGW1.get(j).getUri());
					map.put("title", servicesGW1.get(j).getTitle());
					map.put("description", servicesGW1.get(j).getDescription());
					map.put("unit", servicesGW1.get(j).getUnit());
					map.put("rt", "" + servicesGW1.get(j).getRt());
					map.put("rel", "" +servicesGW1.get(j).getRb());
					map.put("locationX", servicesGW1.get(j).getLocationX());
					map.put("locationY", servicesGW1.get(j).getLocationY());
					map.put("locationZ", servicesGW1.get(j).getLocationZ());
					map.put("input", servicesGW1.get(j).getInput());
					map.put("output", servicesGW1.get(j).getOutput());
					ServiceInfo info = ServiceInfo.create("_"+servicesGW1.get(j).getName()+"._tcp.local.",servicesGW1.get(j).getId(),port,100,100,map);
					u.writeFile(base+"temp/unreg/unreg_"+servicesGW1.get(j).getId().replace("_","")+".txt", "0");
					jmdnsGW1.unregisterService(info);
				}
				jmdnsGW1.close();
			}
			catch(Exception ex){
				ex.printStackTrace();
			}
			
			Thread t = new Thread();
			try {
				t.sleep(20000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			precisionGw1 = true_positivesGw1/(true_positivesGw1+false_negativesGw1);
			recallGw1 = true_positivesGw1/relevants;
			precisionGw1_t1.add(""+precisionGw1);
			recallGw1_t1.add(""+recallGw1);
			try {
				File folderTP = new File(base+"temp/prec_rec/tp/");;
				File folderFN = new File(base+"temp/prec_rec/fn/");;
				true_positivesGw2 = folderTP.listFiles().length;
				false_negativesGw2 = folderFN.listFiles().length;
			} catch (Exception e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			precisionGw2 = true_positivesGw2/(true_positivesGw2+false_negativesGw2);
			recallGw2=true_positivesGw2/relevants;
			precisionGw2_t1.add(""+precisionGw2);
			recallGw2_t1.add(""+recallGw2);
						
			u.delete(base+"temp/reg/");
			u.delete(base+"temp/unreg/");
			u.delete(base+"temp/dis/");
			u.delete(base+"temp/prec_rec/tp/");
			u.delete(base+"temp/prec_rec/fn/");
			
			relevants = 0;
			true_positivesGw1 = 0;
			true_positivesGw2 = 0;
			false_negativesGw1 = 0;
			false_negativesGw2 = 0;
			precisionGw1 = 0;
			precisionGw2 = 0;
			recallGw1 = 0;
			recallGw2 = 0;
		
			gatewaysDataPoints =u.selectData(n,0);
			gateway1DataPoints = gatewaysDataPoints.get(0);
			System.out.println("-----------------------DATAPOINTS GENERATED-------------------------");
			System.out.println("-----------------------REGISTERING SERVICES GW1 TEST 2-------------------------");
			servicesGW1.addAll(u.getServices(gateway1DataPoints,2));
			for(int j=0; j<servicesGW1.size();j++){
				if(servicesGW1.get(j).getDescription().contains("AirTemperature"))relevants++;
			}
			
			
			try {
				JmDNS jmdnsGW1 = JmDNS.create("192.168.1.100");
				for(int x=0; x<servicesGW1.size();x++){
					System.out.println("-----------------------Service "+(x+1)+"-------------------------");
					int port = 2000 + x;
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("uri", servicesGW1.get(x).getUri());
					map.put("title", servicesGW1.get(x).getTitle());
					map.put("description", servicesGW1.get(x).getDescription());
					map.put("unit", servicesGW1.get(x).getUnit());
					map.put("rt", "" + servicesGW1.get(x).getRt());
					map.put("rel", "" +servicesGW1.get(x).getRb());
					map.put("locationX", servicesGW1.get(x).getLocationX());
					map.put("locationY", servicesGW1.get(x).getLocationY());
					map.put("locationZ", servicesGW1.get(x).getLocationZ());
					map.put("input", servicesGW1.get(x).getInput());
					map.put("output", servicesGW1.get(x).getOutput());
					ServiceInfo info = ServiceInfo.create("_"+servicesGW1.get(x).getName()+"._tcp.local.",servicesGW1.get(x).getId(),port,100,100,map);
					
					try {
						u.writeFile(base+"temp/reg/reg_"+servicesGW1.get(x).getId().replace("_","")+".txt", "0");
						jmdnsGW1.registerService(info);
					} 
					catch (IOException e1) {
						// 	TODO Auto-generated catch block
						e1.printStackTrace();
						System.out.println("Problems registering service in round: " + i);
					}
				}
				ServiceInfo[] serviceInfos = jmdnsGW1.list("_AirTemperature._tcp.local.");
				for (ServiceInfo service : serviceInfos) {
					System.out.println("## resolve service " + service.getName()  + " : " + service.getURL());
					System.out.println("-- Description: " + service.getPropertyString("description"));
					System.out.println("-- URI: " + service.getPropertyString("uri"));
					System.out.println("-- Title: " + service.getPropertyString("title"));
					System.out.println("-- Unit: " + service.getPropertyString("unit"));
					System.out.println("-- Response Time: " + service.getPropertyString("rt"));
					System.out.println("-- Reliability: " + service.getPropertyString("rel"));
					System.out.println("-- Latitude: " + service.getPropertyString("locationX"));
					System.out.println("-- Longitude: " + service.getPropertyString("locationY"));
					System.out.println("-- Altitude: " + service.getPropertyString("locationZ"));
					System.out.println("-- Input: " + service.getPropertyString("input"));
					System.out.println("-- Output: " + service.getPropertyString("output"));
						
					if(service.getPropertyString("description").contains("AirTemperature"))true_positivesGw1 ++;
					else false_negativesGw1++;
				}
					
				for(int j=0; j<servicesGW1.size();j++){
					System.out.println("-----------------------Service "+(j+1)+"-------------------------");
					int port = 2000 + j;
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("uri", servicesGW1.get(j).getUri());
					map.put("title", servicesGW1.get(j).getTitle());
					map.put("description", servicesGW1.get(j).getDescription());
					map.put("unit", servicesGW1.get(j).getUnit());
					map.put("rt", "" + servicesGW1.get(j).getRt());
					map.put("rel", "" +servicesGW1.get(j).getRb());
					map.put("locationX", servicesGW1.get(j).getLocationX());
					map.put("locationY", servicesGW1.get(j).getLocationY());
					map.put("locationZ", servicesGW1.get(j).getLocationZ());
					map.put("input", servicesGW1.get(j).getInput());
					map.put("output", servicesGW1.get(j).getOutput());
					ServiceInfo info = ServiceInfo.create("_"+servicesGW1.get(j).getName()+"._tcp.local.",servicesGW1.get(j).getId(),port,100,100,map);
					u.writeFile(base+"temp/unreg/unreg_"+servicesGW1.get(j).getId().replace("_","")+".txt", "0");
					jmdnsGW1.unregisterService(info);
				}
				jmdnsGW1.close();
			}
			catch(Exception ex){
				ex.printStackTrace();
			}	
			
			t = new Thread();
			try {
				t.sleep(20000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			precisionGw1 = true_positivesGw1/(true_positivesGw1+false_negativesGw1);
			recallGw1 = true_positivesGw1/relevants;
			precisionGw1_t2.add(""+precisionGw1);
			recallGw1_t2.add(""+recallGw1);
			
			try {
				File folderTP = new File(base+"temp/prec_rec/tp/");;
				File folderFN = new File(base+"temp/prec_rec/fn/");;
				true_positivesGw2 = folderTP.listFiles().length;
				false_negativesGw2 = folderFN.listFiles().length;
			} catch (Exception e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			precisionGw2 = true_positivesGw2/(true_positivesGw2+false_negativesGw2);
			recallGw2=true_positivesGw2/relevants;
			precisionGw2_t2.add(""+precisionGw2);
			recallGw2_t2.add(""+recallGw2);
			
			u.delete(base+"temp/reg/");
			u.delete(base+"temp/unreg/");
			u.delete(base+"temp/dis/");
			u.delete(base+"temp/prec_rec/tp/");
			u.delete(base+"temp/prec_rec/fn/");
			
			
			relevants = 0;
			true_positivesGw1 = 0;
			true_positivesGw2 = 0;
			false_negativesGw1 = 0;
			false_negativesGw2 = 0;
			precisionGw1 = 0;
			precisionGw2 = 0;
			recallGw1 = 0;
			recallGw2 = 0;
			
			gatewaysDataPoints =u.selectData(n,0);
			gateway1DataPoints = gatewaysDataPoints.get(0);
			System.out.println("-----------------------DATAPOINTS GENERATED-------------------------");
			System.out.println("-----------------------REGISTERING SERVICES GW1 TEST 3-------------------------");
			servicesGW1.addAll(u.getServices(gateway1DataPoints,3));
			for(int j=0; j<servicesGW1.size();j++){
				if(servicesGW1.get(j).getDescription().contains("AirTemperature"))relevants++;
			}
			
			try {
				JmDNS jmdnsGW1 = JmDNS.create("192.168.1.100");
				for(int x=0; x<servicesGW1.size();x++){
					System.out.println("-----------------------Service "+(x+1)+"-------------------------");
					int port = 2000 + x;
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("uri", servicesGW1.get(x).getUri());
					map.put("title", servicesGW1.get(x).getTitle());
					map.put("description", servicesGW1.get(x).getDescription());
					map.put("unit", servicesGW1.get(x).getUnit());
					map.put("rt", "" + servicesGW1.get(x).getRt());
					map.put("rel", "" +servicesGW1.get(x).getRb());
					map.put("locationX", servicesGW1.get(x).getLocationX());
					map.put("locationY", servicesGW1.get(x).getLocationY());
					map.put("locationZ", servicesGW1.get(x).getLocationZ());
					map.put("input", servicesGW1.get(x).getInput());
					map.put("output", servicesGW1.get(x).getOutput());
					ServiceInfo info = ServiceInfo.create("_"+servicesGW1.get(x).getName()+"._tcp.local.",servicesGW1.get(x).getId(),port,100,100,map);
					
					try {
						u.writeFile(base+"temp/reg/reg_"+servicesGW1.get(x).getId().replace("_","")+".txt", "0");
						jmdnsGW1.registerService(info);
					} 
					catch (IOException e1) {
						// 	TODO Auto-generated catch block
						e1.printStackTrace();
						System.out.println("Problems registering service in round: " + i);
					}
				}
				ServiceInfo[] serviceInfos = jmdnsGW1.list("_AirTemperature._tcp.local.");
				for (ServiceInfo service : serviceInfos) {
					System.out.println("## resolve service " + service.getName()  + " : " + service.getURL());
					System.out.println("-- Description: " + service.getPropertyString("description"));
					System.out.println("-- URI: " + service.getPropertyString("uri"));
					System.out.println("-- Title: " + service.getPropertyString("title"));
					System.out.println("-- Unit: " + service.getPropertyString("unit"));
					System.out.println("-- Response Time: " + service.getPropertyString("rt"));
					System.out.println("-- Reliability: " + service.getPropertyString("rel"));
					System.out.println("-- Latitude: " + service.getPropertyString("locationX"));
					System.out.println("-- Longitude: " + service.getPropertyString("locationY"));
					System.out.println("-- Altitude: " + service.getPropertyString("locationZ"));
					System.out.println("-- Input: " + service.getPropertyString("input"));
					System.out.println("-- Output: " + service.getPropertyString("output"));
					
					if(service.getPropertyString("description").contains("AirTemperature"))true_positivesGw1 ++;
					else false_negativesGw1++;
				}
				
				for(int j=0; j<servicesGW1.size();j++){
					System.out.println("-----------------------Service "+(j+1)+"-------------------------");
					int port = 2000 + j;
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("uri", servicesGW1.get(j).getUri());
					map.put("title", servicesGW1.get(j).getTitle());
					map.put("description", servicesGW1.get(j).getDescription());
					map.put("unit", servicesGW1.get(j).getUnit());
					map.put("rt", "" + servicesGW1.get(j).getRt());
					map.put("rel", "" +servicesGW1.get(j).getRb());
					map.put("locationX", servicesGW1.get(j).getLocationX());
					map.put("locationY", servicesGW1.get(j).getLocationY());
					map.put("locationZ", servicesGW1.get(j).getLocationZ());
					map.put("input", servicesGW1.get(j).getInput());
					map.put("output", servicesGW1.get(j).getOutput());
					ServiceInfo info = ServiceInfo.create("_"+servicesGW1.get(j).getName()+"._tcp.local.",servicesGW1.get(j).getId(),port,100,100,map);
					u.writeFile(base+"temp/unreg/unreg_"+servicesGW1.get(j).getId().replace("_","")+".txt", "0");
					jmdnsGW1.unregisterService(info);
				}
				jmdnsGW1.close();
			}
			catch(Exception ex){
				ex.printStackTrace();
			}
			
			t = new Thread();
			try {
				t.sleep(20000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			precisionGw1 = true_positivesGw1/(true_positivesGw1+false_negativesGw1);
			recallGw1 = true_positivesGw1/relevants;
			precisionGw1_t3.add(""+precisionGw1);
			recallGw1_t3.add(""+recallGw1);
			
			try {
				File folderTP = new File(base+"temp/prec_rec/tp/");;
				File folderFN = new File(base+"temp/prec_rec/fn/");;
				true_positivesGw2 = folderTP.listFiles().length;
				false_negativesGw2 = folderFN.listFiles().length;
			} catch (Exception e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			precisionGw2 = true_positivesGw2/(true_positivesGw2+false_negativesGw2);
			recallGw2=true_positivesGw2/relevants;
			precisionGw2_t3.add(""+precisionGw2);
			recallGw2_t3.add(""+recallGw2);
			
			u.delete(base+"temp/reg/");
			u.delete(base+"temp/unreg/");
			u.delete(base+"temp/dis/");
			u.delete(base+"temp/prec_rec/tp/");
			u.delete(base+"temp/prec_rec/fn/");
			
			relevants = 0;
			true_positivesGw1 = 0;
			true_positivesGw2 = 0;
			false_negativesGw1 = 0;
			false_negativesGw2 = 0;
			precisionGw1 = 0;
			precisionGw2 = 0;
			recallGw1 = 0;
			recallGw2 = 0;
		
		}
	
		System.out.println("\n-----------------------WRITING FILES PRECISION AND RECALL GW1-------------------------");
		u.writeFile(base+"prec_recall/Gw1/1/Precision.txt", precisionGw1_t1);
		u.writeFile(base+"prec_recall/Gw1/2/Precision.txt", precisionGw1_t2);
		u.writeFile(base+"prec_recall/Gw1/3/Precision.txt", precisionGw1_t3);
		u.writeFile(base+"prec_recall/Gw1/1/Recall.txt", recallGw1_t1);
		u.writeFile(base+"prec_recall/Gw1/2/Recall.txt", recallGw1_t2);
		u.writeFile(base+"prec_recall/Gw1/3/Recall.txt", recallGw1_t3);
		
		System.out.println("\n-----------------------WRITING FILES PRECISION AND RECALL GW2-------------------------");
		u.writeFile(base+"prec_recall/Gw2/1/Precision.txt", precisionGw2_t1);
		u.writeFile(base+"prec_recall/Gw2/2/Precision.txt", precisionGw2_t2);
		u.writeFile(base+"prec_recall/Gw2/3/Precision.txt", precisionGw2_t3);
		u.writeFile(base+"prec_recall/Gw2/1/Recall.txt", recallGw2_t1);
		u.writeFile(base+"prec_recall/Gw2/2/Recall.txt", recallGw2_t2);
		u.writeFile(base+"prec_recall/Gw2/3/Recall.txt", recallGw2_t3);*/
	}
}
