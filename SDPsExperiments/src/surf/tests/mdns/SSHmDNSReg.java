package surf.tests.mdns;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

//import surf.data.logic.Extract;
import surf.util.Util;

public class SSHmDNSReg {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String user = "root";
	    String password = "root";
	    String host = "192.168.1.147";
	    int port=22;
	    String round = "";
	    
	    Util u = new Util();
	    
	    String base = "D:/experiments/results/mdns/";

	    JSch jsch = new JSch();
	    try{
	    	Session session = jsch.getSession(user, host, port);
	    	session.setPassword(password);
	    	session.setConfig("StrictHostKeyChecking", "no");
	    	System.out.println("Establishing Connection...");
        
        	session.connect();
        	System.out.println("Connection established.");
        	ChannelExec channel=(ChannelExec) session.openChannel("exec");
        	BufferedReader in=new BufferedReader(new InputStreamReader(channel.getInputStream()));
        	channel.setCommand("avahi-browse -all;");
        	channel.connect();
        	String msg=null;
        	int i = 1;
        	long after = 0;
        	ArrayList<String> timesRegister = new ArrayList<>();
        	while((msg=in.readLine())!=null){
        		if(!round.equals(u.readFile(base+"temp/round.txt").replaceAll("[\n\r]",""))){
        			round = u.readFile(base+"temp/round.txt").replaceAll("[\n\r]","");
        			i=1;
        			timesRegister = new ArrayList<>();
        		}
        		if(msg.contains("+  wlan3 IPv4 example"+i)){
        			after = System.currentTimeMillis();
        			String res1 = u.readFile(base+"temp/reg/example_ssh"+i+".txt").replaceAll("[\n\r]","");
        			i++;
        			long before = Long.parseLong(res1);
        			long total = after - before;
        			timesRegister.add(""+total);
        			u.writeFile(base+"/"+round+"/20/timesRegister.txt", timesRegister);
        		}
        		
        		System.out.println(msg+"---timesRegister: "+timesRegister.size());
        		
        	}
        	channel.disconnect();
        	session.disconnect();
        }catch(Exception e){
        	e.printStackTrace();
        }
    }

}
