package surf.tests.mdns;

import java.io.IOException;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jmdns.JmDNS;
import javax.jmdns.ServiceEvent;
import javax.jmdns.ServiceListener;

//import surf.util.Util;

public class MDNSListener {
	
	
	static class SampleListener implements ServiceListener {
        @Override
        public void serviceAdded(ServiceEvent event) {
        	/*long after = System.currentTimeMillis();
        	String base = "D:/experiments/results/dnssd/";
        	Util u = new Util();
        	String before;
        	String name = event.getName().replace("_", "").replace(".tcp.local.", "");*/
        	System.out.println("Service added   : " + event.getName() + "." + event.getType());
			try {
				//before = u.readFile(base+"temp/reg/reg_"+name+".txt");
				//long total = after - Long.parseLong(before.replaceAll("[\n\r]",""));
				//u.writeFile(base+"temp/reg/reg_"+name+".txt", ""+total);
				//u.writeFile(base+"temp/dis/dis_"+name+".txt", before);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("AQUI");
			}
			System.out.println(event.getSource().toString());
		}

        @Override
        public void serviceRemoved(ServiceEvent event) {
        	/*long after = System.currentTimeMillis();
        	String base = "D:/experiments/results/dnssd/";
        	Util u = new Util();
        	String before;
        	String name = event.getName().replace("_", "").replace(".tcp.local.", "");*/
        	System.out.println("Service removed : " + event.getName() + "." + event.getType());
			try {
				/*before = u.readFile(base+"temp/unreg/unreg_"+name+".txt");
				long total = after - Long.parseLong(before.replaceAll("[\n\r]",""));
				u.writeFile(base+"temp/unreg/unreg_"+name+".txt", ""+total);*/
	        } catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }

        @Override
        public void serviceResolved(ServiceEvent event) {
        	/*long after = System.currentTimeMillis();
        	String base = "D:/experiments/results/dnssd/";
        	Util u = new Util();
        	String before;
        	String name = event.getName().replace("_", "").replace(".tcp.local.", "");*/
        	try {
				/*before = u.readFile(base+"temp/dis/dis_"+name+".txt");
				long total = after - Long.parseLong(before.replaceAll("[\n\r]",""));
				u.writeFile(base+"temp/dis/dis_"+name+".txt", ""+total);*/
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
        	/*if(event.getType().contains("AirTemperature")){
        		System.out.println("Service resolved: " + event.getInfo());
        		if(event.getInfo().getPropertyString("description").contains("AirTemperature")){
        			try {
        				u.writeFile(base+"temp/prec_rec/tp/"+name+".txt", "1");
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
    			}
        		else{
        			try {
        				u.writeFile(base+"temp/prec_rec/fn/"+name+".txt", "1");
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
        		}
        	}*/
        }
    }
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {

            // Activate these lines to see log messages of JmDNS
            boolean log = false;
            if (log) {
                Logger logger = Logger.getLogger(JmDNS.class.getName());
                ConsoleHandler handler = new ConsoleHandler();
                logger.addHandler(handler);
                logger.setLevel(Level.FINER);
                handler.setLevel(Level.FINER);
            }

            final JmDNS jmdns = JmDNS.create("192.168.1.135");
            jmdns.addServiceListener("_example._tcp.local.", new SampleListener());
            /*jmdns.addServiceListener("_RelativeHumidity._tcp.local.", new SampleListener());
            jmdns.addServiceListener("_DewPoint._tcp.local.", new SampleListener());
            jmdns.addServiceListener("_AirTemperature._tcp.local.", new SampleListener());
            jmdns.addServiceListener("_PeakWindSpeed._tcp.local.", new SampleListener());
            jmdns.addServiceListener("_WindSpeed._tcp.local.", new SampleListener());
            jmdns.addServiceListener("_WindGust._tcp.local.", new SampleListener());
            jmdns.addServiceListener("_PrecipitationAccumulated._tcp.local.", new SampleListener());
            jmdns.addServiceListener("_PeakWindDirection._tcp.local.", new SampleListener());
            jmdns.addServiceListener("_Visibility._tcp.local.", new SampleListener());
            jmdns.addServiceListener("_SnowSmoothed._tcp.local.", new SampleListener());
            jmdns.addServiceListener("_SnowDepth._tcp.local.", new SampleListener());
            jmdns.addServiceListener("_PrecipitationSmoothed._tcp.local.", new SampleListener());
            jmdns.addServiceListener("_SoilTemperature._tcp.local.", new SampleListener());
            jmdns.addServiceListener("_SoilMoisture._tcp.local.", new SampleListener());
            jmdns.addServiceListener("_SnowInterval._tcp.local.", new SampleListener());*/
            

            System.out.println("Press q and Enter, to quit");
            int b;
            while ((b = System.in.read()) != -1 && (char) b != 'q') {
                /* Stub */
            }
            jmdns.close();
            System.out.println("Done");
        } catch (IOException e) {
            e.printStackTrace();
        }

	}

}
