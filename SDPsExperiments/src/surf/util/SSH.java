package surf.util;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

public class SSH {
	
	private String user;
	private String password;
	private String host;
	private int port;
	
	public SSH(String user, String password, String host, int port) {
		// TODO Auto-generated constructor stub
		this.setUser(user);
		this.setPassword(password);
		this.setHost(host);
		this.setPort(port);
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}
	
	public void runCommand(String command){
		try{
	    	JSch jsch = new JSch();
	        Session session = jsch.getSession(user, host, port);
	        session.setPassword(password);
	        session.setConfig("StrictHostKeyChecking", "no");
	        System.out.println("Establishing Connection...");
	        session.connect();
	        System.out.println("Connection established.");
	        
	        Channel channel=session.openChannel("exec");
	        System.out.println(command);
			((ChannelExec) channel).setCommand(command);
	        System.out.println("Running command.");
	        channel.connect();
	        
	        try{Thread.sleep(5000);}catch(Exception ee){}
	        
	        System.out.println("Closing channel.");
	        channel.disconnect();
	        System.out.println("Closing session.");
	        session.disconnect();
	        
	    }
	    catch(Exception e){
	    	System.err.print(e);
	    }
	}

}
