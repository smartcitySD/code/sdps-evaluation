package surf.coap.client;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Set;

import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapResponse;
import org.eclipse.californium.core.WebLink;
import org.eclipse.californium.core.coap.CoAP.Code;
import org.eclipse.californium.core.coap.Request;
import org.json.JSONObject;

public class Client {

	protected CoapClient client;
	private URI uri = null;
	
	public Client(String server) {
		// TODO Auto-generated constructor stub
				
				try {
					uri = new URI(server);
				} catch (URISyntaxException e) {
					System.err.println("Invalid URI: " + e.getMessage());
					System.exit(-1);
				}
				client = new CoapClient(uri);
	}
	
	public String Register(ArrayList<Integer> gateWay){
	
		CoapResponse response;
		int airTemperature=0;
		int airTemp=0;
		int total = 0;
		int rt = 0;
		long it = 0;
		long et = 0;
		long rtt = 0;
		long avrt = 0;
		double rb = 0;
		
		//File folder = new File("/home/ubuntu/data/");
		File folder = new File("D:/experiments/data/");
		File[] listOfFiles = folder.listFiles();
		
		ArrayList<String> registered = new ArrayList<String>();
	
		for(int i=0; i<gateWay.size(); i++){
			try{
				String f = readFile(listOfFiles[gateWay.get(i)].getPath());
				String [] parts = f.split(";");
				JSONObject service = new JSONObject();
				JSONObject attributes = new JSONObject();
				service.append("href", parts[1]);
				registered.add(parts[1]);
				service.append("rt", parts[2]);
				if(parts[2].equals("AirTemperature")&&parts[9].equals("*C"))airTemperature++;
				if(parts[2].equals("AirTemp")&&parts[9].equals("*C"))airTemp++;
				rt = Integer.parseInt(parts[3]);
				attributes.append("responseTime",rt);
				rb = Double.parseDouble(parts[4]);
				attributes.append("reliability", rb);
				attributes.append("locationX", parts[5]);
				attributes.append("locationY", parts[6]);
				attributes.append("locationZ", parts[7]);
				attributes.append("description", parts[8]);
				attributes.append("unit", parts[9]);
				attributes.append("title", parts[10]);
				service.append("attributes", attributes);
				String message = service.toString().replace("[", "").replace("]", "");
				System.out.println(message);
				it = System.currentTimeMillis();
				response = client.post(message,50);
				et = System.currentTimeMillis();
				rtt = et - it;
				avrt += rtt;
				System.out.println(response.advanced().getType() + "-" + response.getCode()+ "-" + response.getResponseText());
				System.out.println(response.getResponseText());
				System.out.println(i);
			}
			catch(Exception ex){
				ex.getMessage();
			}
		}
		avrt = avrt/gateWay.size();
		total = airTemperature + airTemp;
		String regs= "";
		for(int i=0; i<registered.size();i++)regs = regs + "-" + registered.get(i);
		return airTemperature + "," + airTemp + "," + total + "," + avrt + "," + regs;
	}
	
	public String Discover(String query){
		long it = System.currentTimeMillis();
		
		try{
			Set<WebLink> links = client.discover("title="+query);
			long et = System.currentTimeMillis();
			long rt = et - it;
			String result = links.toString().replace("[", "").replace("]", "");
			String [] results = result.split(",");
			System.out.println(result);
			int fp=0;
			for(int i=0; i<results.length; i++){
				if(results[i].contains("Vis")) fp++;
			}
			return results.length+ "," + fp + "," + it + "," + et + "," + rt;
		}
		catch(Exception ex){
			return "fail";
		}
	}
	
	public String Delete(){
		long it = 0;
		long et = 0;
		long rtt = 0;
		CoapResponse response;
		try{
			it = System.currentTimeMillis();
			//response = client.delete();
			/*Request request = new Request(Code.DELETE);
			request.setURI(uri);
			request.setPayload(resource);
			request.send();*/
			response=client.delete();
			et = System.currentTimeMillis();
			rtt = et - it;
			System.out.println(response.advanced().getType() + "-" + response.getCode() + "-" + response.getResponseText());
			System.out.println(response.getResponseText());
			return "" + rtt;
		}
		catch(Exception ex){
			return Delete();
		}
	}
	
	
	private static String readFile( String file ) throws IOException {
	    BufferedReader reader = new BufferedReader( new FileReader (file));
	    String         line = null;
	    StringBuilder  stringBuilder = new StringBuilder();
	    try {
	        while( ( line = reader.readLine() ) != null ) {
	            stringBuilder.append( line );
	            //stringBuilder.append( ls );
	        }

	        return stringBuilder.toString();
	    } finally {
	        reader.close();
	    }
	}
	
	/*private static void writeFile( Service service ) {
		File f;
		f = new File("/home/ubuntu/data/"+service.getId()+".txt");
		
		try{
			FileWriter w = new FileWriter(f);
			BufferedWriter bw = new BufferedWriter(w);
			PrintWriter wr = new PrintWriter(bw);  
			wr.write(service.getId()+";"+service.getUri()+";"+service.getName()+";"+service.getRt()+";"+service.getRb()+";"+service.getLocationX()+";"+service.getLocationY()+";"+service.getLocationZ());
			wr.close();
			bw.close();
			}
		catch(IOException e){
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}*/

}
