package surf.coap.test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Random;

import surf.coap.client.Client;

public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Client c = new Client("coap://134.226.36.181:55683/");
		String base = "/home/ubuntu/experiments/";
		int round = 0;
		int subround = 0;
		
		
		System.out.println("-----------------------CLIENTS STARTING-------------------------\n");
		//Client c1 = new Client("coap://localhost:5683/WeatherTest/");
		Client c1 = new Client("coap://134.226.36.181:45683/WeatherTest/");
		//Client c2 = new Client("coap://localhost:5683/WeatherTest/");
		Client c2 = new Client("coap://134.226.36.181:55683/WeatherTest/");
		System.out.println("-----------------------CLIENTS STARTED-------------------------\n");
		
		for(int n=2000; n<=20000;n+=2000){
			round ++;
			subround = 0;  
			System.out.println("-----------------------ROUND "+ round +" N=" + n + "-------------------------");
			
			for(double i=0; i<0.1; i+=0.1){
				subround ++;
				ArrayList<String> responseTimeRegisterGw1 = new ArrayList<String>();
				ArrayList<String> responseTimeRegisterGw2 = new ArrayList<String>();
				ArrayList<String> responseTimeGw1 = new ArrayList<String>();
				ArrayList<String> responseTimeGw2 = new ArrayList<String>();
				ArrayList<String> precisionGw1 = new ArrayList<String>();
				ArrayList<String> precisionGw2 = new ArrayList<String>();
				ArrayList<String> recallGw1 = new ArrayList<String>();
				ArrayList<String> recallGw2 = new ArrayList<String>();
				ArrayList<String> responseTimeUnregisterGw1 = new ArrayList<String>();
				ArrayList<String> responseTimeUnregisterGw2 = new ArrayList<String>();
				for(int j = 0; j< 100; j++){
					
					System.out.println("-----------------------DELETING CURRENT SERVICES-------------------------");
					String tunr1=c1.Delete();
					responseTimeUnregisterGw1.add(""+tunr1);
					
					String tunr2=c2.Delete();
					responseTimeUnregisterGw2.add(""+tunr2);
					
					
					System.out.println("-----------------------SUB-ROUND "+ subround +"-------------------------");
					System.out.println("-----------------------GENERATING DATAPOINTS-------------------------");
					ArrayList<ArrayList<Integer>> gatewaysDataPoints =selectData(n,i);
					ArrayList<Integer> gateway1DataPoints = gatewaysDataPoints.get(0);
					ArrayList<Integer> gateway2DataPoints = gatewaysDataPoints.get(1);
					System.out.println("-----------------------DATAPOINTS GENERATED-------------------------");
					System.out.println("-----------------------REGISTERING SERVICES GW1-------------------------");
					String r1 = c1.Register(gateway1DataPoints);
					String [] v1 = r1.split(",");
					double total1 = Double.parseDouble(v1[2]);
					responseTimeRegisterGw1.add(v1[3]);
					System.out.println("-----------------------SERVICES REGISTERED GW1-------------------------");
					System.out.println("-----------------------REGISTERING SERVICES GW2-------------------------");
					String r2 = c2.Register(gateway2DataPoints);
					String [] v2 = r2.split(",");
					double total2 = Double.parseDouble(v2[2]);
					responseTimeRegisterGw2.add(v2[3]);
					System.out.println("-----------------------SERVICES REGISTERED GW2-------------------------");
					
					System.out.println("\n-----------------------STARTING DISCOVERY GW1 " + j +"-------------------------");
					String rs1 = c1.Discover("AirTemperature");
					double relevant1;
					double retrieved1;
					double falsePos1;
					double precision1;
					double recall1;
					if(!rs1.equals("fail")){
						String[] res2 = rs1.split(",");
						responseTimeGw1.add(""+res2[4]);
						retrieved1 = Double.parseDouble(res2[0]);
						falsePos1 = Double.parseDouble(res2[1]);
						relevant1 = retrieved1 - falsePos1;
						precision1  = relevant1/retrieved1;
						precisionGw1.add(""+precision1);
						recall1 = relevant1/total1;
						recallGw1.add(""+recall1);
						System.out.println("\n-----------------------SUCCESS DISCOVERY GW1 " + j +"-------------------------");
					}
					else{
						responseTimeGw1.add("--");
						retrieved1 = 0;
						falsePos1 = 0;
						relevant1 = retrieved1 - falsePos1;
						precision1  = 0;
						precisionGw2.add(""+precision1);
						recall1 = 0;
						recallGw2.add(""+recall1);
						System.out.println("\n-----------------------FAIL DISCOVERY GW1 " + j +"-------------------------");
					}
					System.out.println("\n-----------------------FINISHING DISCOVERY GW1 " + j +"-------------------------");
					System.out.println("\n-----------------------STARTING DISCOVERY GW2 " + j +"-------------------------");
					String rs2 = c2.Discover("AirTemperature");
					double relevant2;
					double retrieved2;
					double falsePos2;
					double precision2;
					double recall2;
					if(!rs2.equals("fail")){
						String[] res2 = rs2.split(",");
						responseTimeGw2.add(""+res2[4]);
						retrieved2 = Double.parseDouble(res2[0]);
						falsePos2 = Double.parseDouble(res2[1]);
						relevant2 = retrieved2 - falsePos2;
						precision2  = relevant2/retrieved2;
						precisionGw2.add(""+precision2);
						recall2 = relevant2/total2;
						recallGw2.add(""+recall2);
						System.out.println("\n-----------------------SUCCESS DISCOVERY GW2 " + j +"-------------------------");
					}
					else{
						responseTimeGw2.add("--");
						retrieved2 = 0;
						falsePos2 = 0;
						relevant2 = retrieved2 - falsePos2;
						precision2  = 0;
						precisionGw2.add(""+precision2);
						recall2 = 0;
						recallGw2.add(""+recall2);
						System.out.println("\n-----------------------FAIL DISCOVERY GW2 " + j +"-------------------------");
					}
					System.out.println("\n-----------------------FINISHING DISCOVERY GW2 " + j +"-------------------------");
		
				}
				
				System.out.println("\n-----------------------WRITING FILES GW1-------------------------");
				writeFile(base+round+"/"+subround+"/Gw1/ResponseTimeRegister.txt", responseTimeRegisterGw1);
				writeFile(base+round+"/"+subround+"/Gw1/ResponseTimeUnRegister.txt", responseTimeUnregisterGw1);
				writeFile(base+round+"/"+subround+"/Gw1/ResponseTime.txt", responseTimeGw1);
				writeFile(base+round+"/"+subround+"/Gw1/Precision.txt", precisionGw1);
				writeFile(base+round+"/"+subround+"/Gw1/Recall.txt", recallGw1);
				
				System.out.println("\n-----------------------WRITING FILES GW2-------------------------");
				writeFile(base+round+"/"+subround+"/Gw2/ResponseTimeRegister.txt", responseTimeRegisterGw2);
				writeFile(base+round+"/"+subround+"/Gw2/ResponseTimeUnRegister.txt", responseTimeUnregisterGw2);
				writeFile(base+round+"/"+subround+"/Gw2/ResponseTime.txt", responseTimeGw2);
				writeFile(base+round+"/"+subround+"/Gw2/Precision.txt", precisionGw2);
				writeFile(base+round+"/"+subround+"/Gw2/Recall.txt", recallGw2);
			}
		}
		
	}

	private static ArrayList<ArrayList<Integer>> selectData(int n, double x) {
		// TODO Auto-generated method stub
		ArrayList<ArrayList<Integer>> gatewaysDataPoints = new ArrayList<ArrayList<Integer>>();
		ArrayList<Integer> gateway1 = new ArrayList<Integer>();
		ArrayList<Integer> gateway2 = new ArrayList<Integer>();
		double both = n*x;
		Random rnd = new Random();
		
		for(double i=0; i<both;i++){
			int value = (int) (rnd.nextDouble() * 59108);
			if(!gateway1.contains(value)){
				gateway1.add(value);
				gateway2.add(value);
			}
			else i--;
		}
		
		for(double i=0; i<n-both;i++){
			int value = (int) (rnd.nextDouble() * 59108);
			if(!gateway1.contains(value)){
				gateway1.add(value);
			}
			else i--;
		}
		
		for(double i=0; i<n-both;i++){
			int value = (int) (rnd.nextDouble() * 59108);
			if(!gateway2.contains(value) && !gateway1.contains(value)){
				gateway2.add(value);
			}
			else i--;
		}
		
		gatewaysDataPoints.add(gateway1);
		gatewaysDataPoints.add(gateway2);
		return gatewaysDataPoints;
	}
	
	private static void writeFile( String path, ArrayList<String> content ) {
		File f;
		f = new File(path);
		
		try{
			FileWriter w = new FileWriter(f);
			BufferedWriter bw = new BufferedWriter(w);
			PrintWriter wr = new PrintWriter(bw);
			for(int i=0;i<content.size();i++)wr.write(content.get(i)+"\n");
			wr.close();
			bw.close();
			}
		catch(IOException e){
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}

}
