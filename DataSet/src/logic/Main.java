package logic;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Random;

import entity.Service;

public class Main {

	public static void main(String[] args) {
		extraction();
	}
	
	public static void extraction(){
		//File folder = new File("/home/ubuntu/Downloads/rdf/");
		File folder = new File("D:/experiments/rdf/");
		File[] listOfFiles = folder.listFiles();
		Random randomGenerator = new Random();
		ArrayList<Service> services = new ArrayList<Service>();
		Service service = null;
		String[] parameters = null;
		ArrayList<String> others = new ArrayList<String>();
		
		int WindDirection = 0;
		int WindDirectionRT = 0;
		int WindDirectionRB = 0;
		int WindDir = 0;
		int WindDirRT = 0;
		int WindDirRB = 0;
		int RelativeHumidity = 0;
		int RelativeHumidityRT = 0;
		int RelativeHumidityRB = 0;
		int RelHumidity = 0;
		int RelHumidityRT = 0;
		int RelHumidityRB = 0;
		int DewPoint = 0;
		int DewPointRT = 0;
		int DewPointRB = 0;
		int DewP = 0;
		int DewPRT = 0;
		int DewPRB = 0;
		int AirTemperature = 0;
		int AirTemperatureRT = 0;
		int AirTemperatureRB = 0;
		int AirTemp = 0;
		int AirTempRT = 0;
		int AirTempRB = 0;
		int PeakWindSpeed = 0;
		int PeakWindSpeedRT = 0;
		int PeakWindSpeedRB = 0;
		int PeakWindS = 0;
		int PeakWindSRT = 0;
		int PeakWindSRB = 0;
		int WindSpeed = 0;
		int WindSpeedRT = 0;
		int WindSpeedRB = 0;
		int WindS = 0;
		int WindSRT = 0;
		int WindSRB = 0;
		int WindGust = 0;
		int WindGustRT = 0;
		int WindGustRB = 0;
		int WindG = 0;
		int WindGRT = 0;
		int WindGRB = 0;
		int PrecipitationAccumulated = 0;
		int PrecipitationAccumulatedRT = 0;
		int PrecipitationAccumulatedRB = 0;
		int PrecipitationAcc = 0;
		int PrecipitationAccRT = 0;
		int PrecipitationAccRB = 0;
		int PeakWindDirection = 0;
		int PeakWindDirectionRT = 0;
		int PeakWindDirectionRB = 0;
		int PeakWindDir = 0;
		int PeakWindDirRT = 0;
		int PeakWindDirRB = 0;
		int Visibility = 0;
		int VisibilityRT = 0;
		int VisibilityRB = 0;
		int Vis = 0;
		int VisRT = 0;
		int VisRB = 0;
		int SnowSmoothed = 0;
		int SnowSmoothedRT = 0;
		int SnowSmoothedRB = 0;
		int SnowSmoot = 0;
		int SnowSmootRT = 0;
		int SnowSmootRB = 0;
		int SnowDepth = 0;
		int SnowDepthRT = 0;
		int SnowDepthRB = 0;
		int SnowD = 0;
		int SnowDRT = 0;
		int SnowDRB = 0;
		int PrecipitationSmoothed = 0;
		int PrecipitationSmoothedRT = 0;
		int PrecipitationSmoothedRB = 0;
		int PrecipitationSmoot = 0;
		int PrecipitationSmootRT = 0;
		int PrecipitationSmootRB = 0;
		int SoilTemperature = 0;
		int SoilTemperatureRT = 0;
		int SoilTemperatureRB = 0;
		int SoilTemp = 0;
		int SoilTempRT = 0;
		int SoilTempRB = 0;
		int SoilMoisture = 0;
		int SoilMoistureRT = 0;
		int SoilMoistureRB = 0;
		int SoilMoist = 0;
		int SoilMoistRT = 0;
		int SoilMoistRB = 0;
		int SnowInterval = 0;
		int SnowIntervalRT = 0;
		int SnowIntervalRB = 0;
		int SnowInt = 0;
		int SnowIntRT = 0;
		int SnowIntRB = 0;
		
		for (int i = 0; i < listOfFiles.length; i++) {
		
			String station = "";
			String uri="";
			String id = "";
			String name = "";
			String title = "";
			String description = "";
			String unit = "";
			int rt = 0;
			double rb = 0;
			String locationX="";
			String locationY="";
			String locationZ="";
			System.out.println("**********************LIST OF SERVICES AT THE PROCESS**********************");
			System.out.println("----------------New Service FILE " + listOfFiles[i].getName() +  "----------------");
			
			try {
				
				String f = readFile(listOfFiles[i].getPath()).replaceAll("\n", "");
				if(f.contains("om-owl:parameter")){
					String[] parts = f.split("sens_obs:");
				
					for(int j = 0; j < parts.length ; j++){
						if(parts[j].contains("System")){
							String [] p1=parts[j].split("hasSourceURI ");
							String [] p2=p1[1].split("om-owl:parameter");
							station = p2[0].split("=")[1].replace(" ","").replace(">;", "");
							String [] p3=p2[1].split("om-owl:processLocation");
							parameters = p3[0].split("weather:_");
						}
						if(parts[j].contains("point")){
							String[] p4 = parts[j].split("alt");
							String[] p5 = p4[1].split("lat");
							locationZ = p5[0].replace(" ", "").replace("\"", "").replace("^", "").replace("xsd:float;wgs84:", "").replaceAll("\n", "");
							String[] p6 = p5[1].split("long");
							locationY = p6[0].replace(" ", "").replace("\"", "").replace("^", "").replace("xsd:float;wgs84:", "").replaceAll("\n", "");;
							String[] p7 = p6[1].split("float");
							locationX = p7[0].replace(" ", "").replace("\"", "").replace("^", "").replace("xsd:", "").replaceAll("\n", "");;
						}
					}
					
					rt = (int) Math.round(randomGenerator.nextGaussian() * 100 + 500);
					rb = Math.round(randomGenerator.nextGaussian() * 75 + 800);
					rb = rb/1000;
				
					for(int j = 1; j<parameters.length;j++){
						parameters[j]=parameters[j].replace(" ", "").replace(",", "").replace(";", "").replace("_", "");
						int r = randomGenerator.nextInt(100);
						if(r>50){
							switch (parameters[j]){
								case "WindDirection":
									name = "WindDir";
									title = "WindDir";
									unit = "degrees";
									WindDir++;
									if(rt < 500) WindDirRT++;
									if(rb > 0.90) WindDirRB++;
									break;
								case "RelativeHumidity":
									name = "RelHumidity";
									title = "RelHumidity";
									unit = "%";
									RelHumidity++;
									if(rt < 500) RelHumidityRT++;
									if(rb > 0.90) RelHumidityRB++;
									break;
								case "DewPoint":
									name = "DewP";
									title = "DewP";
									unit = "*C";
									DewP++;
									if(rt < 500) DewPRT++;
									if(rb > 0.90) DewPRB++;
									break;
								case "AirTemperature":
									name = "AirTemp";
									title = "AirTemp";
									unit = "*C";
									AirTemp++;
									if(rt < 500) AirTempRT++;
									if(rb > 0.90) AirTempRB++;
									break;
								case "PeakWindSpeed":
									name = "PeakWindS";
									title = "PeakWindS";
									unit = "km/h";
									PeakWindS++;
									if(rt < 500) PeakWindSRT++;
									if(rb > 0.90) PeakWindSRB++;
									break;
								case "WindSpeed":
									name = "WindS";
									title = "WindS";
									unit = "km/h";
									WindS++;
									if(rt < 500) WindSRT++;
									if(rb > 0.90) WindSRB++;
									break;
								case "WindGust":
									name = "WindG";
									title = "WindG";
									unit = "km/h";
									WindG++;
									if(rt < 500) WindGRT++;
									if(rb > 0.90) WindGRB++;
									break;
								case "PrecipitationAccumulated":
									name = "PrecipitationAcc";
									title = "PrecipitationAcc";
									unit = "mm3";
									PrecipitationAcc++;
									if(rt < 500) PrecipitationAccRT++;
									if(rb > 0.90) PrecipitationAccRB++;
									break;
								case "PeakWindDirection":
									name = "PeakWindDir";
									title = "PeakWindDir";
									unit = "degrees";
									PeakWindDir++;
									if(rt < 500) PeakWindDirRT++;
									if(rb > 0.90) PeakWindDirRB++;
									break;
								case "Visibility":
									name = "Vis";
									title = "AirTemperature";
									unit = "kms";
									Vis++;
									if(rt < 500) VisRT++;
									if(rb > 0.90) VisRB++;
									break;
								case "SnowSmoothed":
									name = "SnowSmoot";
									title = "SnowSmoot";
									unit = "kg/m2";
									SnowSmoot++;
									if(rt < 500) SnowSmoothedRT++;
									if(rb > 0.90) SnowSmoothedRB++;
									break;
								case "SnowDepth":
									name = "SnowD";
									title = "SnowD";
									unit = "kg/m2";
									SnowD++;
									if(rt < 500) SnowDRT++;
									if(rb > 0.90) SnowDRB++;
									break;
								case "PrecipitationSmoothed":
									name = "PrecipitationSmoot";
									title = "PrecipitationSmoot";
									unit = "kg/m2";
									PrecipitationSmoot++;
									if(rt < 500) PrecipitationSmootRT++;
									if(rb > 0.90) PrecipitationSmootRB++;
									break;
								case "SoilTemperature":
									name = "SoilTemp";
									title = "SoilTemp";
									unit = "*C";
									SoilTemp++;
									if(rt < 500) SoilTempRT++;
									if(rb > 0.90) SoilTempRB++;
									break;
								case "SoilMoisture":
									name = "SoilMoist";
									title = "SoilMoist";
									unit = "mm3";
									SoilMoist++;
									if(rt < 500) SoilMoistRT++;
									if(rb > 0.90) SoilMoistRB++;
									break;
								case "SnowInterval":
									name = "SnowInt";
									title = "SnowInt";
									unit = "sec";
									SnowInt++;
									if(rt < 500) SnowIntRT++;
									if(rb > 0.90) SnowIntRB++;
									break;
								default:
									others.add(parameters[j]);
									break;
							}
							description = "This service monitors " + name;
						}
						else{
						name = parameters[j];
						title = parameters[j];
						description = "This service monitors " + name;
							switch (parameters[j]){
								case "WindDirection":
									unit = "degrees";
									WindDirection++;
									if(rt < 500) WindDirectionRT++;
									if(rb > 0.90) WindDirectionRB++;
									break;
								case "RelativeHumidity":
									unit = "%";
									RelativeHumidity++;
									if(rt < 500) RelativeHumidityRT++;
									if(rb > 0.90) RelativeHumidityRB++;
									break;
								case "DewPoint":
									unit = "*C";
									DewPoint++;
									if(rt < 500) DewPointRT++;
									if(rb > 0.90) DewPointRB++;
									break;
								case "AirTemperature":
									unit = "*C";
									AirTemperature++;
									if(rt < 500) AirTemperatureRT++;
									if(rb > 0.90) AirTemperatureRB++;
									break;
								case "PeakWindSpeed":
									unit = "km/h";
									PeakWindSpeed++;
									if(rt < 500) PeakWindSpeedRT++;
									if(rb > 0.90) PeakWindSpeedRB++;
									break;
								case "WindSpeed":
									unit = "km/h";
									WindSpeed++;
									if(rt < 500) WindSpeedRT++;
									if(rb > 0.90) WindSpeedRB++;
									break;
								case "WindGust":
									unit = "km/h";
									WindGust++;
									if(rt < 500) WindGustRT++;
									if(rb > 0.90) WindGustRB++;
									break;
								case "PrecipitationAccumulated":
									unit = "mm3";
									PrecipitationAccumulated++;
									if(rt < 500) PrecipitationAccumulatedRT++;
									if(rb > 0.90) PrecipitationAccumulatedRB++;
									break;
								case "PeakWindDirection":
									unit = "degrees";
									PeakWindDirection++;
									if(rt < 500) PeakWindDirectionRT++;
									if(rb > 0.90) PeakWindDirectionRB++;
									break;
								case "Visibility":
									unit = "kms";
									Visibility++;
									if(rt < 500) VisibilityRT++;
									if(rb > 0.90) VisibilityRB++;
									break;
								case "SnowSmoothed":
									unit = "kg/m2";
									SnowSmoothed++;
									if(rt < 500) SnowSmoothedRT++;
									if(rb > 0.90) SnowSmoothedRB++;
									break;
								case "SnowDepth":
									unit = "kg/m2";
									SnowDepth++;
									if(rt < 500) SnowDepthRT++;
									if(rb > 0.90) SnowDepthRB++;
									break;
								case "PrecipitationSmoothed":
									unit = "kg/m2";
									PrecipitationSmoothed++;
									if(rt < 500) PrecipitationSmoothedRT++;
									if(rb > 0.90) PrecipitationSmoothedRB++;
									break;
								case "SoilTemperature":
									unit = "*C";
									SoilTemperature++;
									if(rt < 500) SoilTemperatureRT++;
									if(rb > 0.90) SoilTemperatureRB++;
									break;
								case "SoilMoisture":
									unit = "mm3";
									SoilMoisture++;
									if(rt < 500) SoilMoistureRT++;
									if(rb > 0.90) SoilMoistureRB++;
									break;
								case "SnowInterval":
									unit = "sec";
									SnowInterval++;
									if(rt < 500) SnowIntervalRT++;
									if(rb > 0.90) SnowIntervalRB++;
									break;
								default:
									others.add(parameters[j]);
									break;
							}
						}
						uri = station + "_" + name; 
						id = station + "_"+ name;
						service = new Service(id,uri,name,title,description,unit,rt,rb,locationX,locationY,locationZ);
						services.add(service);
						id = "";
						uri = "";
						name = "";
						title = "";
						description = "";
						unit = "";
						
						System.out.println("ID: " + service.getId());
						System.out.println("URI: " + service.getUri());
						System.out.println("NAME: " + service.getName());
						System.out.println("TITLE: " + service.getTitle());
						System.out.println("DESCRIPTION: " + service.getDescription());
						System.out.println("UNIT: " + service.getUnit());
						System.out.println("RELIABILITY: " + service.getRb());
						System.out.println("RESPONSE TIME: " + service.getRt());
						System.out.println("LOCATION.X: " + service.getLocationX());
						System.out.println("LOCATION.Y: " + service.getLocationY());
						System.out.println("LOCATION.Z: " + service.getLocationZ());
					
						System.out.println("++++++++++++++++++++++++++++++STATISTICS AT THE PROCESS++++++++++++++++++++++++++++++");
						System.out.println("NUMBER OF SERVICES: " + services.size());
						System.out.println("NUMBER OF WIND_DIRECTION: " + WindDirection);
						System.out.println("NUMBER OF WIND_DIRECTION MINOR RESPONSE TIME: " + WindDirectionRT);
						System.out.println("NUMBER OF WIND_DIRECTION MAJOR RELIABILITY: " + WindDirectionRB);
						System.out.println("NUMBER OF WIND_DIR: " + WindDir);
						System.out.println("NUMBER OF WIND_DIR MINOR RESPONSE TIME: " + WindDirRT);
						System.out.println("NUMBER OF WIND_DIR MAJOR RELIABILITY: " + WindDirRB);
						System.out.println("NUMBER OF RELATIVE_HUMIDITY: " + RelativeHumidity);
						System.out.println("NUMBER OF RELATIVE_HUMIDITY MINOR RESPONSE TIME: " + RelativeHumidityRT);
						System.out.println("NUMBER OF RELATIVE_HUMIDITY MAJOR RELIABILITY: " + RelativeHumidityRB);
						System.out.println("NUMBER OF REL_HUMIDITY: " + RelHumidity);
						System.out.println("NUMBER OF REL_HUMIDITY MINOR RESPONSE TIME: " + RelHumidityRT);
						System.out.println("NUMBER OF REL_HUMIDITY MAJOR RELIABILITY: " + RelHumidityRB);
						System.out.println("NUMBER OF DEW_POINT: " + DewPoint);
						System.out.println("NUMBER OF DEW_POINT MINOR RESPONSE TIME: " + DewPointRT);
						System.out.println("NUMBER OF DEW_POINT MAJOR RELIABILITY: " + DewPointRB);
						System.out.println("NUMBER OF DEW_P: " + DewP);
						System.out.println("NUMBER OF DEW_P MINOR RESPONSE TIME: " + DewPRT);
						System.out.println("NUMBER OF DEW_P MAJOR RELIABILITY: " + DewPRB);
						System.out.println("NUMBER OF AIR_TEMPERATURE: " + AirTemperature);
						System.out.println("NUMBER OF AIR_TEMPERATURE MINOR RESPONSE TIME: " + AirTemperatureRT);
						System.out.println("NUMBER OF AIR_TEMPERATURE MAJOR RELIABILITY: " + AirTemperatureRB);
						System.out.println("NUMBER OF AIR_TEMP: " + AirTemp);
						System.out.println("NUMBER OF AIR_TEMP MINOR RESPONSE TIME: " + AirTempRT);
						System.out.println("NUMBER OF AIR_TEMP MAJOR RELIABILITY: " + AirTempRB);
						System.out.println("NUMBER OF PEAK_WIND_SPEED: " + PeakWindSpeed);
						System.out.println("NUMBER OF PEAK_WIND_SPEED MINOR RESPONSE TIME: " + PeakWindSpeedRT);
						System.out.println("NUMBER OF PEAK_WIND_SPEED MAJOR RELIABILITY: " + PeakWindSpeedRB);
						System.out.println("NUMBER OF PEAK_WIND_S: " + PeakWindS);
						System.out.println("NUMBER OF PEAK_WIND_S MINOR RESPONSE TIME: " + PeakWindSRT);
						System.out.println("NUMBER OF PEAK_WIND_S MAJOR RELIABILITY: " + PeakWindSRB);
						System.out.println("NUMBER OF WIND_SPEED: " + WindSpeed);
						System.out.println("NUMBER OF WIND_SPEED MINOR RESPONSE TIME: " + WindSpeedRT);
						System.out.println("NUMBER OF WIND_SPEED MAJOR RELIABILITY: " + WindSpeedRB);
						System.out.println("NUMBER OF WIND_S: " + WindS);
						System.out.println("NUMBER OF WIND_S MINOR RESPONSE TIME: " + WindSRT);
						System.out.println("NUMBER OF WIND_S MAJOR RELIABILITY: " + WindSRB);
						System.out.println("NUMBER OF WIND_GUST: " + WindGust);
						System.out.println("NUMBER OF WIND_GUST MINOR RESPONSE TIME: " + WindGustRT);
						System.out.println("NUMBER OF WIND_GUST MAJOR RELIABILITY: " + WindGustRB);
						System.out.println("NUMBER OF WIND_G: " + WindG);
						System.out.println("NUMBER OF WIND_G MINOR RESPONSE TIME: " + WindGRT);
						System.out.println("NUMBER OF WIND_G MAJOR RELIABILITY: " + WindGRB);
						System.out.println("NUMBER OF PRECIPITATION_ACCUMULATED: " + PrecipitationAccumulated);
						System.out.println("NUMBER OF PRECIPITATION_ACCUMULATED MINOR RESPONSE TIME: " + PrecipitationAccumulatedRT);
						System.out.println("NUMBER OF PRECIPITATION_ACCUMULATED MAJOR RELIABILITY: " + PrecipitationAccumulatedRB);
						System.out.println("NUMBER OF PRECIPITATION_ACC: " + PrecipitationAcc);
						System.out.println("NUMBER OF PRECIPITATION_ACC MINOR RESPONSE TIME: " + PrecipitationAccRT);
						System.out.println("NUMBER OF PRECIPITATION_ACC MAJOR RELIABILITY: " + PrecipitationAccRB);
						System.out.println("NUMBER OF PEAK_WIND_DIRECTION: " + PeakWindDirection);
						System.out.println("NUMBER OF PEAK_WIND_DIRECTION MINOR RESPONSE TIME: " + PeakWindDirectionRT);
						System.out.println("NUMBER OF PEAK_WIND_DIRECTION MAJOR RELIABILITY: " + PeakWindDirectionRB);
						System.out.println("NUMBER OF PEAK_WIND_DIR: " + PeakWindDir);
						System.out.println("NUMBER OF PEAK_WIND_DIR MINOR RESPONSE TIME: " + PeakWindDirRT);
						System.out.println("NUMBER OF PEAK_WIND_DIR MAJOR RELIABILITY: " + PeakWindDirRB);
						System.out.println("NUMBER OF VISIBILITY: " + Visibility);
						System.out.println("NUMBER OF VISIBILITY MINOR RESPONSE TIME: " + VisibilityRT);
						System.out.println("NUMBER OF VISIBILITY MAJOR RELIABILITY: " + VisibilityRB);
						System.out.println("NUMBER OF VIS: " + Vis);
						System.out.println("NUMBER OF VIS MINOR RESPONSE TIME: " + VisRT);
						System.out.println("NUMBER OF VIS MAJOR RELIABILITY: " + VisRB);
						System.out.println("NUMBER OF SNOW_SMOOTHED: " + SnowSmoothed);
						System.out.println("NUMBER OF SNOW_SMOOTHED MINOR RESPONSE TIME: " + SnowSmoothedRT);
						System.out.println("NUMBER OF SNOW_SMOOTHED MAJOR RELIABILITY: " + SnowSmoothedRB);
						System.out.println("NUMBER OF SNOW_SMOOT: " + SnowSmoot);
						System.out.println("NUMBER OF SNOW_SMOOT MINOR RESPONSE TIME: " + SnowSmootRT);
						System.out.println("NUMBER OF SNOW_SMOOT MAJOR RELIABILITY: " + SnowSmootRB);
						System.out.println("NUMBER OF SNOW_DEPTH: " + SnowDepth);
						System.out.println("NUMBER OF SNOW_DEPTH MINOR RESPONSE TIME: " + SnowDepthRT);
						System.out.println("NUMBER OF SNOW_DEPTH MAJOR RELIABILITY: " + SnowDepthRB);
						System.out.println("NUMBER OF SNOW_D: " + SnowD);
						System.out.println("NUMBER OF SNOW_D MINOR RESPONSE TIME: " + SnowDRT);
						System.out.println("NUMBER OF SNOW_D MAJOR RELIABILITY: " + SnowDRB);
						System.out.println("NUMBER OF PRECIPITATION_SMOOTHED: " + PrecipitationSmoothed);
						System.out.println("NUMBER OF PRECIPITATION_SMOOTHED MINOR RESPONSE TIME: " + PrecipitationSmoothedRT);
						System.out.println("NUMBER OF PRECIPITATION_SMOOTHED MAJOR RELIABILITY: " + PrecipitationSmoothedRB);
						System.out.println("NUMBER OF PRECIPITATION_SMOOT: " + PrecipitationSmoot);
						System.out.println("NUMBER OF PRECIPITATION_SMOOT MINOR RESPONSE TIME: " + PrecipitationSmootRT);
						System.out.println("NUMBER OF PRECIPITATION_SMOOT MAJOR RELIABILITY: " + PrecipitationSmootRB);
						System.out.println("NUMBER OF SOIL_TEMPERATURE: " + SoilTemperature);
						System.out.println("NUMBER OF SOIL_TEMPERATURE MINOR RESPONSE TIME: " + SoilTemperatureRT);
						System.out.println("NUMBER OF SOIL_TEMPERATURE MAJOR RELIABILITY: " + SoilTemperatureRB);
						System.out.println("NUMBER OF SOIL_TEMP: " + SoilTemp);
						System.out.println("NUMBER OF SOIL_TEMP MINOR RESPONSE TIME: " + SoilTempRT);
						System.out.println("NUMBER OF SOIL_TEMP MAJOR RELIABILITY: " + SoilTempRB);
						System.out.println("NUMBER OF SOIL_MOISTURE: " + SoilMoisture);
						System.out.println("NUMBER OF SOIL_MOISTURE MINOR RESPONSE TIME: " + SoilMoistureRT);
						System.out.println("NUMBER OF SOIL_MOISTURE MAJOR RELIABILITY: " + SoilMoistureRB);
						System.out.println("NUMBER OF SOIL_MOIST: " + SoilMoist);
						System.out.println("NUMBER OF SOIL_MOIST MINOR RESPONSE TIME: " + SoilMoistRT);
						System.out.println("NUMBER OF SOIL_MOIST MAJOR RELIABILITY: " + SoilMoistRB);
						System.out.println("NUMBER OF SNOW_INTERVAL: " + SnowInterval);
						System.out.println("NUMBER OF SNOW_INTERVAL MINOR RESPONSE TIME: " + SnowIntervalRT);
						System.out.println("NUMBER OF SNOW_INTERVAL MAJOR RELIABILITY: " + SnowIntervalRB);
						System.out.println("NUMBER OF SNOW_INT: " + SnowInt);
						System.out.println("NUMBER OF SNOW_INT MINOR RESPONSE TIME: " + SnowIntRT);
						System.out.println("NUMBER OF SNOW_INT MAJOR RELIABILITY: " + SnowIntRB);
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
				break;
			}
		}
		System.out.println("**********************LIST OF SERVICES AT THE END**********************");
		for(int i = 0; i<services.size(); i++){
			System.out.println("----------------New Service----------------");
			System.out.println("ID: " + services.get(i).getId());
			System.out.println("URI: " + services.get(i).getUri());
			System.out.println("NAME: " + services.get(i).getName());
			System.out.println("TITLE: " + service.getTitle());
			System.out.println("DESCRIPTION: " + services.get(i).getDescription());
			System.out.println("UNIT: " + services.get(i).getUnit());
			System.out.println("RELIABILITY: " + services.get(i).getRb());
			System.out.println("RESPONSE TIME: " + services.get(i).getRt());
			System.out.println("LOCATION.X: " + services.get(i).getLocationX());
			System.out.println("LOCATION.Y: " + services.get(i).getLocationY());
			System.out.println("LOCATION.Z: " + services.get(i).getLocationZ());
		}
		
		System.out.println("++++++++++++++++++++++++++++++STATISTICS AT THE END++++++++++++++++++++++++++++++");
		System.out.println("NUMBER OF SERVICES: " + services.size());
		System.out.println("NUMBER OF WIND_DIRECTION: " + WindDirection);
		System.out.println("NUMBER OF WIND_DIRECTION MINOR RESPONSE TIME: " + WindDirectionRT);
		System.out.println("NUMBER OF WIND_DIRECTION MAJOR RELIABILITY: " + WindDirectionRB);
		System.out.println("NUMBER OF WIND_DIR: " + WindDir);
		System.out.println("NUMBER OF WIND_DIR MINOR RESPONSE TIME: " + WindDirRT);
		System.out.println("NUMBER OF WIND_DIR MAJOR RELIABILITY: " + WindDirRB);
		System.out.println("NUMBER OF RELATIVE_HUMIDITY: " + RelativeHumidity);
		System.out.println("NUMBER OF RELATIVE_HUMIDITY MINOR RESPONSE TIME: " + RelativeHumidityRT);
		System.out.println("NUMBER OF RELATIVE_HUMIDITY MAJOR RELIABILITY: " + RelativeHumidityRB);
		System.out.println("NUMBER OF REL_HUMIDITY: " + RelHumidity);
		System.out.println("NUMBER OF REL_HUMIDITY MINOR RESPONSE TIME: " + RelHumidityRT);
		System.out.println("NUMBER OF REL_HUMIDITY MAJOR RELIABILITY: " + RelHumidityRB);
		System.out.println("NUMBER OF DEW_POINT: " + DewPoint);
		System.out.println("NUMBER OF DEW_POINT MINOR RESPONSE TIME: " + DewPointRT);
		System.out.println("NUMBER OF DEW_POINT MAJOR RELIABILITY: " + DewPointRB);
		System.out.println("NUMBER OF DEW_P: " + DewP);
		System.out.println("NUMBER OF DEW_P MINOR RESPONSE TIME: " + DewPRT);
		System.out.println("NUMBER OF DEW_P MAJOR RELIABILITY: " + DewPRB);
		System.out.println("NUMBER OF AIR_TEMPERATURE: " + AirTemperature);
		System.out.println("NUMBER OF AIR_TEMPERATURE MINOR RESPONSE TIME: " + AirTemperatureRT);
		System.out.println("NUMBER OF AIR_TEMPERATURE MAJOR RELIABILITY: " + AirTemperatureRB);
		System.out.println("NUMBER OF AIR_TEMP: " + AirTemp);
		System.out.println("NUMBER OF AIR_TEMP MINOR RESPONSE TIME: " + AirTempRT);
		System.out.println("NUMBER OF AIR_TEMP MAJOR RELIABILITY: " + AirTempRB);
		System.out.println("NUMBER OF PEAK_WIND_SPEED: " + PeakWindSpeed);
		System.out.println("NUMBER OF PEAK_WIND_SPEED MINOR RESPONSE TIME: " + PeakWindSpeedRT);
		System.out.println("NUMBER OF PEAK_WIND_SPEED MAJOR RELIABILITY: " + PeakWindSpeedRB);
		System.out.println("NUMBER OF PEAK_WIND_S: " + PeakWindS);
		System.out.println("NUMBER OF PEAK_WIND_S MINOR RESPONSE TIME: " + PeakWindSRT);
		System.out.println("NUMBER OF PEAK_WIND_S MAJOR RELIABILITY: " + PeakWindSRB);
		System.out.println("NUMBER OF WIND_SPEED: " + WindSpeed);
		System.out.println("NUMBER OF WIND_SPEED MINOR RESPONSE TIME: " + WindSpeedRT);
		System.out.println("NUMBER OF WIND_SPEED MAJOR RELIABILITY: " + WindSpeedRB);
		System.out.println("NUMBER OF WIND_S: " + WindS);
		System.out.println("NUMBER OF WIND_S MINOR RESPONSE TIME: " + WindSRT);
		System.out.println("NUMBER OF WIND_S MAJOR RELIABILITY: " + WindSRB);
		System.out.println("NUMBER OF WIND_GUST: " + WindGust);
		System.out.println("NUMBER OF WIND_GUST MINOR RESPONSE TIME: " + WindGustRT);
		System.out.println("NUMBER OF WIND_GUST MAJOR RELIABILITY: " + WindGustRB);
		System.out.println("NUMBER OF WIND_G: " + WindG);
		System.out.println("NUMBER OF WIND_G MINOR RESPONSE TIME: " + WindGRT);
		System.out.println("NUMBER OF WIND_G MAJOR RELIABILITY: " + WindGRB);
		System.out.println("NUMBER OF PRECIPITATION_ACCUMULATED: " + PrecipitationAccumulated);
		System.out.println("NUMBER OF PRECIPITATION_ACCUMULATED MINOR RESPONSE TIME: " + PrecipitationAccumulatedRT);
		System.out.println("NUMBER OF PRECIPITATION_ACCUMULATED MAJOR RELIABILITY: " + PrecipitationAccumulatedRB);
		System.out.println("NUMBER OF PRECIPITATION_ACC: " + PrecipitationAcc);
		System.out.println("NUMBER OF PRECIPITATION_ACC MINOR RESPONSE TIME: " + PrecipitationAccRT);
		System.out.println("NUMBER OF PRECIPITATION_ACC MAJOR RELIABILITY: " + PrecipitationAccRB);
		System.out.println("NUMBER OF PEAK_WIND_DIRECTION: " + PeakWindDirection);
		System.out.println("NUMBER OF PEAK_WIND_DIRECTION MINOR RESPONSE TIME: " + PeakWindDirectionRT);
		System.out.println("NUMBER OF PEAK_WIND_DIRECTION MAJOR RELIABILITY: " + PeakWindDirectionRB);
		System.out.println("NUMBER OF PEAK_WIND_DIR: " + PeakWindDir);
		System.out.println("NUMBER OF PEAK_WIND_DIR MINOR RESPONSE TIME: " + PeakWindDirRT);
		System.out.println("NUMBER OF PEAK_WIND_DIR MAJOR RELIABILITY: " + PeakWindDirRB);
		System.out.println("NUMBER OF VISIBILITY: " + Visibility);
		System.out.println("NUMBER OF VISIBILITY MINOR RESPONSE TIME: " + VisibilityRT);
		System.out.println("NUMBER OF VISIBILITY MAJOR RELIABILITY: " + VisibilityRB);
		System.out.println("NUMBER OF VIS: " + Vis);
		System.out.println("NUMBER OF VIS MINOR RESPONSE TIME: " + VisRT);
		System.out.println("NUMBER OF VIS MAJOR RELIABILITY: " + VisRB);
		System.out.println("NUMBER OF SNOW_SMOOTHED: " + SnowSmoothed);
		System.out.println("NUMBER OF SNOW_SMOOTHED MINOR RESPONSE TIME: " + SnowSmoothedRT);
		System.out.println("NUMBER OF SNOW_SMOOTHED MAJOR RELIABILITY: " + SnowSmoothedRB);
		System.out.println("NUMBER OF SNOW_SMOOT: " + SnowSmoot);
		System.out.println("NUMBER OF SNOW_SMOOT MINOR RESPONSE TIME: " + SnowSmootRT);
		System.out.println("NUMBER OF SNOW_SMOOT MAJOR RELIABILITY: " + SnowSmootRB);
		System.out.println("NUMBER OF SNOW_DEPTH: " + SnowDepth);
		System.out.println("NUMBER OF SNOW_DEPTH MINOR RESPONSE TIME: " + SnowDepthRT);
		System.out.println("NUMBER OF SNOW_DEPTH MAJOR RELIABILITY: " + SnowDepthRB);
		System.out.println("NUMBER OF SNOW_D: " + SnowD);
		System.out.println("NUMBER OF SNOW_D MINOR RESPONSE TIME: " + SnowDRT);
		System.out.println("NUMBER OF SNOW_D MAJOR RELIABILITY: " + SnowDRB);
		System.out.println("NUMBER OF PRECIPITATION_SMOOTHED: " + PrecipitationSmoothed);
		System.out.println("NUMBER OF PRECIPITATION_SMOOTHED MINOR RESPONSE TIME: " + PrecipitationSmoothedRT);
		System.out.println("NUMBER OF PRECIPITATION_SMOOTHED MAJOR RELIABILITY: " + PrecipitationSmoothedRB);
		System.out.println("NUMBER OF PRECIPITATION_SMOOT: " + PrecipitationSmoot);
		System.out.println("NUMBER OF PRECIPITATION_SMOOT MINOR RESPONSE TIME: " + PrecipitationSmootRT);
		System.out.println("NUMBER OF PRECIPITATION_SMOOT MAJOR RELIABILITY: " + PrecipitationSmootRB);
		System.out.println("NUMBER OF SOIL_TEMPERATURE: " + SoilTemperature);
		System.out.println("NUMBER OF SOIL_TEMPERATURE MINOR RESPONSE TIME: " + SoilTemperatureRT);
		System.out.println("NUMBER OF SOIL_TEMPERATURE MAJOR RELIABILITY: " + SoilTemperatureRB);
		System.out.println("NUMBER OF SOIL_TEMP: " + SoilTemp);
		System.out.println("NUMBER OF SOIL_TEMP MINOR RESPONSE TIME: " + SoilTempRT);
		System.out.println("NUMBER OF SOIL_TEMP MAJOR RELIABILITY: " + SoilTempRB);
		System.out.println("NUMBER OF SOIL_MOISTURE: " + SoilMoisture);
		System.out.println("NUMBER OF SOIL_MOISTURE MINOR RESPONSE TIME: " + SoilMoistureRT);
		System.out.println("NUMBER OF SOIL_MOISTURE MAJOR RELIABILITY: " + SoilMoistureRB);
		System.out.println("NUMBER OF SOIL_MOIST: " + SoilMoist);
		System.out.println("NUMBER OF SOIL_MOIST MINOR RESPONSE TIME: " + SoilMoistRT);
		System.out.println("NUMBER OF SOIL_MOIST MAJOR RELIABILITY: " + SoilMoistRB);
		System.out.println("NUMBER OF SNOW_INTERVAL: " + SnowInterval);
		System.out.println("NUMBER OF SNOW_INTERVAL MINOR RESPONSE TIME: " + SnowIntervalRT);
		System.out.println("NUMBER OF SNOW_INTERVAL MAJOR RELIABILITY: " + SnowIntervalRB);
		System.out.println("NUMBER OF SNOW_INT: " + SnowInt);
		System.out.println("NUMBER OF SNOW_INT MINOR RESPONSE TIME: " + SnowIntRT);
		System.out.println("NUMBER OF SNOW_INT MAJOR RELIABILITY: " + SnowIntRB);
		
		for(int i = 0; i < others.size(); i++){
			System.out.println("NEW SERVICE: " + others.get(i));				
		}
		
		System.out.println("**********************WRITTING FILES**********************");
		for(int i = 0; i<services.size(); i++){
			writeFile(services.get(i));
			//System.out.println("--"+i+"--");
		}
		System.out.println("**********************WRITTING FILES FINISHED**********************");
	}
	
	private static String readFile( String file ) throws IOException {
	    BufferedReader reader = new BufferedReader( new FileReader (file));
	    String         line = null;
	    StringBuilder  stringBuilder = new StringBuilder();
	    String         ls = System.getProperty("line.separator");

	    try {
	        while( ( line = reader.readLine() ) != null ) {
	            stringBuilder.append( line );
	            stringBuilder.append( ls );
	        }

	        return stringBuilder.toString();
	    } finally {
	        reader.close();
	    }
	}
	
	private static void writeFile( Service service ) {
		File f;
		f = new File("/home/ubuntu/data/"+service.getId()+".txt");
		
		try{
			FileWriter w = new FileWriter(f);
			BufferedWriter bw = new BufferedWriter(w);
			PrintWriter wr = new PrintWriter(bw);  
			wr.write(service.getId()+";"+service.getUri()+";"+service.getName()+";"+service.getRt()+";"+service.getRb()+";"+service.getLocationX()+";"+service.getLocationY()+";"+service.getLocationZ()+";"+service.getDescription()+";"+service.getUnit()+";"+service.getTitle());
			wr.close();
			bw.close();
			}
		catch(IOException e){
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}
}
