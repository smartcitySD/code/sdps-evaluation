package surf.coap.server;

/*import java.io.BufferedReader;
import java.io.InputStreamReader;*/

import org.eclipse.californium.core.CoapServer;
//import org.eclipse.californium.core.server.resources.ResourceAttributes;

import surf.coap.resource.CoAPResource;

public class CoAPServer {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// binds on UDP port 5683
		//freePort();
		CoapServer server = new CoapServer();
		CoAPResource res = new CoAPResource("WeatherTest");
		res.setObservable(true);
		res.setPath("/WeatherTest/");
		res.setName("WeatherTest");
		res.getAttributes().setAttribute("name","name1");
		res.getAttributes().setAttribute("author","author1");
		server.add(res);
		try {
			server.start();
		}
		catch(Exception ex){
			System.out.println(ex.getMessage());
		}
	}

	/*private static void freePort() {
		// TODO Auto-generated method stub
		StringBuffer output = new StringBuffer();

		Process p;
		String process="";
		try {
			p = Runtime.getRuntime().exec("netstat -lpn | grep :5683");
			p.waitFor();
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));

            String line = "";
            while ((line = reader.readLine())!= null) {
				output.append(line + "\n");
				if(line.contains("5683"))process = line;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		process=process.replace(" ", "").replace("udp600:::5683:::*", "").replace("/java", "");
		try {
	        p = Runtime.getRuntime().exec("sh /home/ubuntu/killCoap.sh "+process);
	    } catch (Exception e) {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
	    }
		System.out.println(process);
	}*/
}
