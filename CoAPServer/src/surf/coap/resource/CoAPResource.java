package surf.coap.resource;


import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;

import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.coap.MediaTypeRegistry;
import org.eclipse.californium.core.coap.Response;
import org.eclipse.californium.core.coap.CoAP.ResponseCode;
import org.eclipse.californium.core.server.resources.CoapExchange;
import org.eclipse.californium.core.server.resources.Resource;
import org.json.JSONException;
import org.json.JSONObject;

public class CoAPResource extends CoapResource  {

private String content;
	
	public CoAPResource(String name) {
		super(name);
	}
	
	@Override
	public void handleGET(CoapExchange exchange) {
		if (content != null) {
			JSONObject payload = new JSONObject();
			try{
				if(this.getName().trim().length()>0) payload.append("name", this.getName());
				if(this.getURI().trim().length()>0) payload.append("URI", this.getURI());
				if(this.getPath().trim().length()>0) payload.append("path", this.getPath()); 
				if(this.getAttributes().getCount()>0){
					JSONObject attributes = new JSONObject();
					attributes.append("title", this.getAttributes().getTitle());
					if(this.getAttributes().containsAttribute("responseTime"))attributes.append("responseTime", this.getAttributes().getAttributeValues("responseTime"));
					if(this.getAttributes().containsAttribute("name"))attributes.append("name", this.getAttributes().getAttributeValues("name"));
					if(this.getAttributes().containsAttribute("author"))attributes.append("author", this.getAttributes().getAttributeValues("author"));
					if(this.getAttributes().containsAttribute("reliability"))attributes.append("reliability", this.getAttributes().getAttributeValues("reliability"));
					if(this.getAttributes().containsAttribute("locationX"))attributes.append("locationX", this.getAttributes().getAttributeValues("locationX"));
					if(this.getAttributes().containsAttribute("locationY"))attributes.append("locationY", this.getAttributes().getAttributeValues("locationY"));
					if(this.getAttributes().containsAttribute("locationZ"))attributes.append("locationZ", this.getAttributes().getAttributeValues("locationZ"));
					if(this.getAttributes().containsAttribute("description"))attributes.append("description", this.getAttributes().getAttributeValues("description"));
					if(this.getAttributes().containsAttribute("unit"))attributes.append("unit", this.getAttributes().getAttributeValues("unit"));
					payload.append("attributes", attributes);
				}
			}
			catch(Exception ex){
				ex.printStackTrace();
			}
			String message = payload.toString().replace("[", "").replace("]", "").replace("\n", "");
			exchange.respond(ResponseCode.CONTENT, message, MediaTypeRegistry.APPLICATION_JSON);
		} else {
			JSONObject payload = new JSONObject();
			try{
				if(this.getName().trim().length()>0) payload.append("name", this.getName());
				if(this.getURI().trim().length()>0) payload.append("URI", this.getURI());
				if(this.getPath().trim().length()>0) payload.append("path", this.getPath()); 
				if(this.getAttributes().getCount()>0){
					JSONObject attributes = new JSONObject();
					attributes.append("title", this.getAttributes().getTitle());
					if(this.getAttributes().containsAttribute("responseTime"))attributes.append("responseTime", this.getAttributes().getAttributeValues("responseTime"));
					if(this.getAttributes().containsAttribute("reliability"))attributes.append("reliability", this.getAttributes().getAttributeValues("reliability"));
					if(this.getAttributes().containsAttribute("locationX"))attributes.append("locationX", this.getAttributes().getAttributeValues("locationX"));
					if(this.getAttributes().containsAttribute("locationY"))attributes.append("locationY", this.getAttributes().getAttributeValues("locationY"));
					if(this.getAttributes().containsAttribute("locationZ"))attributes.append("locationZ", this.getAttributes().getAttributeValues("locationZ"));
					if(this.getAttributes().containsAttribute("description"))attributes.append("description", this.getAttributes().getAttributeValues("description"));
					if(this.getAttributes().containsAttribute("unit"))attributes.append("unit", this.getAttributes().getAttributeValues("unit"));
					payload.append("attributes", attributes);
				}
			}
			catch(Exception ex){
				ex.printStackTrace();
			}
			String message = payload.toString().replace("[", "").replace("]", "").replace("\n", "");
			exchange.respond(ResponseCode.CONTENT, message, MediaTypeRegistry.APPLICATION_JSON);
		}
	}

	@Override
	public void handlePOST(CoapExchange exchange) {
		int format = exchange.getRequestOptions().getContentFormat();
		String payload = exchange.getRequestText();
		Resource resource;
		Response response;
		switch(format){
			case 0:
				String[] parts = payload.split("\\?");
				String[] path = parts[0].split("/");
				resource = create(new LinkedList<String>(Arrays.asList(path)));
				response = new Response(ResponseCode.CREATED);
				response.getOptions().setLocationPath(resource.getURI());
				exchange.respond(response);
				break;
			case 50:
				try {
					JSONObject jsonobject = new JSONObject(payload);
					resource = create(jsonobject);
					response = new Response(ResponseCode.CREATED);
					response.getOptions().setLocationPath(resource.getURI());
					exchange.respond(response);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;
		}
	}

	@Override
	public void handlePUT(CoapExchange exchange) {
		content = exchange.getRequestText();
		exchange.respond(ResponseCode.CHANGED);
	}

	@Override
	public void handleDELETE(CoapExchange exchange) {
		int format = exchange.getRequestOptions().getContentFormat();
		//System.out.println("Format: "+format);
		String payload = exchange.getRequestText();
		//System.out.println("Payload: "+payload);
		Collection<Resource> resources = this.getChildren();
		//System.out.println("Number of resources: "+resources.size());
		Resource resource;
		switch(format){
			case 0:
				//System.out.println("Into case 0");
				for(Resource r : resources){
					//System.out.println("Into for resources - 0");
					this.delete(r);
					//System.out.println("Deleted resource:"+ r.getName());
				}
				break;
			case 1:
				//System.out.println("Into case 1");
				for(Resource r : resources){
					//System.out.println("Into for resources - 1");
					JSONObject jsonobject;
					try {
						jsonobject = new JSONObject(payload);
						resource = create(jsonobject);
						//System.out.println("After json: " + r.getName() + "-" + resource.getName());
						if(r.getName().equals(resource.getName())){
							//System.out.println("Find the resource");
							this.delete(r);
							//System.out.println("Delete resource:" + r.getName());
							break;
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				break;
		}
		exchange.respond(ResponseCode.DELETED);
	}

	/**
	 * Find the requested child. If the child does not exist yet, create it.
	 */
	@Override
	public Resource getChild(String name) {
		Resource resource = super.getChild(name);
		if (resource == null) {
			resource = new CoAPResource(name);
			add(resource);
		}
		return resource;
	}
	
	/**
	 * Create a resource hierarchy with according to the specified path.
	 * @param path the path
	 * @return the lowest resource from the hierarchy
	 */
	private Resource create(LinkedList<String> path) {
		String segment;
		do {
			if (path.size() == 0)
				return this;
		
			segment = path.removeFirst();
		} while (segment.isEmpty() || segment.equals("/"));
		
		CoAPResource resource = new CoAPResource(segment);
		add(resource);
		return resource.create(path);
	}
	
	/**
	 * Create a resource with its attributes.
	 * @param path the path
	 * @return the lowest resource from the hierarchy
	 */
	private Resource create(JSONObject jsonObject) {
		String href="";
		String title="";
		JSONObject attributes;
		String responseTime="";
		String reliability="";
		String locationX="";
		String locationY="";
		String locationZ="";
		String description="";
		String unit="";
		try {
			href = jsonObject.getString("href");
			attributes = jsonObject.getJSONObject("attributes");
			responseTime = attributes.getString("responseTime");
			reliability = attributes.getString("reliability");
			locationX = attributes.getString("locationX");
			locationY = attributes.getString("locationY");
			locationZ = attributes.getString("locationZ");
			description = attributes.getString("description");
			unit = attributes.getString("unit");
			title = attributes.getString("title");
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		CoAPResource resource = new CoAPResource(href);
		resource.setObservable(true);
		resource.setPath(href);
		resource.getAttributes().setTitle(title);
		resource.getAttributes().addAttribute("responseTime", responseTime);
		resource.getAttributes().addAttribute("reliability", reliability);
		resource.getAttributes().addAttribute("locationX", locationX);
		resource.getAttributes().addAttribute("locationY", locationY);
		resource.getAttributes().addAttribute("locationZ", locationZ);
		resource.getAttributes().addAttribute("description", description);
		resource.getAttributes().addAttribute("unit", unit);
		add(resource);
		return resource;
		
	}

}

