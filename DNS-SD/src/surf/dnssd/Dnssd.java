package surf.dnssd;

import java.io.IOException;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;

import javax.jmdns.JmDNS;
import javax.jmdns.ServiceInfo;

public class Dnssd {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			InetAddress address =InetAddress.getByName("192.168.1.101");
	
			JmDNS jmdns = JmDNS.create(address);
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("rt", "500");
			map.put("rel", "0.8");
			map.put("x", "1");
			map.put("y", "-1");
			map.put("z", "1");
			ServiceInfo info = ServiceInfo.create("_test._tcp.local.","test46",4444,100,100,map);
			//info.
			jmdns.registerService(info);
			//jmdns.unregisterAllServices();
			ServiceInfo[] serviceInfos = jmdns.list("_test._tcp.local.");
			for (ServiceInfo service : serviceInfos) {
				  System.out.println("## resolve service " + service.getName()  + " : " + service.getURL());
				  System.out.println("-- Response Time: " + service.getPropertyString("rt"));
				  System.out.println("-- Reliability: " + service.getPropertyString("rel"));
				  System.out.println("-- Latitude: " + service.getPropertyString("x"));
				  System.out.println("-- Longitude: " + service.getPropertyString("y"));
				  System.out.println("-- Altitude: " + service.getPropertyString("z"));
			}
			
			serviceInfos = jmdns.list("_teamviewer._tcp.local.");
			for (ServiceInfo service : serviceInfos) {
				  System.out.println("## resolve service " + service.getName()  + " : " + service.getURL());
				  System.out.println("-- Response Time: " + service.getPropertyString("rt"));
				  System.out.println("-- Reliability: " + service.getPropertyString("rel"));
				  System.out.println("-- Latitude: " + service.getPropertyString("x"));
				  System.out.println("-- Longitude: " + service.getPropertyString("y"));
				  System.out.println("-- Altitude: " + service.getPropertyString("z"));
			}
			
			serviceInfos = jmdns.list("_http._tcp.local.");
			for (ServiceInfo service : serviceInfos) {
				  System.out.println("## resolve service " + service.getName()  + " : " + service.getURL());
				  System.out.println("-- Response Time: " + service.getPropertyString("rt"));
				  System.out.println("-- Reliability: " + service.getPropertyString("rel"));
				  System.out.println("-- Latitude: " + service.getPropertyString("x"));
				  System.out.println("-- Longitude: " + service.getPropertyString("y"));
				  System.out.println("-- Altitude: " + service.getPropertyString("z"));
			}
			//jmdns.printServices();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}
