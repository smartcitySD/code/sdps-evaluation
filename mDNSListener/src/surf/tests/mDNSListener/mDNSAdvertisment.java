package surf.tests.mDNSListener;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

import surf.util.Util;

public class mDNSAdvertisment {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//String user = "root";
		String user = "pi";
	    //String password = "root";
	    String password = "raspberry";
	    //String host = "192.168.2.1";
	    String host = "192.168.2.5";
	    //String host = "192.168.2.10";
	    //String host = "192.168.2.15";
	    //String host = "192.168.2.20";
	    int port=22;
	    String round = "";
	    
	    Util u = new Util();
	    
	    //String base = "D:/experiments/results/mdns/";
	    String base = "/home/pi/experiments/results/mdns/";

	    JSch jsch = new JSch();
	    try{
	    	Session session = jsch.getSession(user, host, port);
	    	session.setPassword(password);
	    	session.setConfig("StrictHostKeyChecking", "no");
	    	System.out.println("Establishing Connection...");
        
        	session.connect();
        	System.out.println("ADVERTISEMENT.");
        	System.out.println("Connection established.");
        	ChannelExec channel=(ChannelExec) session.openChannel("exec");
        	BufferedReader in=new BufferedReader(new InputStreamReader(channel.getInputStream()));
        	channel.setCommand("avahi-browse -a;");
        	channel.connect();
        	String msg=null;
        	int i = 1;
        	int read = 0;
        	
        	ArrayList<String> timesRegister = new ArrayList<>();
        	while((msg=in.readLine())!=null){
        		read ++;
        		i=Integer.parseInt(u.readFile(base+"temp/i.txt").replaceAll("[\n\r]",""));
        		if(!round.equals(u.readFile(base+"temp/round.txt").replaceAll("[\n\r]",""))){
        			round = u.readFile(base+"temp/round.txt").replaceAll("[\n\r]","");
        			timesRegister = new ArrayList<>();
        		}
        		if(msg.contains("+  wlan0 IPv4 example"+i)){
        		//if(msg.contains("+  wlan3 IPv4 example"+i)){
        			timesRegister.add(""+System.currentTimeMillis()+","+i);
        			//u.writeFile(base+"/"+round+"/1/timesRegister.txt", timesRegister);
        			u.writeFile(base+"/"+round+"/5/timesRegister.txt", timesRegister);
        			//u.writeFile(base+"/"+round+"/10/timesRegister.txt", timesRegister);
        			//u.writeFile(base+"/"+round+"/15/timesRegister.txt", timesRegister);
        			//u.writeFile(base+"/"+round+"/20/timesRegister.txt", timesRegister);
        		}
        		
        		System.out.println(msg+"---timesRegister: "+timesRegister.size()+"---i: "+i+"---Round:"+round+"---Read:"+read);
        		
        	}
        	channel.disconnect();
        	session.disconnect();
        }catch(Exception e){
        	e.printStackTrace();
        }
	}

}
