package surf.tests.DDSListener;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;

import com.jcraft.jsch.ChannelShell;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

import surf.util.Util;

public class DDSListener {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String user = "root";
		//String user = "pi";
	    String password = "root";
	    //String password = "raspberry";
	    //String host = "192.168.2.1";
	    //String host = "192.168.2.5";
	    //String host = "192.168.2.10";
	    //String host = "192.168.2.15";
	    String host = "192.168.2.20";
	    int port=22;
	    String round = "";
	    
	    Util u = new Util();
	    
	    //String base = "D:/experiments/results/dds/";
	    String base = "/home/pi/experiments/results/dds/";

	    JSch jsch = new JSch();
	    try{
	    	Session session = jsch.getSession(user, host, port);
	    	session.setPassword(password);
	    	session.setConfig("StrictHostKeyChecking", "no");
	    	System.out.println("Establishing Connection...");
        
        	session.connect();
        	System.out.println("LISTENER.");
        	System.out.println("Connection established.");
        	ChannelShell channel=(ChannelShell) session.openChannel("shell");
			BufferedReader in=new BufferedReader(new InputStreamReader(channel.getInputStream()));
			OutputStream out = channel.getOutputStream();
			PrintStream commander = new PrintStream(out, true);
			channel.connect();
        	
			//commander.println("java -jar /home/pi/env/rti/SimpleDDS/SimpleDDS.jar");
			commander.println("java -jar /root/env/rti/SimpleDDS/SimpleDDS.jar");
        	commander.close();
        	
        	String msg=null;
        	int i = 1;
        	boolean r = false;
        	
        	ArrayList<String> timesRegister = new ArrayList<>();
        	ArrayList<String> timesUnregister = new ArrayList<>();
        	while((msg=in.readLine())!=null){
        		if(!round.equals(u.readFile(base+"temp/round.txt").replaceAll("[\n\r]",""))){
        			round = u.readFile(base+"temp/round.txt").replaceAll("[\n\r]","");
        			timesRegister = new ArrayList<>();
        			timesUnregister = new ArrayList<>();
        		}
        		if(msg.contains("DataWriter (New) name: \"null\" topic: \"example"+i+"\" type: \"DDS::String\"")){
        			timesRegister.add(""+System.currentTimeMillis());
        			//u.writeFile(base+"/"+round+"/5/timesRegister.txt", timesRegister);
        			//u.writeFile(base+"/"+round+"/10/timesRegister.txt", timesRegister);
        			//u.writeFile(base+"/"+round+"/15/timesRegister.txt", timesRegister);
        			u.writeFile(base+"/"+round+"/20/timesRegister.txt", timesRegister);
        			r = true;
        		}
        		
        		if(r){
        	    	if(msg.contains("processDiscoveredTypes")){
        	    		timesUnregister.add(""+System.currentTimeMillis());
            			//u.writeFile(base+"/"+round+"/5/timesUnregister.txt", timesUnregister);
            			//u.writeFile(base+"/"+round+"/10/timesUnregister.txt", timesUnregister);
            			//u.writeFile(base+"/"+round+"/15/timesUnregister.txt", timesUnregister);
            			u.writeFile(base+"/"+round+"/20/timesUnregister.txt", timesUnregister);
            			r = false;
            			i++;
            	    }
        	    }
        		System.out.println(msg+"---timesRegister: "+timesRegister.size()+"---timesUnregister: "+timesUnregister.size()+"---i: "+i+"---Round:"+round);
        	}
        	channel.disconnect();
        	session.disconnect();
        }catch(Exception e){
        	e.printStackTrace();
        }
	}

}
